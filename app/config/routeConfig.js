angular.module("app").config(function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');

    $routeProvider.when("/Home", {
      templateUrl: "view/Home.html",
      controller: "HomeController",
      controllerAs: 'vm'
    })
    $routeProvider.when("/admin", {
      templateUrl: "view/admin/Admin.html",
      controller: "AdminController",
      controllerAs: 'vm',
      resolve: {
        rotas: function(permissaoservice){
          return permissaoservice.getRotas();
        }
      }
    })
    $routeProvider.when("/analise-documental", {
      templateUrl: "view/analise-documental/home-analise-documental.html",
      controller: "HomeAnaliseDocumentalController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/home-formalizacao", {
      templateUrl: "view/formalizacao/home-formalizacao.html",
      controller: "HomeFormalizacaoController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/home-sde", {
      templateUrl: "view/sde/home-sde.html",
      controller: "HomeSDEController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/home-monitoramento", {
      templateUrl: "view/monitoramento/home-monitoramento.html",
      controller: "HomeMonitoramentoController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/contestacao", {
      templateUrl: "view/contestacao/contestacao.html",
      controller: "HomeContestacaoController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/usuarios", {
      templateUrl: "view/usuarios.html",
      controller: "UsuariosController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/lista-contestacao", {
      templateUrl: "view/contestacao/lista-contestacao.html",
      controller: "ListaContestacaoController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/importacao", {
      templateUrl: "view/importacao.html",
      controller: "ImportacaoController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/home-portabilidade", {
      templateUrl: "view/portabilidade/home-portabilidade.html",
      controller: "HomePortabilidadeController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/lista-portabilidade", {
      templateUrl: "view/portabilidade/lista_portabilidade.html",
      controller: "ListaPortabilidadeController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/nova-importacao", {
      templateUrl: "view/novaimportacao.html",
      controller: "ImportacaoController",
      controllerAs: 'vm',
      resolve: {
        permissao:function(permissaoservice){
          return permissaoservice.getPermissoes();
        }
      }
    })
    $routeProvider.when("/fonte", {
      templateUrl: "view/fonte.html",
      controller: "FonteController",
      controllerAs: 'vm'
    })
    $routeProvider.when("/fonte/edit/:id", {
      templateUrl: "view/fonteedit.html",
      controller: "FonteController",
      controllerAs: 'vm'
    })
    // $routeProvider.when("/usuarios/novo", {
    //   templateUrl: "view/novousuario.html",
    //   controller: "NovoUsuarioController",
    //   controllerAs: 'vm',
    //   resolve: {
    //     rotas: function(permissaoservice){
    //       return permissaoservice.getRotas();
    //     }
    //   }
    // })
    // $routeProvider.when("/usuarios/novo/:id", {
    //   templateUrl: "view/novousuario.html",
    //   controller: "EditarUsuarioController",
    //   controllerAs: 'vm',
    //   resolve: {
    //     rotas: function(permissaoservice){
    //       return permissaoservice.getRotas();
    //     }
    //   }
    // })
    $routeProvider.when("/indicadores-contestacao", {
      templateUrl: "view/contestacao/indicadores-contestacao.html",
      controller: "IndicadoresContestacaoController",
      controllerAs: 'vm'
    })
    $routeProvider.when("/indicadores-harpia", {
      templateUrl: "view/analise-documental/indicadores-harpia.html",
      controller: "IndicadoresHarpiaController",
      controllerAs: 'vm'
    })
    $routeProvider.when("/indicadores-harpiaxwork", {
      templateUrl: "view/analise-documental/indicadores-harpiaxwork.html",
      controller: "IndicadoresHarpiaWorkController",
      controllerAs: 'vm'
    })
    $routeProvider.when("/indicadores-formalizacao", {
      templateUrl: "view/formalizacao/indicadores-formalizacao.html",
      controller: "IndicadoresFormalizacaoController",
      controllerAs: 'vm'
    })
    $routeProvider.when("/indicadores-sde", {
      templateUrl: "view/sde/indicadores-glpi.html",
      controller: "IndicadoresGLPIController"
    })

    $routeProvider.when("/indicadores-monitoramento", {
      templateUrl: "view/monitoramento/indicadores-monitoramento.html",
      controller: "MonitoramentoController",
    })


  })

  .run(function($rootScope, $location, $http) {

    $rootScope.$on('$locationChangeStart', function(event) {
      $http.get('backend/auth/auth.php').then(function successCallBack(response){
        if(response.data.dados.id_perfil != 1){
          if(response.data.permissoes.indexOf($location.path()) == -1){

          }
        }
      }, function errorCallBack(response){
        console.log(response)
      })

    });
  });
