(function () {
    'use strict';

    angular.module("app").controller("AdminController", AdminController);

    function AdminController($http, userservice, rotas, $scope) {
        var vm = this;

        vm.alert = false
        vm.unidades = []
        vm.usuarios = []
        vm.ufs = []
        vm.permissoesSelecionadas = []
        vm.filiaisSelecionadas = []
        vm.filiais = []

        vm.cadastrar_unidade = cadastrar_unidade
        vm.cadastrar_usuario = cadastrar_usuario
        vm.toggleSelection = toggleSelection

        getUsuarios()
        getUF()
        getUnidades()
        getPerfil()

        vm.tree = rotas

        function getUsuarios() {
            $http.get('backend/users/get_users.php')
                .then(function successCallback(response) {
                    vm.usuarios = response.data.dados
                }, function errorCallback(response) {
                    console.log(response.data)
                })
        }

        function cadastrar_usuario(usuario, myForm) {
            getPermissoesSelecionadas();
            if (myForm.$invalid) {
                $scope.form = myForm;
            } else {
                $http.post('backend/users/new.php', {
                    user: usuario,
                    permissao: vm.permissoesSelecionadas,
                    filial: vm.filiaisSelecionadas
                }).then(function successCallback(response) {
                    console.log(response.data)
                    if (response.data.retorno == true) {
                        $location.path('/usuarios')
                    } else if (response.data.retorno == false) {
                        $scope.error_form = true;
                        $scope.error_msg = response.data.msg;
                    }
                }, function errorCallback(response) {
                    console.log(response)
                })
            }
        }

        function getPerfil() {
            $http.get('backend/users/get_perfil.php').then(function successCallback(response) {
                console.log(response.data)
                vm.perfil = response.data;
            }, function errorCallback(response) {
                console.log(response)
            })
        }

        function getPermissoesSelecionadas() {
            angular.forEach(vm.tree, function (t) {
                if (t.checked) {
                    vm.permissoesSelecionadas.push(t.rota)
                }
                getPermissaoSeleionadasChildren(t)
            })
        }

        function getPermissaoSeleionadasChildren(tree) {
            angular.forEach(tree.children, function (c) {
                if (c.checked) {
                    vm.permissoesSelecionadas.push(c.rota)
                }
            })
        }

        function getUF() {
            return userservice.getFilial().then(function (data) {
                vm.ufs = data
                
                vm.filiais = data
                
            })
        }

        function getUnidades(uf) {
            $http.get('backend/contestacao/unidades.php')
                .then(function (response) {
                    vm.unidades = response.data
                })
        }

        function cadastrar_unidade(unidade, myForm) {
            vm.alert = false

            if (myForm.$invalid) {

            } else {
                $http.post('backend/unidade/novo.php', {
                    unidade: unidade
                }).then(function successCallback(response) {
                    if (response.data.status == 'error') {
                        vm.alert = true
                        vm.error_status = 'warning'
                        vm.error_msg = response.data.msg
                    } else {
                        getUnidades()
                        vm.alert = true
                        vm.error_status = 'success'
                        vm.error_msg = response.data.msg
                    }
                })
            }
        }

        function toggleSelection(filial) {
            var idx = vm.filiaisSelecionadas.indexOf(filial.filial);
      
            if (idx > -1) {
              vm.filiaisSelecionadas.splice(idx, 1);
            } else {
              vm.filiaisSelecionadas.push(filial);
            }
          };
    }

})()