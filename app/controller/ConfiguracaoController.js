(function(){
  'use strict';

  angular.module("app").controller("ConfiguracaoController", ConfiguracaoController);

  function ConfiguracaoController($routeParams,$scope,$route,$uibModal,cardservice) {
    var vm = this;
    vm.cards = [];
    vm.voltar = voltar
    vm.open = open;
    vm.excluir = excluir;

    getCardSecundario($routeParams.id);

    function getCardSecundario(id){
      return cardservice.get(id).then(function(data){
        console.log(data)
        vm.cards = data
      })
    }

    function getCardPrimario(){
      return cardservice.getPrimario(8).then(function(data){
        console.log(data)
        vm.cards = data
      })
    }

    function open(c){
      vm.success = false
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'view/modal/myModalCard.html',
        controller: 'myModalCardController',
        controllerAs: 'vm',
        resolve: {
          item: function() {
            return c;
          },
          id: function(){
            return $routeParams.id
          }
        },
      });

      modalInstance.result.then(function(dados) {

        cardservice.post(dados).then(function(data){
          if(data.retorno == true){
            vm.success = true;
            vm.msg = data.msg
            vm.cards.push(dados)
          }
        })
      }, function() {
        console.log('Modal dismissed at: ' + new Date());
      });
    }

    function excluir(item){
      vm.success = false;
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'view/modal/myModalExcluir.html',
        controllerAs: 'vm',
        controller: function($uibModalInstance){

          var vm = this;
          vm.ok = ok;
          vm.cancel = cancel;
          vm.title = 'Excluir Card';

          function ok(){
            $uibModalInstance.close(item);
          }
          function cancel(){
            $uibModalInstance.dismiss('cancel');
          }

        },

      });

      modalInstance.result.then(function(item) {
        cardservice.excluir(item).then(function(data){
          if(data.retorno == true){

            vm.cards.splice(vm.cards.indexOf(item), 1);
            vm.success = true;
            vm.msg = data.msg
          }
        })

      }, function() {
        console.log('Modal dismissed at: ' + new Date());
      });
    }


    $scope.toggleAnimation = function() {
      $scope.animationsEnabled = !$scope.animationsEnabled;
    };

    function voltar(){
      history.go(-1)
    }

  }


  angular.module("app").controller("myModalCardController", myModalCardController);

  function myModalCardController($scope, $uibModalInstance, item,id) {
    var vm = this;
    vm.ok = ok;
    vm.cancel = cancel;

    console.log(id)
    if(typeof item != 'undefined'){
      $scope.dados = item
    }else{
      $scope.dados = {
        id_card: id
      }
    }

    function ok(dados) {
      $uibModalInstance.close(dados);
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    };

  }


})()
