(function() {
  'use strict';

  angular.module("app").controller("EditarUsuarioController", EditarUsuarioController);

  function EditarUsuarioController($location, $routeParams, $http, $scope, $timeout, rotas) {

    var vm = this;
    vm.perfil = [];
    vm.permissaoSelecionada = [];
    vm.filiaisSelecionadas = [];
    vm.permissoesSelecionadas = []
    vm.tree = []
    vm.filiais = ["AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"];

    vm.cadastrar = cadastrar
    vm.toggleSelection = toggleSelection

    vm.tree = rotas

    getPerfil();
    getUsuario($routeParams.id)

    function toggleSelection(filial) {
      
      var idx = vm.filiaisSelecionadas.indexOf(filial);

      if (idx > -1) {
        vm.filiaisSelecionadas.splice(idx, 1);
      } else {
        vm.filiaisSelecionadas.push(filial);
      }
    };

    function cadastrar(user, myForm) {
      getPermissoesSelecionadas();
      if(myForm.$invalid){
        $scope.form = myForm;
      }else{
        $http.post('backend/users/editar.php', {
          user: user,
          permissao: vm.permissoesSelecionadas,
          filial: $scope.filiaisSelecionadas
        }).then(function successCallback(response){
          console.log(response.data)
          if(response.data.retorno == true){
            $location.path('/usuarios')
          }else if(response.data.retorno == false){
            $scope.error_form = true;
            $scope.error_msg = response.data.msg;
          }
        }, function errorCallback(response){
          console.log(response)
        })
      }
    }

    function getPerfil() {
      $http.get('backend/users/get_perfil.php').then(function successCallback(response) {
        console.log(response.data)
        vm.perfil = response.data;
      }, function errorCallback(response) {
        console.log(response)
      })
    }

    function getUsuario(id) {
      $http.post('backend/users/get_users.php', {
        id: id
      }).then(function successCallback(response) {
        console.log(response.data)
        $scope.user = response.data.dados
        if(typeof response.data.filiais == 'object'){
          vm.filiaisSelecionadas = response.data.filiais
        }
        $scope.permissoesSelecionadas = response.data.permissoes
        verificarPermissao(response.data.permissoes)
      }, function errorCallback(response) {
        console.log(response.data)
      })
    }

    function getPermissoesSelecionadas(){
      angular.forEach(vm.tree, function(t){
        if(t.checked){
          vm.permissoesSelecionadas.push(t.rota)
        }
        getPermissaoSeleionadasChildren(t)
      })
    }

    function getPermissaoSeleionadasChildren(tree){
      angular.forEach(tree.children, function(c){
        if(c.checked){
          vm.permissoesSelecionadas.push(c.rota)
        }
      })
    }

    function verificarPermissao(permissao){
      angular.forEach(vm.tree, function(t){
        if(permissao.indexOf(t.rota) >-1){
          t.checked = true;
        }

        verificarPermissaoChildren(t, permissao)

      })
    }

    function verificarPermissaoChildren(tree, permissao){
      angular.forEach(tree.children, function(t){
        if(permissao.indexOf(t.rota) >-1){
          t.checked = true;
        }

        verificarPermissaoChildren(t)

      })
    }

  }
})();
