(function(){
    'use strict';

    angular.module('app').controller('HomeController', HomeController);

    function HomeController($location,$http){

      var vm = this;
      vm.cards = [];

      getCard();

      function getCard(){
        $http.get('backend/auth/card.php').then(function successCallBack(response){
          if(response.data.length >0){
            console.log(response.data)
            vm.cards = response.data
          }
        }, function errorCallback(response){
          console.log(response)
        })
      }
    }

})()
