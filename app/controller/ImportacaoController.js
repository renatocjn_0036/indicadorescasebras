(function() {
  'use strict';

  angular.module("app").controller("ImportacaoController", ImportacaoController);

  function ImportacaoController($location, $routeParams, $http, $scope, $route,$interval, permissao) {


    var vm = this;
    //  vm.atualizar = atualizar;
    // $scope.attribute = {};
    vm.datasources = [];
    vm.importacoes = [];
    vm.situacao = [];
    vm.verificarPermissao = verificarPermissao
    getImports();
    getFontes();
    getSituacao();

    function verificarPermissao(rota){
      if(permissao == 'admin'){
        return true
      }else{
        if(permissao.indexOf(rota) == -1){
          return false
        }else{
          return true
        }
      }
    }

    function getImports() {
        $http.get('backend/imports/index.php').then(function successCallback(response) {
          console.log(response.data)
          vm.importacoes = response.data;
        }, function errorCallback(response) {

        })
      }


    function getFontes() {
      $http.get('backend/datasources/index.php').then(function successCallback(response) {
        console.log(response.data)
        vm.datasources = response.data;
      }, function errorCallback(response) {

      })
    }

    function getSituacao(){
      $http.get('backend/datasources/get_situacao.php').then(function successCallback(response){
        console.log(response.data)
        vm.situacao = response.data
      }, function errorCallback(response){

      })
    }

    $interval(getImports, 10000)

    // function atualizar(attribute, datasources){
    //   console.log(attribute)
    //   console.log(datasources)
    //   $http.post('backend/datasources/update.php', {
    //     datasources: datasources,
    //     attribute: attribute
    //   }).then(function successCallback(response){
    //     $route.reload();
    //   }, function errorCallback(response){
    //     console.log(response.data)
    //   })
    // }
    //
    // function getFontesAttributes(id){
    //   $http.post('backend/contracts_attribute/index.php',{
    //     id:id
    //   }).then(function successCallback(response){
    //     console.log(response.data)
    //     var objNovo = {};
    //
    //     for(var i in response.data){
    //     	objNovo[response.data[i].attribute_name] = parseInt(response.data[i].column_number)
    //
    //     }
    //     $scope.attribute = objNovo;
    //
    //
    //   })
    // }
    //
    // function getFonteByID(id){
    //   console.log(id)
    //   $http.post('backend/datasources/index.php',{
    //     'id':id
    //   }).then(function successCallback(response) {
    //     console.log(response.data)
    //     response.data[0].header_lines = parseInt(response.data[0].header_lines)
    //     $scope.datasources = response.data[0];
    //     // vm.fontes = response.data;
    //   }, function errorCallback(response) {
    //
    //   })
    //
    // }
    //
    // if(typeof $routeParams.id == 'string'){
    //   console.log($routeParams.id)
    //   $scope.datasource = $routeParams.id;
    //   getFontesAttributes($routeParams.id);
    //   getFonteByID($routeParams.id)
    // }

  }
})()
