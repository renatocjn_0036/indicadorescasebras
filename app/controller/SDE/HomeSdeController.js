(function (){
  'use strict';

  angular.module("app").controller("HomeSDEController", HomeSDEController);

  function HomeSDEController(cardservice, permissao) {

      var vm = this;
      vm.cards = [];
      vm.verificarPermissao = verificarPermissao
      
      getCardSecundario();

      function getCardSecundario(){
        return cardservice.get(3).then(function(data){
          console.log(data)
          vm.cards = data
        })
      }

      function verificarPermissao(rota){
        if(permissao == 'admin'){
          return true
        }else{
          if(permissao.indexOf(rota) == -1){
            return false
          }else{
            return true
          }
        }
      }


  }
})();
