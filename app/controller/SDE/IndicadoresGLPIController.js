app.controller("IndicadoresGLPIController", function($scope, $http, ChartService, UltimaImportacao) {

  getImportacao = function() {
    UltimaImportacao.getDate('glpi').then(function(data) {
      $scope.ultima_importacao = data
    })
  }

  getImportacao()

  $scope.filtros_uf = [];
  $scope.filtros = [];


  $scope.get_agrupamento = function(agrupamento) {
    console.log($scope.filtrosUFSelecionados)
    $http.post('backend/indicadores/glpi/get_agrupamento_sde.php', {
      agrupamento: agrupamento,
      filtros_uf: $scope.filtrosUFSelecionados
    }).then(function successCallBack(response) {
      $scope.filtros = response.data;
    }, function errorCallBack(response) {
      console.log(response)
    })
  }

  $scope.get_agrupamento_uf = function() {

    $http.post('backend/indicadores/glpi/get_agrupamento_sde.php', {
      agrupamento: 'uf'
    }).then(function successCallBack(response) {
      $scope.filtros_uf = response.data;
    }, function errorCallBack(response) {
      console.log(response)
    })
  }
  $scope.get_agrupamento_uf();

  $scope.get_IPT_agrupado = function(indicadores, myForm) {
    $http.post('backend/indicadores/glpi/indicadores_ipt_agrupado.php', {
      indicadores: indicadores,
      filtrosUnidade: $scope.filtrosSelecionados,
      filtrosUF: $scope.filtrosUFSelecionados
    }).then(function successCallBack(response) {
      console.log(response.data)
      $scope.char_criacao_de_usuario = ChartService.getChart('Criação de Usuário', '#81c868');
      $scope.char_bloqueio_de_usuario = ChartService.getChart('Bloqueio de Usuário', '#f05050');
      $scope.char_reinicializacao = ChartService.getChart('Reinicialização', '#6e8cd7');
      $scope.char_alteracao_de_permissao = ChartService.getChart('Alteração de Permissão', '#4c5667');
      $scope.char_desbloqueio_de_usuario = ChartService.getChart('Desbloqueio de Usuário', '#ffbd4a');

      angular.forEach(response.data, function(value, key) {

        if (value['CRIACAO DE USUARIO']) {
          if (value['perc_criacao'] == undefined) {

            value['perc_criacao'] = null;
          }
          var rows = {
            c: [{
                v: key
              },
              {
                v: value['perc_criacao']
              },
              {
                v: value['perc_criacao']
              },
              {
                v: "<table class=\"tooltipChart\"><tr><td colspan=\"3\" style=\"margin-left: 5px\"><strong>" + key + "</strong></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #81c868;margin-left: 5px\"></i> Criação</td><td ><strong>" + value['CRIACAO DE USUARIO'] + "</strong></td></td></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #f9cb9c;margin-left: 5px\"></i> Total Criação</td><td style=\"padding: 0px 20px;\"><strong>" + value['total_criacao'] + "</strong></td></tr></table>"
              }
            ]
          }

          $scope.char_criacao_de_usuario.data.rows.push(rows);
        }

        if (value['BLOQUEIO DE USUARIO']) {
          if (value['perc_bloqueio'] == undefined) {

            value['perc_bloqueio'] = null;
          }
          var rows = {
            c: [{
                v: key
              },
              {
                v: value['perc_bloqueio']
              },
              {
                v: value['perc_bloqueio']
              },
              {
                v: "<table class=\"tooltipChart\"><tr><td colspan=\"3\" style=\"margin-left: 5px\"><strong>" + key + "</strong></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #f05050;margin-left: 5px\"></i> Bloqueio</td><td ><strong>" + value['BLOQUEIO DE USUARIO'] + "</strong></td></td></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #f9cb9c;margin-left: 5px\"></i> Total Bloqueio</td><td style=\"padding: 0px 20px;\"><strong>" + value['total_bloqueio'] + "</strong></td></tr></table>"
              }
            ]
          }

          $scope.char_bloqueio_de_usuario.data.rows.push(rows);
        }

        if (value['DESBLOQUEIO DE USUARIO']) {
          if (value['perc_desbloqueio'] == undefined) {

            value['perc_desbloqueio'] = null;
          }
          var rows = {
            c: [{
                v: key
              },
              {
                v: value['perc_desbloqueio']
              },
              {
                v: value['perc_desbloqueio']
              },
              {
                v: "<table class=\"tooltipChart\"><tr><td colspan=\"3\" style=\"margin-left: 5px\"><strong>" + key + "</strong></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #ffbd4a;margin-left: 5px\"></i> Desbloqueio</td><td ><strong>" + value['DESBLOQUEIO DE USUARIO'] + "</strong></td></td></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #f9cb9c;margin-left: 5px\"></i> Total Desbloqueio</td><td style=\"padding: 0px 20px;\"><strong>" + value['total_desbloqueio'] + "</strong></td></tr></table>"
              }
            ]
          }

          $scope.char_desbloqueio_de_usuario.data.rows.push(rows);
        }

        if (value['REINICIALIZACAO']) {
          if (value['perc_reinicializacao'] == undefined) {

            value['perc_reinicializacao'] = null;
          }
          var rows = {
            c: [{
                v: key
              },
              {
                v: value['perc_reinicializacao']
              },
              {
                v: value['perc_reinicializacao']
              },
              {
                v: "<table class=\"tooltipChart\"><tr><td colspan=\"3\" style=\"margin-left: 5px\"><strong>" + key + "</strong></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #6e8cd7;margin-left: 5px\"></i> Reinicialização</td><td ><strong>" + value['REINICIALIZACAO'] + "</strong></td></td></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #f9cb9c;margin-left: 5px\"></i> Total Reinicialização</td><td style=\"padding: 0px 20px;\"><strong>" + value['total_reinicializacao'] + "</strong></td></tr></table>"
              }
            ]
          }

          $scope.char_reinicializacao.data.rows.push(rows);
        }

        if (value['ALTERACAO DE PERMISSAO']) {
          if (value['perc_permissao'] == undefined) {

            value['perc_permissao'] = null;
          }
          var rows = {
            c: [{
                v: key
              },
              {
                v: value['perc_permissao']
              },
              {
                v: value['perc_permissao']
              },
              {
                v: "<table class=\"tooltipChart\"><tr><td colspan=\"3\" style=\"margin-left: 5px\"><strong>" + key + "</strong></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #4c5667;margin-left: 5px\"></i> Permissão</td><td ><strong>" + value['ALTERACAO DE PERMISSAO'] + "</strong></td></td></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #f9cb9c;margin-left: 5px\"></i> Total Permissão</td><td style=\"padding: 0px 20px;\"><strong>" + value['total_permissao'] + "</strong></td></tr></table>"
              }
            ]
          }

          $scope.char_alteracao_de_permissao.data.rows.push(rows);
        }

      })

      console.log($scope.char_criacao_de_usuario);

    }, function errorCallBack(response) {

    })
  }


  $scope.get_IPT_acumulado = function(indicadores, myForm) {
    $http.post('backend/indicadores/glpi/indicadores_ipt_acumulado.php', {
      indicadores: indicadores
    }).then(function successCallBack(response) {
      console.log(response.data)
      $scope.myChartObjectPie = {};
      $scope.myChartObjectPie.type = "PieChart";
      $scope.myChartObjectPie.data = {
        "cols": [{
            id: "t",
            label: "Topping",
            type: "string"
          },
          {
            id: "s",
            label: "Slices",
            type: "number"
          }
        ],
        "rows": []

      };
      $scope.myChartObjectPie.options = {
        "colors": ['#4c5667', '#f05050', '#81c868', '#ffbd4a', '#6e8cd7'],
        chartArea: {
          width: '80%',
          height: '80%'
        },
        'animation': {
          'duration': 1000,
          'easing': 'out',
          'startup': true
        },
      };
      angular.forEach(response.data, function(value, key) {
        console.log(value.subcategoria)
        var rows = {
          c: [{
            v: value.subcategoria
          }, {
            v: value.perc
          }]
        }
        $scope.myChartObjectPie.data.rows.push(rows);
      })


    }, function errorCallBack(response) {
      console.log(response)
    })
  }
});
