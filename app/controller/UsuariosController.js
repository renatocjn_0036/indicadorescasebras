(function (){
  'use strict';

  angular.module("app").controller("UsuariosController", UsuariosController);

  function UsuariosController($location,$routeParams, $http, $scope, $timeout, permissao){

    var vm = this;
    vm.verificarPermissao = verificarPermissao
    vm.usuarios_cadastrados= [];
    vm.alterar_situacao = alterar_situacao;

    getUsuarios()

    function verificarPermissao(rota){
      if(permissao == 'admin'){
        return true
      }else{
        if(permissao.indexOf(rota) == -1){
          return false
        }else{
          return true
        }
      }
    }

    function alterar_situacao(usuario){
      console.log(usuario)
      $http.post('backend/users/alterar_situacao.php', {
        usuario: usuario
      }).then(function successCallBack(response){
        console.log(response)
        if(response.data.retorno == true){
          getUsuarios();
        }
      }, function errorCallBack(response){
        console.log(response.data)
      })
    }


    function getUsuarios(){
      $http.get('backend/users/get_users.php').then(function successCallback(response){
        console.log(response.data)
          vm.usuarios_cadastrados = response.data.dados
      }, function errorCallback(response){
        console.log(response.data)
      })
    }

  }
})();
