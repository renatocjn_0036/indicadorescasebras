(function() {
  'use strict';

  angular.module("app").controller("IndicadoresHarpiaController", IndicadoresHarpiaController);

  function IndicadoresHarpiaController($http, ChartService, UltimaImportacao, userservice,ChartContestacaoService) {
    var vm = this;

    vm.filtros = [];
    vm.get_filtro = get_filtro;
    vm.indicadores = indicadores;

    getImportacao();
    getUF();
    getBancos();

    function getUF() {
      return userservice.getFilial().then(function(data) {
        console.log(data)
        vm.ufs = data;
        vm.ufs.push({
          'filial': 'TODOS'
        })
      })
    }

    function getBancos() {
      return userservice.getBancos().then(function(data) {
        console.log(data)
        vm.bancos = data
        vm.bancos.push({
          'devname': 'TODOS'
        })
      })
    }

    function getImportacao() {
      UltimaImportacao.getDate('harpia').then(function(data) {
        vm.ultima_importacao = data
      })
    }

    function get_filtro(agrupamento) {

      $http.post('backend/indicadores/harpia/get_agrupamento_harpia.php', {
        agrupamento: agrupamento,
      }).then(function successCallBack(response) {
        vm.filtros = response.data;

      }, function errorCallBack(response) {
        console.log(response)
      })
    }

    function indicadores(indicador, myForm) {
      if (myForm.$invalid) {

        vm.form = myForm

      } else {

        $http.post('backend/indicadores/harpia/indicadores_agrupados.php', {
          indicador: indicador,
        }).then(function successCallBack(response) {

          vm.chart_uf = ChartContestacaoService.getChart('APROVADOS', 'PENDENTE', 'RECUSADO');
          vm.chart_banco = ChartContestacaoService.getChart('APROVADOS', 'PENDENTE', 'RECUSADO');


          angular.forEach(response.data.array_uf, function(value, key) {

              vm.chart_uf.data.rows.push(gerar_grafico(value))
          })

          angular.forEach(response.data.array_banco, function(value, key) {

              vm.chart_banco.data.rows.push(gerar_grafico(value))
          })

          vm.total_aprovados = calculo_perc(response.data.total_aprovados, response.data.total)
          vm.total_pendentes = calculo_perc(response.data.total_pendentes, response.data.total)
          vm.total_recusados = calculo_perc(response.data.total_recusados, response.data.total)
        }, function errorCallBack(response) {
          if (response.status == 404) {
            $scope.agrupados404 = true;
          }
        })
      }
    }

    function gerar_grafico(chart) {
      var rows = {
        c: [{
            v: chart.row
          },
          {
            v: chart.aprovado != 0 ? parseInt(chart.aprovado) : null
          },
          {
            v: chart.aprovado != 0 ? parseInt(chart.aprovado) : null
          },
          {
            v: chart.pendente != 0 ? parseInt(chart.pendente) : null
          },
          {
            v: chart.pendente != 0 ? parseInt(chart.pendente) : null
          },
          {
            v: chart.recusado != 0 ? parseInt(chart.recusado) : null
          },
          {
            v: chart.recusado != 0 ? parseInt(chart.recusado) : null
          }
        ]
      }

      return rows;

    }

    function calculo_perc(valor1, total){
      if(valor1 > 0){
        var valor_final = parseInt(valor1)/parseInt(total)*100
        return valor_final.toFixed(2)
      }else{
        return null
      }
    }

  }
})()
