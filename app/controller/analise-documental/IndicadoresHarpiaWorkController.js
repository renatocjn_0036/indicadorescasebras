(function() {

  angular.module('app').controller('IndicadoresHarpiaWorkController', IndicadoresHarpiaWorkController);

  function IndicadoresHarpiaWorkController($http, ChartService, UltimaImportacao, userservice, ChartContestacaoService) {
    var vm = this;
    vm.total_work = 0;
    vm.total_harpia = 0;
    vm.total_aprovados = 0;
    vm.total_pendentes = 0;
    vm.total_recusados = 0;
    vm.filtros = [];
    vm.get_filtro = get_filtro;
    vm.indicadores = indicadores;

    getImportacao();
    getUF();
    getBancos();

    function getUF() {
      return userservice.getFilial().then(function(data) {
        console.log(data)
        vm.ufs = data;
        vm.ufs.push({
          'filial': 'TODOS'
        })
      })
    }

    function getBancos() {
      return userservice.getBancos().then(function(data) {
        console.log(data)
        vm.bancos = data
        vm.bancos.push({
          'devname': 'TODOS'
        })
      })
    }

    function getImportacao() {
      UltimaImportacao.getDate('workbank').then(function(data) {
        vm.ultima_importacao = data
      })
    }

    function get_filtro(agrupamento) {
      $http.post('backend/indicadores/harpia/get_agrupamento_harpia.php', {
        agrupamento: agrupamento,
      }).then(function successCallBack(response) {
        vm.filtros = response.data;

      }, function errorCallBack(response) {
        console.log(response)
      })
    }

    function indicadores(indicador, myForm) {
      if (myForm.$invalid) {
        vm.form = myForm
      } else {
        $http.post('backend/indicadores/harpiawork/indicadores_agrupados.php', {
          indicador: indicador,
        }).then(function successCallBack(response) {
          console.log(response.data);
          vm.total_work = response.data.total_work.total
          // vm.total_aprovados = calculo_perc
          vm.chart_uf = getChart('APROVADOS', 'PENDENTE', 'RECUSADO');
          vm.chart_banco = getChart('APROVADOS', 'PENDENTE', 'RECUSADO');

          angular.forEach(response.data.total_harpia_situacao, function(value, key){
              vm.total_harpia += parseInt(value.total);
            if(value.situacao == 'APROVADO'){
              vm.total_aprovados = calculo_perc(value.total, response.data.total_work.total)
            }
            if(value.situacao == 'PENDENTE'){
              vm.total_pendentes = calculo_perc(value.total, response.data.total_work.total)
            }
            if(value.situacao == 'RECUSADO'){
              vm.total_recusado = calculo_perc(value.total, response.data.total_work.total)
            }
          })

          angular.forEach(response.data.array_uf, function(value, key) {

            vm.chart_uf.data.rows.push(gerar_grafico(value))
          })

          angular.forEach(response.data.array_banco, function(value, key) {

            vm.chart_banco.data.rows.push(gerar_grafico(value))
          })
          vm.chart_acumulado = getChartGauge(calculo_perc(vm.total_harpia,vm.total_work))

        }, function errorCallBack(response) {
          if (response.status == 404) {
            $scope.agrupados404 = true;
          }
        })
      }
    }

    function getChartGauge(valor){
      return  {
        type: "Gauge",
        options: {
          colors: ['#81c868', '#f05050', '#ffbd4a'],
          width: '100%',
          height: '80%',
          redFrom: 0,
          redTo: 25,
          yellowFrom: 25,
          yellowTo: 75,
          greenFrom: 75,
          greenTo: 100,
          minorTicks: 5,
          animation: {
            duration: 1000,
            easing: 'in',
            startup: true
          }
        },
        data: [
          ['Label', 'Value'],
          ['Contratos', parseFloat(valor)]
        ]
      };
  }

    function gerar_grafico(chart) {

      var rows = {
        c: [{
            v: chart.row
          },
          {
            v: calculo_perc(chart.aprovado, chart.total_work)
          },
          {
            v: calculo_perc(chart.aprovado, chart.total_work)
          },
          {
            v: calculo_perc(chart.pendente, chart.total_work)
          },
          {
            v: calculo_perc(chart.pendente, chart.total_work)
          },
          {
            v: calculo_perc(chart.recusado, chart.total_work)
          },
          {
            v: calculo_perc(chart.recusado, chart.total_work)
          }
        ]
      }

      return rows;


    }

    function calculo_perc(valor1, total) {
        var valor_final = parseInt(valor1) / parseInt(total) * 100
        return valor_final.toFixed(2)
    }

    function getChart(label_1, label_2, label_3) {
      return {
        type: "ColumnChart",
        data: {
          cols: [{
              id: "t",
              label: "Topping",
              type: "string"
            },
            {
              id: "s",
              label: label_1,
              type: "number"
            },
            {
              id: "s",
              type: "number",
              role: "annotation"
            }, {
              id: "s",
              label: label_2,
              type: "number"
            },
            {
              id: "s",
              type: "number",
              role: "annotation"
            },
            {
              id: "s",
              label: label_3,
              type: "number"
            },
            {
              id: "s",
              type: "number",
              role: "annotation"
            },
          ],
          rows: []
        },
        options: {
          colors: ['#81c868', '#f90', '#f05050'],
          chartArea: {
            width: '90%',
            height: '80%'
          },
          isStacked: false,
          legend: {
            position: 'top',
            alignment: 'center'
          },
          animation: {
            duration: 1000,
            easing: 'out',
            startup: false
          },
          annotations: {
            textStyle: {
              bold: true,
              color: 'black'
            },
            alwaysOutside: true
          },
        },
        vAxis: {
          format: '#\'%\'',
        },
        formatters: {
          number: [{
              columnNum: 1,
              suffix: '%'
            },
            {
              columnNum: 2,
              suffix: '%'
            },
          ]
        }
      }
    }


  }

})();
