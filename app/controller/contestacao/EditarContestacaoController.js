(function() {
  'use strict';

  angular.module("app").controller("EditarContestacaoController", EditarContestacaoController);

  function EditarContestacaoController($routeParams,  $scope, $route, contestacaoservice,userservice) {
    var vm = this;

    vm.remove_email = remove_email;
    vm.add_email = add_email;

    vm.lista_emails = [];
    vm.unidades = [];
    vm.bancos = [];
    vm.cadastrar = cadastrar;

    get_contestacao_by_id($routeParams.id);
    get_email($routeParams.id)
    getBancos();
    getUF();

    function getUF() {
      return userservice.getFilial().then(function(data){
        vm.unidades = data
      })
    }

    function getBancos() {
      return userservice.getBancos().then(function(data){
        vm.bancos = data
      })
    }

    function get_contestacao_by_id(id) {
      vm.error_pesquisa = false
      return contestacaoservice.getContestacao({
        'id': id
      }).then(function(data) {
        console.log(data)
        $scope.dados = data[0]
      })
    }

    function get_email(id) {

      return contestacaoservice.getEmail({
        'id': id
      }).then(function(data) {
        if (data.length > 0) {
          vm.lista_emails = data
        }
      })
    }

    function cadastrar(dados, myForm) {
      console.log(dados)
      if (myForm.$invalid) {
        vm.form = myForm
      } else {
        return contestacaoservice.postContestacao(dados, vm.lista_emails).then(function(data) {
          if (data.retorno == false) {
            vm.error_msg = response.data.msg
            vm.error_form = true;
          } else {
            // delete $scope.dados
            // delete vm.lista_emails
            vm.success_msg =data.msg
            vm.success_form = true
          }
        })
      }
    }

    function add_email(email) {
      vm.lista_emails.push({
        'email': email,
        'done': false
      });
      $scope.email = '';
    }

    function remove_email(index) {
      vm.lista_emails.splice(index, 1);
    }

    alert('ok')

  }

})();
