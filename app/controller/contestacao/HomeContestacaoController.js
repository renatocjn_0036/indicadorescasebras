(function (){
  'use strict';

  angular.module("app").controller("HomeContestacaoController", HomeContestacaoController);

  function HomeContestacaoController(contestacaoservice,cardservice, permissao) {

      var vm = this;

      vm.total_atrasados = 0;
      vm.total_contestacoes = 0;
      vm.percContestacao = percContestacao;
      vm.verificarPermissao = verificarPermissao
      vm.cards = [];

      getInformacaoCard();
      getCardSecundario();

      function getCardSecundario(){
        return cardservice.get(8).then(function(data){
          console.log(data)
          vm.cards = data
        })
      }

      function getInformacaoCard() {
        return contestacaoservice.getContestacao().then(function(data) {
          console.log(data)
          vm.total_contestacoes = data.length
          angular.forEach(data, function(value, key) {
            if (parseInt(value.horas) >= 24 && value.situacao == 0) {
              vm.total_atrasados++
            }
          })
          return data;
        })
      }

      function percContestacao(){
        var perc = vm.total_atrasados/vm.total_contestacoes*100;
        return perc.toFixed(2);
      }

      function verificarPermissao(rota){
        if(permissao == 'admin'){
          return true
        }else{
          if(permissao.indexOf(rota) == -1){
            return false
          }else{
            return true
          }
        }
      }

  }
})();
