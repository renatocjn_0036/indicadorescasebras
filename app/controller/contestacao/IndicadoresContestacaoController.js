(function() {
  'use strict';

  angular.module("app").controller("IndicadoresContestacaoController", IndicadoresContestacaoController);

  function IndicadoresContestacaoController($http, $scope, $route, $uibModal, userservice, ChartContestacaoService) {
    var vm = this;
    vm.indicadores = indicadores;
    vm.changeChart = changeChart;

    vm.ufs = [];
    vm.bancos = [];

    getUF();
    getBancos();

    $scope.tipo = {
      name : 'Ambos'
    }

    function changeChart(tipo){
      vm.myChartUF = chartColumn(vm.dados_chart_uf,vm.dados_chart_uf_ambos, tipo)
    }

    function getUF() {
      return userservice.getFilial().then(function(data) {
        vm.ufs = data;
        vm.ufs.push({
          'filial': 'TODOS'
        })
      })
    }

    function getBancos() {
      return userservice.getBancos().then(function(data) {

        vm.bancos = data
        vm.bancos.push({
          'devname': 'TODOS'
        })
      })
    }


    function indicadores(indicador, form) {

      $http.post('backend/contestacao/indicador.php', {
        indicador: indicador
      }).then(function successCallback(response) {
        console.log(response.data)

        vm.total_documental = calculo_perc(response.data.array_total['Documental'], response.data.array_total['total']);
        vm.total_contestacao = calculo_perc(response.data.array_total['Contestacao'], response.data.array_total['total']);
        vm.total_aguardando_retorno = (typeof response.data.array_total[0] != 'undefined') ? calculo_perc(response.data.array_total[0], response.data.array_total['total']) : 0;
        vm.total_concluido = (typeof response.data.array_total[1] != 'undefined') ? calculo_perc(response.data.array_total[1], response.data.array_total['total']) : 0
        vm.total_concluido_sem_retorno = (typeof response.data.array_total[2] != 'undefined') ? calculo_perc(response.data.array_total[2], response.data.array_total['total']) : 0;


        vm.chart_laudo = ChartContestacaoService.getChartLaudo('IMPROCEDENTE', 'PROCEDENTE');
        vm.situacao_x_laudo = ChartContestacaoService.getChartLaudo('IMPROCEDENTE', 'PROCEDENTE');
        // vm.chart_documental = ChartContestacaoService.getChart('CONCLUÍDO', 'AGUARDANDO RETORNO', 'CONCLUÍDO SEM RETORNO');
        // vm.chart_contestacao = ChartContestacaoService.getChart('CONCLUÍDO', 'AGUARDANDO RETORNO', 'CONCLUÍDO SEM RETORNO');
        // vm.chartTodos = ChartContestacaoService.getChart('CONCLUÍDO', 'AGUARDANDO RETORNO', 'CONCLUÍDO SEM RETORNO');
        vm.chart_bancos = ChartContestacaoService.getChart('CONCLUÍDO', 'AGUARDANDO RETORNO', 'CONCLUÍDO SEM RETORNO');
        vm.myChartUF = chartColumn(response.data.array_uf,response.data.array_uf_ambos, 'Ambos');
        vm.dados_chart_uf = response.data.array_uf
        vm.dados_chart_uf_ambos = response.data.array_uf_ambos

        vm.myChartObjectPie = {
          type : "PieChart",
          data : {
            cols: [{
                id: "t",
                label: "Topping",
                type: "string"
              },
              {
                id: "s",
                label: "Slices",
                type: "number"
              }
            ],
            rows: []
          },
          options : {
            chartArea: {
              width: '80%',
              height: '80%'
            }
          },

        };

        vm.myChartObjectPieLaudo = {
          type : "PieChart",
          data : {
            cols: [{
                id: "t",
                label: "Topping",
                type: "string"
              },
              {
                id: "s",
                label: "Slices",
                type: "number"
              }
            ],
            rows: []
          },
          options : {
            chartArea: {
              width: '80%',
              height: '80%'
            }
          },

        };

        angular.forEach(response.data.array_banco, function(value, key) {
            vm.chart_bancos.data.rows.push(gerar_grafico(value))
        })

        angular.forEach(response.data.array_laudo, function(value, key) {
            vm.chart_laudo.data.rows.push(gerar_grafico_laudo(value))
        })

        angular.forEach(response.data.array_situacao_x_laudo, function(value, key) {
            vm.situacao_x_laudo.data.rows.push(gerar_grafico_laudo(value))
        })


        angular.forEach(response.data.array_banco_acumulado, function(value, key) {

          var rows = {
            c: [
              {v: value.banco},
              {v: parseInt(value.total)}]
          }
          vm.myChartObjectPie.data.rows.push(rows);
        })

        angular.forEach(response.data.array_laudo_acumulado, function(value, key) {
          if(value.laudo != null){
            var rows = {
              c: [
                {v: value.laudo},
                {v: parseInt(value.total)}]
            }
            vm.myChartObjectPieLaudo.data.rows.push(rows);
          }
        })

      }, function errorCallback(response) {

      })
    }

    function chartColumn(dados,dados_ambos, tipo){

      vm.myChart = ChartContestacaoService.getChart('CONCLUÍDO', 'AGUARDANDO RETORNO', 'CONCLUÍDO SEM RETORNO');

      if(tipo == 'Ambos'){
        angular.forEach(dados_ambos, function(value, key) {
          console.log(value)
          vm.myChart.data.rows.push(gerar_grafico(value))
        })
      }else{

        angular.forEach(dados, function(value, key) {
          if(value.tipo == tipo){
            vm.myChart.data.rows.push(gerar_grafico(value))
          }
        })

      }

      return vm.myChart;
    }

    function gerar_grafico(chart) {
      var rows = {
        c: [{
            v: chart.row
          },
          {
            v: parseInt(chart.concluido)
          },
          {
            v: parseInt(chart.concluido)
          },
          {
            v: parseInt(chart.aguardando_retorno)
          },
          {
            v: parseInt(chart.aguardando_retorno)
          },
          {
            v: parseInt(chart.concluido_sem_retorno)
          },
          {
            v: parseInt(chart.concluido_sem_retorno)
          }
        ]
      }

      return rows;

    }

    function gerar_grafico_laudo(chart) {

      var rows = {
        c: [{
            v: chart.row
          },
          {
            v: parseInt(chart.Improcedente)
          },
          {
            v: parseInt(chart.Improcedente)
          },
          {
            v: parseInt(chart.Procedente)
          },
          {
            v: parseInt(chart.Procedente)
          },
        ]
      }

      return rows;

    }

    function calculo_perc(valor1, total){
      var valor_final = valor1/total*100
      return valor_final.toFixed(2)
    }


  }
})()
