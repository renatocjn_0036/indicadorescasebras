(function() {
    'use strict';

    angular.module("app").controller("ListaContestacaoController", ListaContestacaoController);

    function ListaContestacaoController($location, $routeParams, $scope, $interval, $uibModal, contestacaoservice, userservice, permissao, $http) {

      var vm = this

      vm.contestacoes_cadastradas = []
      vm.unidades = []
      vm.bancos = []
      vm.lista_emails = []
      vm.filtros = {}
      vm.paginacao = []
      vm.produtos = []
      vm.ufs = []

      vm.pesquisar = pesquisar
      vm.open = open
      vm.openEmail = openEmail
      vm.excluir = excluir
      vm.laudo = laudo
      vm.open_cadastrar = open_cadastrar
      vm.verificarPermissao = verificarPermissao
      vm.gerar_excel = gerar_excel
      vm.getUnidades = getUnidades

      vm.num_registros = [
        {'valor': 10},
        {'valor': 50},
        {'valor': 100}
      ]

      vm.num_registro_atual = 10

      getUF()
      getBancos()
      getProdutos()

      function getUnidades(uf) {
        contestacaoservice.getUnidades(uf).then(function(data){
          vm.unidades = data
        })
      }

      function getProdutos() {
        return contestacaoservice.getProdutos().then(function(data){
          vm.produtos = data
        })
      }

      function verificarPermissao(rota){
        if(permissao == 'admin'){
          return true
        }else{
          if(permissao.indexOf(rota) == -1){
            return false
          }else{
            return true
          }
        }
      }

      function gerar_excel(){
       
          document.cookie ='filtros = '+ JSON.stringify(vm.filtros)
          window.open('http://10.30.0.157/indicadorescasebras/backend/contestacao/excel.php')
        // return contestacaoservice.gerarExcel(vm.filtros).then(function(data){
        //   console.log(data)
        // })
        // var array = remover_observacao();
        // var xls = new XlsExport(array, 'teste');
        // xls.exportToXLS('contestacao.xls')

      }

      function remover_observacao(){
        var novo_array = [];
        angular.forEach(vm.contestacoes_cadastradas, function(value, key){
          delete value.observacao
          novo_array.push(value)
        })

        return novo_array
      }

      function getUF() {
        return userservice.getFilial().then(function(data){
          vm.ufs = data;
        })
      }

      function getBancos() {
        return userservice.getBancos().then(function(data){
          console.log(data)
          vm.bancos = data;
        })
      }


      function pesquisar(page, num_registros) {

        if (!page) page = 1;
        if(!num_registros) num_registros = 10;

        vm.error_pesquisa = false
        return contestacaoservice.getContestacao(vm.filtros,page, num_registros).then(function(data) {
          console.log(data.registros)
          vm.contestacoes_cadastradas = data.registros
          vm.paginacao = data.paginacao
        })
      }

      function excluir(item){
        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'view/modal/myModalExcluir.html',
          controllerAs: 'vm',
          controller: function($uibModalInstance){
            console.log(item)
            var vm = this;
            vm.ok = ok;
            vm.cancel = cancel;
            vm.title = 'Excluir Contestação';
            function ok(){
              $uibModalInstance.close(item);
            }
            function cancel(){
              $uibModalInstance.dismiss('cancel');
            }
          }
        });

        modalInstance.result.then(function(item) {

          contestacaoservice.deleteContestacao(item).then(function(data){
            if(data.retorno == true){
              vm.success = true;
              vm.msg_success = data.msg;
              vm.contestacoes_cadastradas.splice(vm.contestacoes_cadastradas.indexOf(item), 1);
            }
          })
        }, function() {
          console.log('Modal dismissed at: ' + new Date());
        });
      }

      function openEmail(item) {

        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'view/modal/myModalEmails.html',
          controller: 'myModalEmailController',
          controllerAs: 'vm',
          resolve: {
            item: function() {
              return item;
            }
          },
        });

        modalInstance.result.then(function(emails) {
            contestacaoservice.sendEmail(emails, item).then(function(data){})
            vm.success = true;
            vm.msg_success = 'E-mail Enviado!'

        }, function() {
          console.log('Modal dismissed at: ' + new Date());
        });

      }

      function open(item) {

        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'view/modal/myModalContent.html',
          controller: 'myModalController',
          controllerAs: 'vm',
          resolve: {
            item: function() {
              return item;
            }
          },
        });

        modalInstance.result.then(function(situacao) {
          contestacaoservice.updateContestacao(situacao).then(function(data){
              if (data.retorno == true) {
                for (var i = 0; i < vm.contestacoes_cadastradas.length; i++) {
                  if (vm.contestacoes_cadastradas[i].id == situacao.id) {
                    vm.contestacoes_cadastradas[i].situacao = situacao.tipo
                  }
                }
              }

              if(data.retorno_email != true){
                vm.error = true;
                vm.error_msg = 'Falha ao enviar o e-mail'
              }
          })

        }, function() {
          console.log('Modal dismissed at: ' + new Date());
        });

      }

      function laudo(item) {

        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'view/modal/myModalLaudoContent.html',
          controller: 'myModalLaudoController',
          controllerAs: 'vm',
          resolve: {
            item: function() {
              return item;
            }
          },
        });

        modalInstance.result.then(function(situacao) {
          console.log()
          contestacaoservice.updateContestacaoLaudo(situacao).then(function(data){
              if (data.retorno == true) {
                for (var i = 0; i < vm.contestacoes_cadastradas.length; i++) {
                  if (vm.contestacoes_cadastradas[i].id == situacao.id) {
                    vm.contestacoes_cadastradas[i].laudo = situacao.laudo
                  }
                }
              }
          })

        }, function() {
          console.log('Modal dismissed at: ' + new Date());
        });

      }

      function open_cadastrar(item) {

        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'view/contestacao/modal/CadastroContestacao.html',
          controller: 'CadastroContestacaoController',
          controllerAs: 'vm',
          size: 'lg',
          resolve: {
            item: function() {
              return item;
            }
          },
        });

        modalInstance.result.then(function(msg) {
          console.log(msg)
          pesquisar($scope.pesquisa)
          vm.success = true;
          vm.msg_success = msg

        }, function() {
          console.log('Modal dismissed at: ' + new Date());
        });

      }

      $scope.toggleAnimation = function() {
        $scope.animationsEnabled = !$scope.animationsEnabled;
      };

    }

    angular.module("app").controller("myModalController", myModalController);
    function myModalController($scope, $uibModalInstance, item) {
      var vm = this;
      vm.ok = ok;
      vm.cancel = cancel;
      vm.numero_contrato = item.numero_contrato;

      $scope.situacao = {
        tipo: item.situacao,
        id: item.id
      }

      function ok(situacao) {
        $uibModalInstance.close(situacao);
      }

      function cancel() {
        $uibModalInstance.dismiss('cancel');
      };

    }

    angular.module("app").controller("myModalLaudoController", myModalLaudoController);
    function myModalLaudoController($scope, $uibModalInstance, item) {
      var vm = this;
      vm.ok = ok;
      vm.cancel = cancel;
      vm.numero_contrato = item.numero_contrato;
      console.log(item)
      $scope.situacao = {
        laudo: item.laudo,
        id: item.id
      }

      function ok(situacao) {
        $uibModalInstance.close(situacao);
      }

      function cancel() {
        $uibModalInstance.dismiss('cancel');
      };

    }

    angular.module("app").controller("myModalEmailController", myModalEmailController);
    function myModalEmailController($scope, $uibModalInstance, item, contestacaoservice) {
      var vm = this;
      vm.ok = ok;
      vm.cancel = cancel;
      vm.remove_email = remove_email;
      vm.add_email = add_email;

      vm.lista_emails = [];
      getEmails(item)

      $scope.situacao = {
        tipo: item.situacao,
        id: item.id
      }

      function ok() {
        $uibModalInstance.close(vm.lista_emails);
      }

      function cancel() {
        $uibModalInstance.dismiss('cancel');
      };

      function remove_email(index) {
        vm.lista_emails.splice(index, 1);
      }

      function add_email(email) {
        vm.lista_emails.push({
          'email': email,
          'done': false
        });
        $scope.email = '';
      }

      function getEmails(item) {
        return contestacaoservice.getEmail(item).then(function(data){
          if (data.length > 0) {
            vm.lista_emails = data
          }
        })
      }
    }

    angular.module("app").controller("CadastroContestacaoController", CadastroContestacaoController);
    function CadastroContestacaoController($scope, $uibModalInstance, item, contestacaoservice,userservice) {

      var vm = this;
      vm.bancos = []
      vm.unidades = []
      vm.lista_emails = []
      vm.produtos = []
      vm.ufs = []

      vm.cadastrar = cadastrar
      vm.cancel = cancel
      vm.remove_email = remove_email
      vm.add_email = add_email
      vm.getUnidades = getUnidades
      

      if(typeof item == 'undefined'){
        vm.titulo = 'Cadastro'
      }else{
        $scope.dados = item
        getEmail(item.id)
        vm.titulo = 'Editar'
      }

      getBancos()
      getUF()
      getProdutos()

      function getUnidades(uf) {
        contestacaoservice.getUnidades(uf).then(function(data){
          console.log(data)
          vm.unidades = data
        })
      }

      function getProdutos() {
        return contestacaoservice.getProdutos().then(function(data){
          vm.produtos = data
        })
      }

      function getUF() {
        return userservice.getFilial().then(function(data){
          vm.ufs = data
        })
      }

      function getBancos() {
        return userservice.getBancos().then(function(data){
          vm.bancos = data
        })
      }

      function getEmail(id) {

        return contestacaoservice.getEmail({
          'id': id
        }).then(function(data) {
          if (data.length > 0) {
            vm.lista_emails = data
          }
        })
      }

      function add_email(email) {
        vm.lista_emails.push({
          'email': email,
          'done': false
        });
        $scope.email = '';
      }

      function remove_email(index) {
        vm.lista_emails.splice(index, 1);
      }

      function cadastrar(dados, myForm) {
        if (myForm.$invalid) {
          vm.form = myForm
        } else {
          return contestacaoservice.postContestacao(dados, vm.lista_emails).then(function(data){
              if (data.retorno == false) {
                vm.error_msg = data.msg
                vm.error_form = true;
              } else {
                  $uibModalInstance.close(data.msg);
                // vm.success_msg = data.msg
                // vm.success_form = true
                // vm.error_form = false;
                // delete $scope.dados
                // vm.lista_emails = [];
              }
          })
        }
      }

      function cancel() {
        $uibModalInstance.dismiss('cancel');
      };

    }

    })()
