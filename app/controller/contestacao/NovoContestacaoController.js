(function() {
  'use strict';

  angular.module("app").controller("NovoContestacaoController", NovoContestacaoController);

  function NovoContestacaoController($scope, contestacaoservice,userservice) {

    var vm = this
    vm.bancos = []
    vm.unidades = []
    vm.lista_emails = []
    vm.produtos = []

    vm.cadastrar = cadastrar
    vm.remove_email = remove_email
    vm.add_email = add_email

    getBancos()
    getUF()
    getProdutos()

    function getUF() {
      return userservice.getFilial().then(function(data){
        vm.unidades = data
      })
    }

    function getBancos() {
      return userservice.getBancos().then(function(data){
        vm.bancos = data
      })
    }

    function add_email(email) {
      vm.lista_emails.push({
        'email': email,
        'done': false
      });
      $scope.email = '';
    }

    function remove_email(index) {
      vm.lista_emails.splice(index, 1);
    }

    function cadastrar(dados, myForm) {
      console.log(dados)
      // if (myForm.$invalid) {
      //   vm.form = myForm
      // } else {
      //   return contestacaoservice.postContestacao(dados, vm.lista_emails).then(function(data){
      //       if (data.retorno == false) {
      //         vm.error_msg = data.msg
      //         vm.error_form = true;
      //       } else {
      //         vm.success_msg = data.msg
      //         vm.success_form = true
      //         vm.error_form = false;
      //         delete $scope.dados
      //         vm.lista_emails = [];
      //       }
      //   })
      // }
    }

  }


})()
