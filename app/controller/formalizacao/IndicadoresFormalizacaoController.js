(function() {
  'use-strict';

  angular.module("app").controller("IndicadoresFormalizacaoController", IndicadoresFormalizacaoController);

  function IndicadoresFormalizacaoController($http, UltimaImportacao, ChartService, userservice) {
    var vm = this;
    vm.bancos = [];
    vm.ufs = [];
    vm.unidades = [];

    vm.indicadores = indicadores;
    vm.getUnidades = getUnidades;
    vm.indicadorAproveiramento = indicadorAproveiramento;

    getImportacao();
    getUF();
    getBancos();

    vm.myChartGauge = getChartGauge('Aproveitamento', 0);
    vm.myChartGaugeParcial = getChartGauge('Parcial', 0);
    vm.myChartGaugeCompleta = getChartGauge('Completa', 0);

    function getImportacao() {
      UltimaImportacao.getDate('pendencia').then(function(data) {
        vm.ultima_importacao = data
      })
    }

    function getUF() {
      return userservice.getFilial().then(function(data) {
        console.log(data)
        vm.ufs = data;
        vm.ufs.push({
          'filial': 'TODOS'
        })
      })
    }

    function getBancos() {
      return userservice.getBancos().then(function(data) {
        console.log(data)
        vm.bancos = data
        vm.bancos.push({
          'devname': 'TODOS'
        })
      })
    }

    function indicadorAproveiramento(indicador){
      $http.post('backend/indicadores/formalizacao/aproveitamento.php', {
        indicador:indicador
      }).then(function successCallBack(response){
        console.log(response.data)
          vm.myChartGauge = getChartGauge("Aproveitamento", response.data.indicador_aproveitamento);
      }, function errorCallBack(response){

      })
    }

    function indicadores(indicador, myForm) {
      vm.loading = true;
      console.log(indicador)
      $http.post('backend/indicadores/formalizacao/indicadores_agrupados.php', {
        indicador: indicador
      }).then(function successCallBack(response) {
        console.log(response.data)

        vm.myChartGaugeParcial = getChartGauge("Parcial", response.data.indicador_pendencia.perc_pendencia_parcial);
        vm.myChartGaugeCompleta = getChartGauge("Completa", response.data.indicador_pendencia.perc_pendencia_completa);
        vm.myChartColumnUF = getChartColumn("Parcial", "Completa", response.data.indicador_uf);
        vm.myChartColumnBanco = getChartColumn("Parcial", "Completa", response.data.indicador_banco);
        vm.myChartPie = getChatPie(response.data.indicador_pendencia_work);
        vm.zero_trinta = response.data.dias_atraso.zero_trinta;
        vm.trinta_quarenta = response.data.dias_atraso.trinta_quarenta;
        vm.quarenta_noventa = response.data.dias_atraso.quarenta_noventa;
        vm.maisnoventa = response.data.dias_atraso.maisnoventa;
        vm.analisados = response.data.indicador_aproveitamento.total;
        vm.pagos = response.data.indicador_aproveitamento.total_pagos;
        vm.pendentes = response.data.indicador_pendencia.total_pendencia;
        vm.pendentes_parcial = response.data.indicador_pendencia.total_parcial
        vm.pendentes_completa = response.data.indicador_pendencia.total_completa
        vm.loading = false;
      }, function errorCallBack(response) {
        console.log(response)
      })
    }

    function getUnidades(ind) {
      if ((typeof ind != 'undefined' && ind.indexOf('TODOS') == -1) || (typeof ind != 'undefined' && ind.length == 0)) {

        return userservice.getUnidades(ind).then(function(data) {
          console.log(data)
          vm.unidades = data
        })

      } else {
        vm.unidades = [];
      }
    }

    function getChartColumn(label_1, label_2, array) {
      vm.myChart = {
        type: "ColumnChart",
        data: {
          cols: [{
              id: "t",
              label: "Topping",
              type: "string"
            },
            {
              id: "s",
              label: label_1,
              type: "number"
            },
            {
              id: "s",
              type: "number",
              role: "annotation"
            }, {
              id: "s",
              label: label_2,
              type: "number"
            },
            {
              id: "s",
              type: "number",
              role: "annotation"
            }

          ],
          rows: []
        },
        options: {
          colors: ['#81c868', '#f90'],
          chartArea: {
            width: '90%',
            height: '80%'
          },
          isStacked: false,
          legend: {
            position: 'top',
            alignment: 'center'
          },
          animation: {
            duration: 1000,
            easing: 'out',
            startup: false
          },
          annotations: {
            textStyle: {
              bold: true,
              color: 'black'
            },
            alwaysOutside: true
          },
        },

      }

      angular.forEach(array, function(value, key) {
        vm.myChart.data.rows.push(gerar_grafico(value))
      })

      return vm.myChart

    }

    function getChartGauge(titulo, valor) {
      return {
        type: "Gauge",
        options: {
          colors: ['#81c868', '#f05050', '#ffbd4a'],
          width: '100%',
          height: '55%',
          redFrom: 0,
          redTo: 25,
          yellowFrom: 25,
          yellowTo: 75,
          greenFrom: 75,
          greenTo: 100,
          minorTicks: 5,
          animation: {
            duration: 1000,
            easing: 'in',
            startup: true
          }
        },
        data: [
          ['Label', 'Value'],
          [titulo, parseFloat(valor)]
        ]
      };
    }

    function gerar_grafico(chart) {

      var rows = {
        c: [{
            v: chart.row
          },
          {
            v: chart.parcial
          },
          {
            v: chart.parcial
          },
          {
            v: chart.completa
          },
          {
            v: chart.completa
          },
        ]
      }

      return rows;

    }

    function getChatPie(array) {

      vm.myChartObjectPie = {
        type: "PieChart",
        data: {
          cols: [{
              id: "t",
              label: "Topping",
              type: "string"
            },
            {
              id: "s",
              label: "Slices",
              type: "number"
            }
          ],
          rows: []

        },
        options : {
          colors: ['#dc3912', '#36c', '#f90', '#ffbd4a', '#6e8cd7'],
          animation: {
            duration: 1000,
            easing: 'out',
            startup: true
          },
        }
      };

      angular.forEach(array, function(value, key) {

        var rows = {
          c: [{
            v: value.row
          }, {
            v: value.total
          }]
        }
        vm.myChartObjectPie.data.rows.push(rows);
      })
      return vm.myChartObjectPie;

    }

  }
})()
