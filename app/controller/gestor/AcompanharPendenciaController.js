(function (){
  'use strict';

  angular.module("app").controller("AcompanharPendenciaController", AcompanharPendenciaController);

  function AcompanharPendenciaController($http,userservice,gestorpendenciaservice,NgTableParams) {

      var vm = this;
      vm.pesquisar = pesquisar;
      vm.totalSelecionados = totalSelecionados;
      vm.baixaPendencia = baixaPendencia;

      vm.situacao = [];
      vm.pendencias = [];

      getSituacao();
      getUF();
      getBancos();

      function baixaPendencia(pendencia){
        vm.success = false
        vm.error = false
        return gestorpendenciaservice.updatePendencia(vm.contratosSelecionados, pendencia).then(function (data){
          if(data.retorno == true){
            vm.success = true;
            vm.msg_success = data.count + " Contrato(s) Atualizados"
          }else{
            vm.error = true;
          }
        })
      }

      function getSituacao(){
        $http.get('backend/datasources/get_situacao.php').then(function successCallback(response){
          console.log(response.data)
          vm.situacao = response.data
        }, function errorCallback(response){

        })
      }

      function getUF() {
        return userservice.getFilial().then(function(data){
          vm.unidades = data;
        })
      }

      function getBancos() {
        return userservice.getBancos().then(function(data){
          vm.bancos = data;
        })
      }

      function pesquisar(pesquisa){
        vm.success = false
        vm.error = false
        vm.loading = true;
        return gestorpendenciaservice.getPendencia(pesquisa).then(function(data){
          if(data.length > 0){
            vm.pendencia_pesquisa = data[0].id_tipo_pendencia;
            vm.pendencias = data
            vm.tableParams = new NgTableParams({}, { dataset: data});
          }
          vm.loading = false;
        })
      }

      function totalSelecionados(){
        if(typeof vm.tableParams != 'undefined'){
          vm.contratosSelecionados = vm.tableParams.data.filter(function(contrato) {
            if (contrato.selecionado) return contrato;
          });

          return vm.contratosSelecionados.length
        }

      }



  }
})();
