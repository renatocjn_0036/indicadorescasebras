app.controller("MonitoramentoController", function($scope, $http, UltimaImportacao) {


  getImportacao = function() {
    UltimaImportacao.getDate('monitoramento').then(function(data) {
      $scope.ultima_importacao = data
    })
  }

  getImportacao();

  $scope.filtros = [];

  $scope.get_agrupamento = function(agrupamento) {
    console.log(agrupamento)
    $http.post('backend/indicadores/monitoramento/get_agrupamento_monitoramento.php', {
      agrupamento: agrupamento,
    }).then(function successCallBack(response) {
      $scope.filtros = response.data;
      console.log($scope.filtros)
    }, function errorCallBack(response) {
      console.log(response)
    })
  }

  $scope.get_pagamentos_devolvidos = function(indicadores, myForm) {
    $http.post('backend/indicadores/monitoramento/pagamentos_devolvidos.php', {
      indicadores: indicadores,
      filtros: $scope.number
    }).then(function successCallBack(response) {
      $scope.chart_pagamentos_devolvidos = {
        type: "ColumnChart",
        data: {
          cols: [{
              id: "t",
              label: "Topping",
              type: "string"
            },
            {
              id: "s",
              label: "Pagamentos Devolvidos",
              type: "number"
            },
            {
              id: "s",
              type: "number",
              role: "annotation"
            },
            {
              id: "s",
              type: "string",
              role: "tooltip",
              "p": {
                "html": true
              }
            }
          ],
          rows: []
        },
        options: {
          tooltip: {
            isHtml: true
          },
          colors: ['#81c868'],
          chartArea: {
            width: '90%',
            height: '70%'
          },
          vAxis: {
            format: '#\'%\'',
          },
          tooltip: {
            isHtml: true
          },
          legend: {
            position: 'none'
          },
          animation: {
            'duration': 1000,
            'easing': 'out',
            'startup': true
          },
          annotations: {
            textStyle: {
              bold: true,
              color: 'black'
            },
            alwaysOutside: true
          }
        },

        formatters: {
          number: [{
              columnNum: 1,
              suffix: '%'
            },
            {
              columnNum: 2,
              suffix: '%'
            },
          ]
        }
      };

      angular.forEach(response.data, function(value, key) {
        console.log(value)
        if (value.reapresentado != null) {
          var rows = {
            c: [{
                v: value.rows
              },
              {
                v: value.perc_reapresentado
              },
              {
                v: value.perc_reapresentado
              },
              {
                v: "<table class=\"tooltipChart\"><tr><td colspan=\"3\" style=\"margin-left: 5px\"><strong>" + value.rows + "</strong></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #81c868;margin-left: 5px\"></i> Reapresentado</td><td style=\"padding: 0px 20px;\"><strong>" + value.reapresentado + "</strong></td></td></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #f9cb9c;margin-left: 5px\"></i> Total</td><td style=\"padding: 0px 20px;\"><strong>" + value.total_by_uf + "</strong></td></tr></table>"
              }
            ]
          }

          $scope.chart_pagamentos_devolvidos.data.rows.push(rows);
        }
      })

    }, function errorCallBack(response) {
      console.log(response)
    })
  }


  $scope.cardconsig = function(indicadores, myForm) {
    $http.post('backend/indicadores/monitoramento/cardconsig.php', {
      indicadores: indicadores
    }).then(function successCallBack(response) {
      $scope.aprovacao_cardconsig = {
        type: "ColumnChart",
        data: {
          cols: [{
              id: "t",
              label: "Topping",
              type: "string"
            },
            {
              id: "s",
              label: "Aprovados",
              type: "number"
            },
            {
              id: "s",
              type: "number",
              role: "annotation"
            },
            {
              id: "s",
              type: "string",
              role: "tooltip",
              "p": {
                "html": true
              }
            }
          ],
          rows: []
        },
        options: {
          tooltip: {
            isHtml: true
          },
          colors: ['#81c868'],
          chartArea: {
            width: '90%',
            height: '70%'
          },
          vAxis: {
            format: '#\'%\'',
          },
          tooltip: {
            isHtml: true
          },
          legend: {
            position: 'none'
          },
          animation: {
            'duration': 1000,
            'easing': 'out',
            'startup': true
          },
          annotations: {
            textStyle: {
              bold: true,
              color: 'black'
            },
            alwaysOutside: true
          }
        },

        formatters: {
          number: [{
              columnNum: 1,
              suffix: '%'
            },
            {
              columnNum: 2,
              suffix: '%'
            },
          ]
        }
      };

      $scope.reprovacao_cardconsig = {
        type: "ColumnChart",
        data: {
          cols: [{
              id: "t",
              label: "Topping",
              type: "string"
            },
            {
              id: "s",
              label: "Reprovados",
              type: "number"
            },
            {
              id: "s",
              type: "number",
              role: "annotation"
            },
            {
              id: "s",
              type: "string",
              role: "tooltip",
              "p": {
                "html": true
              }
            }
          ],
          rows: []
        },
        options: {
          tooltip: {
            isHtml: true
          },
          colors: ['#f05050'],
          chartArea: {
            width: '90%',
            height: '70%'
          },
          vAxis: {
            format: '#\'%\'',
          },
          tooltip: {
            isHtml: true
          },
          legend: {
            position: 'none'
          },
          animation: {
            'duration': 1000,
            'easing': 'out',
            'startup': true
          },
          annotations: {
            textStyle: {
              bold: true,
              color: 'black'
            },
            alwaysOutside: true
          }
        },

        formatters: {
          number: [{
              columnNum: 1,
              suffix: '%'
            },
            {
              columnNum: 2,
              suffix: '%'
            },
          ]
        }
      };

      angular.forEach(response.data, function(value, key) {
        console.log(value)
        if (value.confirmado != null) {
          var rows = {
            c: [{
                v: value.uf
              },
              {
                v: value.perc_confirmado
              },
              {
                v: value.perc_confirmado
              },
              {
                v: "<table class=\"tooltipChart\"><tr><td colspan=\"3\" style=\"margin-left: 5px\"><strong>" + value.uf + "</strong></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #81c868;margin-left: 5px\"></i> Confirmado</td><td style=\"padding: 0px 20px;\"><strong>" + value.confirmado + "</strong></td></td></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #f9cb9c;margin-left: 5px\"></i> Total</td><td style=\"padding: 0px 20px;\"><strong>" + value.total_by_uf + "</strong></td></tr></table>"
              }
            ]
          }

          $scope.aprovacao_cardconsig.data.rows.push(rows);
        }

        if (value.cancelado != null) {

          var rows = {
            c: [{
                v: value.uf
              },
              {
                v: value.perc_cancelado
              },
              {
                v: value.perc_cancelado
              },
              {
                v: "<table class=\"tooltipChart\"><tr><td colspan=\"3\" style=\"margin-left: 5px\"><strong>" + value.uf + "</strong></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #81c868;margin-left: 5px\"></i> Cancelado</td><td style=\"padding: 0px 20px;\"><strong>" + value.cancelado + "</strong></td></td></tr><tr><td><i class=\"fa fa-circle\" style=\"color: #f9cb9c;margin-left: 5px\"></i> Total</td><td style=\"padding: 0px 20px;\"><strong>" + value.total_by_uf + "</strong></td></tr></table>"
              }
            ]
          }

          $scope.reprovacao_cardconsig.data.rows.push(rows);
        }
      })
    }, function errorCallBack(response) {
      console.log(response)
    })
  }
});
// 131
