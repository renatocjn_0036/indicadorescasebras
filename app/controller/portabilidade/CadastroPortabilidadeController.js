(function() {
  'use strict';

  angular.module("app").controller("CadastroPortabilidadeController", CadastroPortabilidadeController);

  function CadastroPortabilidadeController($location, $routeParams, $http,$scope,$route,userservice, portabilidadeservice) {

    var vm = this;
    vm.bancos = [];
    vm.unidades = [];
    vm.lista_emails = [];

    vm.cadastrar = cadastrar;
    vm.remove_email = remove_email;
    vm.add_email = add_email;

    getBancos();
    getUF();

    function getUF() {
      return userservice.getFilial().then(function(data){
        vm.unidades = data
      })
    }

    function getBancos() {
      return userservice.getBancos().then(function(data){
        vm.bancos = data
      })
    }

    function add_email(email) {
      vm.lista_emails.push({
        'email': email,
        'done': false
      });
      $scope.email = '';
    }

    function remove_email(index) {
      vm.lista_emails.splice(index, 1);
    }

    function cadastrar(dados, myform){
      if (myForm.$invalid) {
        vm.form = myForm
      } else {
        return portabilidadeservice.cadastro(dados, vm.lista_emails).then(function(data){
            if (data.retorno == false) {
              vm.error_msg = data.msg
              vm.error_form = true;
            } else {
              vm.success_msg = data.msg
              vm.success_form = true
              vm.error_form = false;
              delete $scope.dados
              vm.lista_emails = [];
            }
        })
      }
    }

  }
})()
