(function() {
  'use strict';

  angular.module("app").controller("EditarPortabilidadeController", EditarPortabilidadeController);

  function EditarPortabilidadeController($location, $routeParams, $http,$scope,$route,userservice, portabilidadeservice) {

    var vm = this;
    vm.bancos = [];
    vm.unidades = [];
    vm.lista_emails = [];

    vm.cadastrar = cadastrar;
    vm.remove_email = remove_email;
    vm.add_email = add_email;

    getPortabilidadeIB($routeParams)
    getEmail($routeParams.id)

    function getPortabilidadeIB(id){
      return portabilidadeservice.get($routeParams).then(function(data){
        console.log(data)
        $scope.dados = data[0]
      })
    }

    function getEmail(id){
      return portabilidadeservice.getEmail({id_portabilidade: id}).then(function(data){
        console.log(data)
      })
    }

    getBancos();
    getUF();

    function getUF() {
      return userservice.getFilial().then(function(data){
        vm.unidades = data

      })
    }

    function getBancos() {
      return userservice.getBancos().then(function(data){
        vm.bancos = data
      })
    }

    function add_email(email) {
      vm.lista_emails.push({
        'email': email,
        'done': false
      });
      $scope.email = '';
    }

    function remove_email(index) {
      vm.lista_emails.splice(index, 1);
    }

    function cadastrar(dados, myform){
      if (myForm.$invalid) {
        vm.form = myForm
      } else {
        return portabilidadeservice.cadastro(dados, vm.lista_emails).then(function(data){
            if (data.retorno == false) {
              vm.error_msg = data.msg
              vm.error_form = true;
            } else {
              vm.success_msg = data.msg
              vm.success_form = true
              vm.error_form = false;

            }
        })
      }
    }

  }
})()
