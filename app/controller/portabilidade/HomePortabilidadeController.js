(function() {
  'use strict';

  angular.module("app").controller("HomePortabilidadeController", HomePortabilidadeController);

  function HomePortabilidadeController($location, $routeParams, $http,$scope,$route, permissao) {

    var vm = this;
    vm.verificarPermissao = verificarPermissao

    function verificarPermissao(rota){
      console.log(permissao)
      if(permissao == 'admin'){
        return true
      }else{
        if(permissao.indexOf(rota) == -1){
          return false
        }else{
          return true
        }
      }
    }

  }
})()
