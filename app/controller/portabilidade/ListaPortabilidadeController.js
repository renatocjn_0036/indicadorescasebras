(function() {
  'use strict';

  angular.module("app").controller("ListaPortabilidadeController", ListaPortabilidadeController);

  function ListaPortabilidadeController($location, $routeParams, $http,$scope,$route, $interval, $uibModal,userservice,portabilidadeservice, permissao) {

    var vm = this;

    vm.portabilidades_cadastradas = [];
    vm.unidades = [];
    vm.bancos = [];
    vm.paginacao = []
    vm.paginacao = [];
    vm.filtros = {}

    vm.pesquisar = pesquisar;
    vm.open = open
    vm.open_valor_estornado = open_valor_estornado
    vm.open_valor_recuperado = open_valor_recuperado
    vm.open_cadastrar = open_cadastrar
    vm.verificarPermissao = verificarPermissao

    vm.num_registros = [
      {'valor': 10},
      {'valor': 50},
      {'valor': 100}
    ];

    vm.num_registro_atual = 10;

    getUF();
    getBancos();


    function pesquisar(page, num_registros) {
      if(!num_registros) num_registros = 10;
      if (!page) page = 1;
      console.log(num_registros)
      $http.post('backend/portabilidade/get_portabilidade.php', {
        'num_registros': num_registros,
        'pagina': page,
        'filtros':vm.filtros
      }).then(function successCallBack(response){
        console.log(response)

        vm.portabilidades_cadastradas = response.data.registros
        vm.paginacao = response.data.paginacao
      }, function errorCallBack(response){

      })
      // vm.success = false
      // vm.error_pesquisa = false
      // return portabilidadeservice.get(pesquisa).then(function(data) {
      //   console.log(data)
      //   if (data.length > 0) {
      //     vm.portabilidades_cadastradas = data
      //     vm.error = false
      //   } else {
      //     delete vm.portabilidades_cadastradas
      //     vm.error = true
      //     vm.error_msg = 'Nenhuma portabilidade encontrada'
      //   }
      // })
    }


    function verificarPermissao(rota){

      if(permissao == 'admin'){
        return true
      }else{
        if(permissao.indexOf(rota) == -1){
          return false
        }else{
          return true
        }
      }
    }

    function getUF() {
      return userservice.getFilial().then(function(data){
        vm.unidades = data;
      })
    }

    function getBancos() {
      return portabilidadeservice.getBancos().then(function(data){
        vm.bancos = data;
        console.log(vm.bancos)
      })
    }



    function open(item) {

      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'view/modal/myModalPortabilidade.html',
        controller: 'myModalController',
        controllerAs: 'vm',
        resolve: {
          item: function() {
            return item;
          }
        },
      });


      modalInstance.result.then(function(situacao) {
        portabilidadeservice.update(situacao).then(function(data){
            if (data.retorno == true) {
              for (var i = 0; i < vm.portabilidades_cadastradas.length; i++) {
                if (vm.portabilidades_cadastradas[i].id == situacao.id) {
                  vm.portabilidades_cadastradas[i].situacao = situacao.tipo

                }
              }
            }
        })

      }, function() {
        console.log('Modal dismissed at: ' + new Date());
      });

    }

    function open_valor_estornado(item) {

      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'view/modal/myModalPortabilidadeValor.html',
        controller: 'myModalValorController',
        controllerAs: 'vm',
        resolve: {
          item: function() {
            return item;
          }
        },
      });

      modalInstance.result.then(function(situacao) {
        portabilidadeservice.updateValorEstorno(situacao).then(function(data){
            if (data.retorno == true) {
              for (var i = 0; i < vm.portabilidades_cadastradas.length; i++) {
                if (vm.portabilidades_cadastradas[i].id == situacao.id) {
                  vm.portabilidades_cadastradas[i].valor_estorno = situacao.valor_estorno
                }
              }
            }
        })

      }, function() {
        console.log('Modal dismissed at: ' + new Date());
      });

    }

    function open_valor_recuperado(item) {

      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'view/modal/myModalPortabilidadeValorRecuperado.html',
        controller: 'myModalValorRecuperadoController',
        controllerAs: 'vm',
        resolve: {
          item: function() {
            return item;
          }
        },
      });

      modalInstance.result.then(function(valores, id) {
        console.log(valores)
        console.log(item)
        portabilidadeservice.inserirValorRecuperado(valores,item.id).then(function(data){
          if(data.retorno == true){
            for (var i = 0; i < vm.portabilidades_cadastradas.length; i++) {
              if (vm.portabilidades_cadastradas[i].id == item.id) {
                vm.portabilidades_cadastradas[i].total_recuperado = data.total
              }
            }
          }
        })

      }, function() {
        console.log('Modal dismissed at: ' + new Date());
      });

    }

    function open_cadastrar(item, detalhes) {

      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'view/modal/CadastroPortabilidade.html',
        controller: 'CadastroPortabilidadeController',
        controllerAs: 'vm',
        size: 'lg',
        resolve: {
          item: function() {
            return item;
          },
          detalhes: function() {
            return detalhes;
          }
        },
      });

      modalInstance.result.then(function(msg) {
        console.log(msg)
        pesquisar($scope.pesquisa)
        vm.success = true;
        vm.msg_success = msg

      }, function() {
        console.log('Modal dismissed at: ' + new Date());
      });

    }

  }

  angular.module("app").controller("myModalController", myModalController);

  function myModalController($scope, $uibModalInstance, item) {
    var vm = this;
    vm.ok = ok;
    vm.cancel = cancel;
    vm.numero_contrato = item.numero_contrato;

    $scope.situacao = {
      tipo: item.situacao,
      id: item.id
    }

    function ok(situacao) {
      $uibModalInstance.close(situacao);
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    };

  }

  angular.module("app").controller("myModalValorController", myModalValorController);

  function myModalValorController($scope, $uibModalInstance, item) {
    var vm = this;
    vm.ok = ok;
    vm.cancel = cancel;
    vm.numero_contrato = item.numero_contrato;

    $scope.dados = {
      id: item.id
    }

    function ok(situacao) {
      $uibModalInstance.close(situacao);
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    };

  }

  angular.module("app").controller("myModalValorRecuperadoController", myModalValorRecuperadoController);

  function myModalValorRecuperadoController($scope, $uibModalInstance, item, portabilidadeservice) {
    var vm = this;

    getValores()

    vm.ok = ok;
    vm.cancel = cancel;

    vm.add_valor = add_valor
    vm.remove_valor = remove_valor

    vm.valores_recuperados = [];

    function getValores(){
      return portabilidadeservice.getValoresRecuperados(item.id).then(function(data){
        if(data.length >0){
          vm.valores_recuperados = data
        }
      })
    }

    function remove_valor(index) {
      vm.valores_recuperados.splice(index, 1);
    }

    function add_valor(valor, myForm) {
      console.log(myForm)
      if(myForm.$invalid){
        vm.form = myForm
      }else{
        vm.valores_recuperados.push({
          'valor_recuperado': valor.valor_recuperado,
          'data': valor.data,
          'done': false
        });
        delete $scope.dados
      }
    }

    function ok() {
      console.log()
      $uibModalInstance.close(vm.valores_recuperados);
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    };

  }

  angular.module("app").controller("CadastroPortabilidadeController", CadastroPortabilidadeController);

  function CadastroPortabilidadeController($scope, $uibModalInstance, item, detalhes,portabilidadeservice,userservice) {

    var vm = this;


    vm.add_email = add_email
    vm.cadastrar = cadastrar
    vm.detalhes = detalhes
    vm.cancel = cancel
    vm.remove_email = remove_email

    vm.error_email = false;
    $scope.email = '';
    vm.titulo = detalhes ? 'Detalhes' : 'Cadastro';

    vm.bancos = []
    vm.lista_emails = []
    vm.unidades = []
    vm.valores_recuperados = [];

    getBancos();
    getUF();

    if(typeof item != 'undefined'){
      $scope.dados = item
      getEmail(item.id)
      getValores(item)
    }

    function getEmail(id){
      return portabilidadeservice.getEmail({id_portabilidade: id}).then(function(data){
        console.log(data)
        if(data.length > 0){
          vm.lista_emails = data
        }
      })
    }

    function getValores(item){
      return portabilidadeservice.getValoresRecuperados(item.id).then(function(data){
        if(data.length >0){
          vm.valores_recuperados = data
        }
      })
    }

    function getUF() {
      return userservice.getFilial().then(function(data){
        vm.unidades = data
      })
    }

    function getBancos() {
      return userservice.getBancos().then(function(data){
        vm.bancos = data
      })
    }

    function add_email(email) {
      console.log(email)
      if(email == ''){
        vm.error_email = true;
      }else{
        vm.error_email = false;
        vm.lista_emails.push({
          'email': email,
          'done': false
        });
        $scope.email = '';
      }
    }

    function remove_email(index) {
      vm.lista_emails.splice(index, 1);
    }


    // function cadastrar(dados, myform){
    //   console.log(myForm.$invalid)
    //   if (myForm.$invalid) {
    //     vm.form = myForm
    //   } else {
    //     return portabilidadeservice.cadastro(dados, vm.lista_emails).then(function(data){
    //         if (data.retorno == false) {
    //           vm.error_msg = data.msg
    //           vm.error_form = true;
    //         } else {
    //           $uibModalInstance.close(data.msg);
    //         }
    //     })
    //   }
    // }

    function cadastrar(dados, myForm) {
      if (myForm.$invalid) {
        vm.form = myForm
      } else {
        return portabilidadeservice.cadastro(dados, vm.lista_emails).then(function(data){
            if (data.retorno == false) {
              vm.error_msg = data.msg
              vm.error_form = true;
            } else {
                $uibModalInstance.close(data.msg);
              // vm.success_msg = data.msg
              // vm.success_form = true
              // vm.error_form = false;
              // delete $scope.dados
              // vm.lista_emails = [];
            }
        })
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

  }

})()
