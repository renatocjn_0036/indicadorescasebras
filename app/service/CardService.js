(function (){
  'use strict';


  angular
    .module('app')
    .factory('cardservice', cardservice);

cardservice.$inject = ['$http'];

function cardservice($http, logger) {
    return {
        get: get,
        post: post,
        excluir: excluir
    };

    function get(id){
      return $http.post('backend/cards/get_card.php',{
        id: id
      })
          .then(getAvengersComplete)
          .catch(getAvengersFailed);
      function getAvengersComplete(response) {
          return response.data;
      }

      function getAvengersFailed(error) {
          console.log(error)
      }
    }

    function post(dados) {
        return $http.post('backend/cards/post_card.php',{
          dados: dados
        })
            .then(getAvengersComplete)
            .catch(getAvengersFailed);
        function getAvengersComplete(response) {
            return response.data;
        }

        function getAvengersFailed(error) {
            console.log(error)
        }
    }

    function excluir(item){
      return $http.post('backend/cards/excluir.php',{
        dados: item
      })
          .then(getAvengersComplete)
          .catch(getAvengersFailed);
      function getAvengersComplete(response) {
          return response.data;
      }

      function getAvengersFailed(error) {
          console.log(error)
      }
    }

    function getEmail(item) {
      console.log(item)
        return $http.post('backend/contestacao/get_email.php',{
          id: item.id
        })
            .then(getAvengersComplete)
            .catch(getAvengersFailed);
        function getAvengersComplete(response) {
            return response.data;
        }

        function getAvengersFailed(error) {
            console.log(error)
        }
    }

    function sendEmail(emails, item){
      return $http.post('backend/contestacao/enviar_email.php',{
        emails: emails,
        dados: item
      })
          .then(getAvengersComplete)
          .catch(getAvengersFailed);
      function getAvengersComplete(response) {
          return response.data;
      }

      function getAvengersFailed(error) {
          console.log(error)
      }
    }
}

})()
