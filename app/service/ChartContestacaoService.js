app.factory('ChartContestacaoService', function() {

  return {
    getChart: getChart,
    getChartGauge: getChartGauge,
    getChartLaudo:getChartLaudo
  }


  function getChartLaudo(label_1,label_2){
    return{
      type: "ColumnChart",
      data: {
        cols: [{
            id: "t",
            label: "Topping",
            type: "string"
          },
          {
            id: "s",
            label: label_1,
            type: "number"
          },
          {
            id: "s",
            type: "number",
            role: "annotation"
          }, {
            id: "s",
            label: label_2,
            type: "number"
          },
          {
            id: "s",
            type: "number",
            role: "annotation"
          }
        ],
        rows: []
      },
      options: {

        colors: ['#81c868', '#f05050'],
        chartArea: {
          width: '90%',
          height: '80%'
        },
        isStacked: false,
        legend: {
          position: 'top',
          alignment: 'center'
        },
        animation: {
          duration: 1000,
          easing: 'out',
          startup: false
        },
        annotations: {
          textStyle: {
            bold: true,
            color: 'black'
          },
          alwaysOutside: true
        },
      },
      }
    }


  function getChart(label_1,label_2,label_3) {
    return {
      type: "ColumnChart",
      data: {
        cols: [{
            id: "t",
            label: "Topping",
            type: "string"
          },
          {
            id: "s",
            label: label_1,
            type: "number"
          },
          {
            id: "s",
            type: "number",
            role: "annotation"
          }, {
            id: "s",
            label: label_2,
            type: "number"
          },
          {
            id: "s",
            type: "number",
            role: "annotation"
          },
          {
            id: "s",
            label: label_3,
            type: "number"
          },
          {
            id: "s",
            type: "number",
            role: "annotation"
          },
        ],
        rows: []
      },
      options: {

        colors: ['#81c868', '#f90', '#f05050'],
        chartArea: {
          width: '90%',
          height: '80%'
        },
        isStacked: false,
        legend: {
          position: 'top',
          alignment: 'center'
        },
        animation: {
          duration: 1000,
          easing: 'out',
          startup: false
        },
        annotations: {
          textStyle: {
            bold: true,
            color: 'black'
          },
          alwaysOutside: true
        },
      },
      // formatters: {
      //   number: [{
      //       columnNum: 0,
      //       prefix: 'R$',
      //     },
      //     {
      //       columnNum: 2,
      //       prefix: 'R$',
      //     },
      //     {
      //       columnNum: 3,
      //       prefix: 'R$'
      //     },
      //   ]
      // }
    }
  }

  function getChartGauge(width) {
    return {
      type: "Gauge",
      options: {
        colors: ['#81c868', '#f05050', '#ffbd4a'],
        width: width,
        height: '55%',
        redFrom: 0,
        redTo: 25,
        yellowFrom: 25,
        yellowTo: 75,
        greenFrom: 75,
        greenTo: 100,
        minorTicks: 5,
        'animation': {
          'duration': 1000,
          'easing': 'in',
          'startup': true
        }
      },
      data: [
        ['Label', 'Value'],
      ]
    }
  }


})
