app.factory('ChartService', function() {

  return {
    getChart: getChart,
    getChartGauge: getChartGauge
  }

  function getChart(label, color) {
    return {
      type: "ColumnChart",
      data: {
        cols: [{
            id: "t",
            label: "Topping",
            type: "string"
          },
          {
            id: "s",
            label: label,
            type: "number"
          },
          {
            id: "s",
            type: "number",
            role: "annotation"
          },
          {
            id: "s",
            type: "string",
            role: "tooltip",
            "p": {
              "html": true
            }
          }
        ],
        rows: []
      },
      options: {
        tooltip: {
          isHtml: true
        },
        colors: [color],
        chartArea: {
          width: '90%',
          height: '80%'
        },
        isStacked: false,
        legend: {
          position: 'none'
        },
        vAxis: {
          format: '#\'%\'',
        },
        animation: {
          duration: 1000,
          easing: 'out',
          startup: false
        },
        annotations: {
          textStyle: {
            bold: true,
            color: 'black'
          },
          alwaysOutside: true
        },
        vAxis: {
          format: '#\'%\'',
        },
      },
      formatters: {
        number: [{
            columnNum: 1,
            suffix: '%'
          },
          {
            columnNum: 2,
            suffix: '%'
          },
        ]
      }
    }
  }

  function getChartGauge(width) {
    return {
      type: "Gauge",
      options: {
        colors: ['#81c868', '#f05050', '#ffbd4a'],
        width: width,
        height: '55%',
        redFrom: 0,
        redTo: 25,
        yellowFrom: 25,
        yellowTo: 75,
        greenFrom: 75,
        greenTo: 100,
        minorTicks: 5,
        'animation': {
          'duration': 1000,
          'easing': 'in',
          'startup': true
        }
      },
      data: [
        ['Label', 'Value'],
      ]
    }
  }


})