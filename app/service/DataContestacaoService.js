(function() {
  'use strict';


  angular
    .module('app')
    .factory('contestacaoservice', contestacaoservice);

  contestacaoservice.$inject = ['$http'];

  function contestacaoservice($http, logger) {
    return {
      postContestacao: postContestacao,
      getContestacao: getContestacao,
      updateContestacao: updateContestacao,
      updateContestacaoLaudo: updateContestacaoLaudo,
      getEmail: getEmail,
      sendEmail: sendEmail,
      deleteContestacao: deleteContestacao,
      getProdutos: getProdutos,
      getUnidades: getUnidades,
      gerarExcel: gerarExcel
    };

    function gerarExcel(filtros) {
      return $http.post('backend/contestacao/excel.php', {
        filtros: filtros
      })
      .then(function(response){
        return response.data
      })
    }

    function getUnidades(uf) {
      return $http.post('backend/contestacao/unidades.php', {
        uf: uf
      })
      .then(function(response){
        return response.data
      })
    }

    function deleteContestacao(dados) {
      return $http.post('backend/contestacao/excluir.php', {
          dados: dados
        })

        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }

    }

    function postContestacao(dados, emails) {
      return $http.post('backend/contestacao/novo.php', {
          dados: dados,
          lista_email: emails
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {

        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function getProdutos() {
      return $http.get('backend/contestacao/produtos.php')
              .then(function(response){
                return response.data
              })
    }

    function getContestacao(dados, pagina, num_registros) {
      return $http.post('backend/contestacao/get_contestacao.php', {
          dados: dados,
          pagina: pagina,
          num_registros: num_registros
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        console.log(response.data)
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function updateContestacao(item) {
      return $http.post('backend/contestacao/mudar_situacao.php', {
          situacao: item
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function updateContestacaoLaudo(item) {
      return $http.post('backend/contestacao/mudar_laudo.php', {
          laudo: item
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function getEmail(item) {
      console.log(item)
      return $http.post('backend/contestacao/get_email.php', {
          id: item.id
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function sendEmail(emails, item) {
      return $http.post('backend/contestacao/enviar_email.php', {
          emails: emails,
          dados: item
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    

  }

})()
