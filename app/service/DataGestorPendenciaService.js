(function() {
  'use strict';


  angular
    .module('app')
    .factory('gestorpendenciaservice', gestorpendenciaservice);

  gestorpendenciaservice.$inject = ['$http'];

  function gestorpendenciaservice($http, logger) {
    return {
      getPendencia: _getPendencia,
      updatePendencia: _updatePendencia
    };

    function _getPendencia(dados) {
      return $http.post('backend/gestor/get_pendencia.php', {
          dados: dados
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function _updatePendencia(dados, pendencia) {
      return $http.post('backend/gestor/update_pendencia.php', {
          dados: dados,
          pendencia: pendencia
        })

        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }

    }

    function postContestacao(dados, emails) {
      return $http.post('backend/contestacao/novo.php', {
          dados: dados,
          lista_email: emails
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function updateContestacao(item) {
      return $http.post('backend/contestacao/mudar_situacao.php', {
          situacao: item
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function getEmail(item) {
      console.log(item)
      return $http.post('backend/contestacao/get_email.php', {
          id: item.id
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function sendEmail(emails, item) {
      return $http.post('backend/contestacao/enviar_email.php', {
          emails: emails,
          dados: item
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }
  }

})()
