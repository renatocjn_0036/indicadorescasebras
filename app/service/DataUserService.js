(function (){
  'use strict';


  angular
    .module('app')
    .factory('userservice', userservice);

userservice.$inject = ['$http'];

function userservice($http, logger) {
    return {
        getFilial: getFilial,
        getBancos: getBancos,
        getUnidades: getUnidades
    };

    function getFilial() {
        return $http.get('backend/users/get_filial.php')
            .then(getAvengersComplete)
            .catch(getAvengersFailed);
        function getAvengersComplete(response) {
            return response.data;
        }

        function getAvengersFailed(error) {
            console.log(error)
        }
    }

    function getBancos(){
      return $http.post('backend/users/get_bancos.php')
          .then(getAvengersComplete)
          .catch(getAvengersFailed);
      function getAvengersComplete(response) {
          return response.data;
      }

      function getAvengersFailed(error) {
          console.log(error)
      }
    }

    function getUnidades(unidades){
      return $http.post('backend/users/get_unidades.php',{
        unidades: unidades
      })
          .then(getAvengersComplete)
          .catch(getAvengersFailed);
      function getAvengersComplete(response) {
          return response.data;
      }

      function getAvengersFailed(error) {
          console.log(error)
      }
    }

    function getEmail(item) {
      console.log(item)
        return $http.post('backend/contestacao/get_email.php',{
          id: item.id
        })
            .then(getAvengersComplete)
            .catch(getAvengersFailed);
        function getAvengersComplete(response) {
            return response.data;
        }

        function getAvengersFailed(error) {
            console.log(error)
        }
    }

    function sendEmail(emails, item){
      return $http.post('backend/contestacao/enviar_email.php',{
        emails: emails,
        dados: item
      })
          .then(getAvengersComplete)
          .catch(getAvengersFailed);
      function getAvengersComplete(response) {
          return response.data;
      }

      function getAvengersFailed(error) {
          console.log(error)
      }
    }
}

})()
