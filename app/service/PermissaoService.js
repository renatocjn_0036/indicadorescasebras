(function (){
  'use strict';


  angular
    .module('app')
    .factory('permissaoservice', permissaoservice);

permissaoservice.$inject = ['$http'];

function permissaoservice($http, logger) {
    return {
        getPermissoes: _getPermissoes,
        getRotas: _getRotas,
    };

    function _getPermissoes(rota){
      return $http.get('backend/auth/auth.php')

      .then(getAvengersComplete)
      .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        if(response.data.dados.id_perfil != 1){
          return response.data.permissoes
        }else{
          return 'admin'
        }

      }

      function getAvengersFailed(error) {
          console.log(error)
      }
    }

    function _getRotas(){
      return $http.get('backend/users/get_permissoes.php')

      .then(getAvengersComplete)
      .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data

      }

      function getAvengersFailed(error) {
          console.log(error)
      }
    }
}

})()
