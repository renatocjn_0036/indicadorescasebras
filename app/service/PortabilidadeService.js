(function() {
  'use strict';


  angular
    .module('app')
    .factory('portabilidadeservice', portabilidadeservice);

  portabilidadeservice.$inject = ['$http'];

  function portabilidadeservice($http, logger) {
    return {
      cadastro: cadastro,
      get: get,
      update: update,
      updateValorEstorno: updateValorEstorno,
      inserirValorRecuperado: inserirValorRecuperado,
      getValoresRecuperados: getValoresRecuperados,
      getEmail: getEmail,
      getBancos: getBancos

    };

    function getBancos(){
      return $http.post('backend/portabilidade/bancos.php')
          .then(getAvengersComplete)
          .catch(getAvengersFailed);
      function getAvengersComplete(response) {
          return response.data;
      }

      function getAvengersFailed(error) {
          console.log(error)
      }
    }

    function cadastro(dados, emails) {
      return $http.post('backend/portabilidade/cadastro.php', {
          dados: dados,
          lista_email: emails

        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function get(dados) {
      return $http.post('backend/portabilidade/get_portabilidade.php', {
        dados: dados
      })
      .then(getAvengersComplete)
      .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function update(item) {
      return $http.post('backend/portabilidade/mudar_situacao.php', {
          situacao: item
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function updateValorEstorno(item) {
      return $http.post('backend/portabilidade/mudar_valor_estorno.php', {
          item: item
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function inserirValorRecuperado(valores, id) {
      return $http.post('backend/portabilidade/inserir_valor_recuperado.php', {
          valores: valores,
          id:id
        })

        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }

    }



    function getValoresRecuperados(item) {
      console.log(item)
      return $http.post('backend/portabilidade/get_valores_recuperados.php', {
          id: item
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function getEmail(id){
      return $http.post('backend/portabilidade/get_email.php',{
        id:id
      })
      .then(getAvengersComplete)
      .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }

    function sendEmail(emails, item) {
      return $http.post('backend/contestacao/enviar_email.php', {
          emails: emails,
          dados: item
        })
        .then(getAvengersComplete)
        .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        console.log(error)
      }
    }
  }

})()
