app.factory('UltimaImportacao', function($http){
  return {
    getDate : getDate
  };

  function getDate(id){
    return $http.post('backend/lib/ultima_importacao.php', {
      id: id
    }).then(function successCallBack(response){

      return response.data.date
    }, function errorCallBack(response){
      console.log(response)
    })
  }


})
