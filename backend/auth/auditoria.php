<?php
require_once '../lib/crud.php';

$data = json_decode(file_get_contents("php://input"));

if(isset($data->page)){
  $page = $data->page;
  $ob = new stdClass();
  $ob->page = $page;
  $ob->id_user = $_SESSION['usuario_logado']->id;
  $ob->date = date('Y-m-d h:i:s');
  Crud::getInstance('auditoria')->insert($ob);
}

if(isset($data->id)){
  $sql = 'SELECT *, date_format(date, "%d/%m/%Y %H:%i:%s") as date from auditoria where id_user = ? LIMIT 10';
  $auditoria = Crud::getInstance()->getSQLGeneric($sql, array($data->id), TRUE);
  echo json_encode($auditoria);
}

?>
