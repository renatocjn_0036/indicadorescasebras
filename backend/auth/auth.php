<?php
session_start();

if(isset($_SESSION["usuario"]) && (!empty($_SESSION["usuario"]))){
  echo json_encode($_SESSION["usuario"]);
} else{
  session_unset();
  session_destroy();
  echo json_encode(array("error" => "login"));
}
?>
