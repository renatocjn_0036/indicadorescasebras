<?php
require_once '../lib/crud.php';
$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));

if(!empty($data->dados)){

  if(!empty($data->dados->id)){
    $id = $data->dados->id;
    unset($data->dados->id);
    $retorno = Crud::getInstance('cards_secundario')->update($data->dados, array('id' => $id))['retorno'];
    $json = [
      'retorno'=>$retorno,
      'msg'=>'Card Atualizado Com Sucesso'
    ];
  }else{
    $retorno = Crud::getInstance('cards_secundario')->insert($data->dados)['retorno'];
    $json = [
      'retorno'=>$retorno,
      'msg'=>'Card Cadastrado Com Sucesso'
    ];
  }
  echo json_encode($json);
}
// if(!empty($data->id)){
//   $qry = 'select * from cards_secundario where id_card = ?';
//   $json = Crud::getInstance()->getSQLGeneric($qry, array($data->id), TRUE);
//   echo json_encode($json);
// }
?>
