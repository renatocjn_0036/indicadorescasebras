<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title></title>

  <style type="text/css">
    @media yahoo {

      .kmHide,
      .kmMobileOnly,
      .kmMobileHeaderStackDesktopNone,
      .kmMobileWrapHeaderDesktopNone {
        display: none;
        font-size: 0 !important;
        line-height: 0 !important;
        mso-hide: all !important;
        max-height: 0 !important;
        max-width: 0 !important;
        width: 0 !important
      }
    }

    @media only screen and (max-width:480px) {

      body,
      table,
      td,
      p,
      a,
      li,
      blockquote {
        -webkit-text-size-adjust: none !important
      }

      body {
        width: 100% !important;
        min-width: 100% !important
      }

      #bodyCell {
        padding: 10px !important
      }

      table.kmMobileHide {
        display: none !important
      }

      table.kmDesktopOnly,
      td.kmDesktopOnly,
      th.kmDesktopOnly,
      tr.kmDesktopOnly,
      td.kmDesktopWrapHeaderMobileNone {
        display: none !important
      }

      table.kmMobileOnly {
        display: table !important
      }

      tr.kmMobileOnly {
        display: table-row !important
      }

      td.kmMobileOnly,
      td.kmDesktopWrapHeader,
      th.kmMobileOnly {
        display: table-cell !important
      }

      tr.kmMobileNoAlign,
      table.kmMobileNoAlign {
        float: none !important;
        text-align: initial !important;
        vertical-align: middle !important;
        table-layout: fixed !important
      }

      tr.kmMobileCenterAlign {
        float: none !important;
        text-align: center !important;
        vertical-align: middle !important;
        table-layout: fixed !important
      }

      td.kmButtonCollection {
        padding-left: 9px !important;
        padding-right: 9px !important;
        padding-top: 9px !important;
        padding-bottom: 9px !important
      }

      td.kmMobileHeaderStackDesktopNone,
      img.kmMobileHeaderStackDesktopNone,
      td.kmMobileHeaderStack {
        display: block !important;
        margin-left: auto !important;
        margin-right: auto !important;
        padding-bottom: 9px !important;
        padding-right: 0 !important;
        padding-left: 0 !important
      }

      td.kmMobileWrapHeader,
      td.kmMobileWrapHeaderDesktopNone {
        display: inline-block !important
      }

      td.kmMobileHeaderSpacing {
        padding-right: 10px !important
      }

      td.kmMobileHeaderNoSpacing {
        padding-right: 0 !important
      }

      table.kmDesktopAutoWidth {
        width: inherit !important
      }

      table.kmMobileAutoWidth {
        width: 100% !important
      }

      table.kmTextContentContainer {
        width: 100% !important
      }

      table.kmBoxedTextContentContainer {
        width: 100% !important
      }

      td.kmImageContent {
        padding-left: 0 !important;
        padding-right: 0 !important
      }

      img.kmImage {
        width: 100% !important
      }

      td.kmMobileStretch {
        padding-left: 0 !important;
        padding-right: 0 !important
      }

      table.kmSplitContentLeftContentContainer,
      table.kmSplitContentRightContentContainer,
      table.kmColumnContainer,
      td.kmVerticalButtonBarContentOuter table.kmButtonBarContent,
      td.kmVerticalButtonCollectionContentOuter table.kmButtonCollectionContent,
      table.kmVerticalButton,
      table.kmVerticalButtonContent {
        width: 100% !important
      }

      td.kmButtonCollectionInner {
        padding-left: 9px !important;
        padding-right: 9px !important;
        padding-top: 9px !important;
        padding-bottom: 9px !important
      }

      td.kmVerticalButtonIconContent,
      td.kmVerticalButtonTextContent,
      td.kmVerticalButtonContentOuter {
        padding-left: 0 !important;
        padding-right: 0 !important;
        padding-bottom: 9px !important
      }

      table.kmSplitContentLeftContentContainer td.kmTextContent,
      table.kmSplitContentRightContentContainer td.kmTextContent,
      table.kmColumnContainer td.kmTextContent,
      table.kmSplitContentLeftContentContainer td.kmImageContent,
      table.kmSplitContentRightContentContainer td.kmImageContent {
        padding-top: 9px !important
      }

      td.rowContainer.kmFloatLeft,
      td.rowContainer.kmFloatLeft,
      td.rowContainer.kmFloatLeft.firstColumn,
      td.rowContainer.kmFloatLeft.firstColumn,
      td.rowContainer.kmFloatLeft.lastColumn,
      td.rowContainer.kmFloatLeft.lastColumn {
        float: left;
        clear: both;
        width: 100% !important
      }

      table.templateContainer,
      table.templateContainer.brandingContainer,
      div.templateContainer,
      div.templateContainer.brandingContainer,
      table.templateRow {
        max-width: 600px !important;
        width: 100% !important
      }

      h1 {
        font-size: 40px !important;
        line-height: 1.1 !important
      }

      h2 {
        font-size: 32px !important;
        line-height: 1.1 !important
      }

      h3 {
        font-size: 24px !important;
        line-height: 1.1 !important
      }

      h4 {
        font-size: 18px !important;
        line-height: 1.1 !important
      }

      td.kmTextContent {
        font-size: 14px !important;
        line-height: 1.3 !important
      }

      td.kmTextBlockInner td.kmTextContent {
        padding-right: 18px !important;
        padding-left: 18px !important
      }

      table.kmTableBlock.kmTableMobile td.kmTableBlockInner {
        padding-left: 9px !important;
        padding-right: 9px !important
      }

      table.kmTableBlock.kmTableMobile td.kmTableBlockInner .kmTextContent {
        font-size: 14px !important;
        line-height: 1.3 !important;
        padding-left: 4px !important;
        padding-right: 4px !important
      }
    }
  </style>
  <!--[if mso]>
    <style>

      .templateContainer {
        border: 0px none #aaaaaa;
        background-color: #ffffff;

          border-radius: 0px;

      }
      #brandingContainer {
        background-color: transparent !important;
        border: 0;
      }


      .templateContainerInner {
        padding: 0px;
      }

    </style>
  <![endif]-->
</head>

<body style="margin:0;padding:0;background-color:#F7F7F7">
  <center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" id="bodyTable" width="100%" data-upload-file-url="/ajax/email-editor/file/upload" data-upload-files-url="/ajax/email-editor/files/upload" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:auto;padding:0;background-color:#F7F7F7;height:100%;margin:0;width:100%">
      <tbody>
        <tr>
          <td align="center" id="bodyCell" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:auto;padding-top:50px;padding-left:20px;padding-bottom:20px;padding-right:20px;border-top:0;height:100%;margin:0;width:100%">
            <!--[if !mso]><!-->
            <div class="templateContainer" style="border:0 none #aaa;background-color:#fff;border-radius:0;display: table; width:600px">
              <div class="templateContainerInner" style="padding:0">
                <!--<![endif]-->
                <!--[if mso]>
              <table border="0" cellpadding="0" cellspacing="0" class="templateContainer"  width="600" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
              <tbody>
                <tr>
                  <td class="templateContainerInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
            <![endif]-->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                  <tr>
                    <td align="center" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                      <table border="0" cellpadding="0" cellspacing="0" class="templateRow" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                        <tbody>
                          <tr>
                            <td class="rowContainer kmFloatLeft" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                              <table border="0" cellpadding="0" cellspacing="0" class="kmImageBlock" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;min-width:100%">
                                <tbody class="kmImageBlockOuter">
                                  <tr>
                                    <td class="kmImageBlockInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding:0px;padding-right:9;padding-left:9;" valign="top">
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" class="kmImageContentContainer" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;min-width:100%">
                                        <tbody>
                                          <tr>
                                            <td class="kmImageContent" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding:0;font-size:0;padding:0;">
                                              <img align="left" alt="" class="kmImage" src="%%img_cabecalho%%" width=" 582 " style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;padding-bottom:0;display:inline;vertical-align:top;font-size:12px;width:100%;margin-right:0;max-width:1000px;padding:0;border-width:0;" />
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" class="kmTextBlock" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;background-color: #ffffff;">
                                <tbody class="kmTextBlockOuter">
                                  <tr>
                                    <td class="kmTextContent" valign="top" style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;color:#222;font-family:"Helvetica Neue", Arial;font-size:14px;line-height:1.3;letter-spacing:0;text-align:left;max-width:100%;word-wrap:break-word;padding-top:9px;padding-bottom:9px;padding-left:18px;padding-right:18px;'>
                                      <p style="margin:0;text-align: center">====================================================================</p>
                                      <p style="margin:0;padding-bottom:0;text-align: center;"><span style="font-size:15px;"><strong>ATENÇÃO! ESTE É UM E-MAIL DE ENVIO AUTOMÁTICO, FAVOR NÃO RESPONDER</strong></p>
                                      <p style="margin:0;padding-bottom:3em;text-align: center">====================================================================</p>
                                      <p style="margin:0;padding-bottom:2em;font-size: 17px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recebemos o Laudo da Reclamação do cliente em referência, onde se teve a seguinte conclusão:</p>
                                      <ul style="list-style: none;padding-bottom: 2em">
                                        <li><b>CLIENTE: %%cliente%% </b></li>
                                        <li><b>CPF: %%cpf%%</b></li>
                                        <li><b>BANCO: %%banco%%</b></li>
                                        <li><b>CONTRATO: %%contrato%%</b></li>
                                        <li><b>CONCLUSÃO DA RECLAMAÇÃO: <span style="color: %%cor%%">%%situacao%%</span></b></li>
                                      </ul>
                                      <p style="margin:0;padding-bottom:3em;font-size: 17px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salientamos no entanto que, se o reclamante apresentar novos documentos que comprovem a não realização do empréstimo, a  conclusão da análise será reavaliada.</p>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top" style=\"border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed\">
                      <table border="0" cellpadding="0" cellspacing="0" class="templateRow" width="100%" style=\"border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed\">
                        <tbody>
                          <tr>
                            <td class="rowContainer kmFloatLeft" valign="top" style=\"border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed\">
                              <table border="0" cellpadding="0" cellspacing="0" class="kmImageBlock" width="100%" style=\"border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;min-width:100%\">
                                <tbody class="kmImageBlockOuter">
                                  <tr>
                                    <td class="kmImageBlockInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding:0px;padding-right:9;padding-left:9;" valign="top">
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" class="kmImageContentContainer" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;min-width:100%">
                                        <tbody>
                                          <tr>
                                            <td class="kmImageContent" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding:0;font-size:0;padding:0;">
                                              <img align="left" alt="" class="kmImage" src="%%img_rodape%%" width=" 582 " style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;padding-bottom:0;display:inline;vertical-align:top;font-size:12px;width:100%;margin-right:0;max-width:600px;padding:0;border-width:0;" />
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </table>
                <!--[if !mso]><!-->
              </div>
            </div>
            <!--<![endif]-->
            <!--[if mso]>
                    </td>
                  </tr>
                </tbody>
              </table>
            <![endif]-->
            <!--[if !mso]><!-->
            <div class="templateContainer brandingContainer" style="border:0;background-color:transparent;border-radius:0;display: table; width:600px">
              <div class="templateContainerInner" style="padding:0">
                <!--<![endif]-->
                <!--[if mso]>
              <table border="0" cellpadding="0" cellspacing="0" class="templateContainer" id="brandingContainer" width="600" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
              <tbody>
                <tr>
                  <td class="templateContainerInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
            <![endif]-->

                <!--[if !mso]><!-->
              </div>
            </div>
            <!--<![endif]-->
            <!--[if mso]>
                    </td>
                  </tr>
                </tbody>
              </table>
            <![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
  </center>
</body>

</html>
