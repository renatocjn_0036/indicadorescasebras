<?php
require_once '../lib/email.class.php';

$data = json_decode(file_get_contents("php://input"));
$dados = $data->dados;
$emails = $data->emails;

$template = "email.tpl";

$array_template = array(
  "cpf" => $dados->cpf,
  "banco" => strtoupper($dados->banco),
  "numero_contrato" => $dados->numero_contrato,
  "cliente" => strtoupper($dados->cliente),
  "unidade" => strtoupper($dados->unidade),
  "nome_agente" => strtoupper($dados->nome_agente),
  "data_atual" => date('d/m/Y'),
  "observacao" => nl2br($dados->observacao),
  "titulo" => null,
  "sub-titulo" => null,
  "texto" => null,
  "img_cabecalho" => null,
  "img_rodape" =>null
);

if($dados->tipo == 'Contestacao'){

  $assunto = 'COMUNICADO DE CONTESTAÇÃO Nº '.$dados->numero_contrato;
  $array_template['titulo'] = $assunto;
  $array_template['sub-titulo'] = 'Orientações';
  $array_template['texto'] = 'Solicitamos que envie o retorno com os subsídios para o email';
  $array_template['img_cabecalho'] = 'http://www.ltpromotora.com.br/imagem/contestacao_cabecalho.jpg';
  $array_template['img_rodape'] = 'http://www.ltpromotora.com.br/imagem/contestacao-rodape.jpg';

}else if($dados->tipo == 'Documental'){

  $assunto = 'NOTIFICAÇÃO DE PENDÊNCIA Nº '.$dados->numero_contrato;
  $array_template['titulo'] = $assunto;
  $array_template['sub-titulo'] = 'PENDÊNCIA';
  $array_template['texto'] = 'Para que o banco em questão possa realizar a defesa da ação, precisamos que as seguintes pendências sejam enviadas imediatamente para o email';
  $array_template['img_cabecalho'] = 'http://www.ltpromotora.com.br/imagem/documental-cabecalho.jpg';
  $array_template['img_rodape'] =  'http://www.ltpromotora.com.br/imagem/documental-rodape.png';

}

$email = new Email($template);
$email->preencherTemplate($array_template);
$email->setDestinatarios($emails);
$email->setAssunto($assunto);
$email->enviar();

echo "ok";

?>
