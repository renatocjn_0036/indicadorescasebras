<?php 

require_once '../lib/crud.php';
$data = json_decode(file_get_contents("php://input"));

$arrayParams = [];
$where = [];

if (!empty($_COOKIE['filtros'])) {
  unset($_COOKIE['PHPSESSID']);
  $dados = json_decode($_COOKIE['filtros']);
  if (isset($dados->data_inicial) && isset($dados->data_final)) {
    $data_inicial = substr($dados->data_inicial, 0, 10) . " 00:00:00";
    $data_final = substr($dados->data_final, 0, 10) . " 23:59:00";
    $where[] = "date between '{$data_inicial}' and '{$data_final}'";
  }

  unset($dados->data_inicial);
  unset($dados->data_final);
  $arrayParams = array();
  foreach($dados as $key => $value) {
    if($value != '' || $value != null){
      $where[] = "{$key} = ?";
      $arrayParams[] = $value;
    }
  }
}

$sql = 'SELECT contestacao.*,DATEDIFF(now(), contestacao.date) as dias, produto.nome as produto, unidades.uf,
        CASE 
            WHEN contestacao.situacao = 0 THEN "Aguardando Retorno"
            WHEN contestacao.situacao = 1 THEN "Enviado Defesa"
            WHEN contestacao.situacao = 2 THEN "Não Enviado Defesa"
            ELSE " "
        END as nova_situacao    
        FROM contestacao contestacao 
        left join produto produto on contestacao.produto_id = produto.id 
        left join unidades unidades on contestacao.unidade = unidades.nome';

if (!empty($where)) {
  $sql.= ' WHERE ' . implode(' AND ', $where);
}

$results = Crud::getInstance()->getSQLGeneric($sql, $arrayParams, TRUE);
  $arquivo = 'contestacao.xls';

  $tabela  = "";
  $tabela .= "  <table>";
  $tabela .= "      <tr>";
  $tabela .= "          <th>Cliente</th>";
  $tabela .= "          <th>CPF</th>";
  $tabela .= "          <th>Contrato</th>";
  $tabela .= "          <th>Banco</th>";
  $tabela .= "          <th>Unidade</th>";
  $tabela .= "          <th>UF</th>";
  $tabela .= "          <th>Data Abertura</th>";
  $tabela .= "          <th>Valor</th>";
  $tabela .= "          <th>Agente</th>";
  $tabela .= "          <th>Tipo</th>";
  $tabela .= "          <th>Laudo</th>";
  $tabela .= "          <th>Situacao</th>";
  $tabela .= "          <th>Dias em Atraso</th>";
  $tabela .= "      </tr>";

for($i = 0; $i < count($results); $i++)
{
 //print_r($results[$i]->cliente);exit;

  $tabela .= "      <tr>";
  $tabela .= "          <td>".$results[$i]->cliente."</td>";
  $tabela .= "          <td>".$results[$i]->cpf."</td>";
  $tabela .= "          <td>".$results[$i]->numero_contrato."</td>";
  $tabela .= "          <td>".$results[$i]->banco."</td>";
  $tabela .= "          <td>".$results[$i]->unidade."</td>";
  $tabela .= "          <td>".$results[$i]->uf."</td>";
  $tabela .= "          <td>".$results[$i]->date."</td>";
  $tabela .= "          <td>".$results[$i]->valor_contrato."</td>";
  $tabela .= "          <td>".$results[$i]->nome_agente."</td>";
  $tabela .= "          <td>".$results[$i]->tipo."</td>";
  $tabela .= "          <td>".$results[$i]->laudo."</td>";
  $tabela .= "          <td>".$results[$i]->nova_situacao."</td>";
  $tabela .= "          <td>".$results[$i]->dias."</td>";
  $tabela .= "      </tr>";
}
$tabela .= '</table>';
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
header ("Content-Description: PHP Generated Data" );

echo $tabela;
?>