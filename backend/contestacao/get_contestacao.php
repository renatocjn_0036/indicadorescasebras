<?php
require_once '../lib/crud.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));
$arrayParams = [];
$where = [];

$registrosPorPagina = $data->num_registros;
$paginaAtual = $data->pagina;

if (!empty($data->dados)) {
  $dados = $data->dados;
  // print_r($dados);exit;
  if (isset($dados->data_inicial) && isset($dados->data_final)) {
    $data_inicial = substr($dados->data_inicial, 0, 10) . " 00:00:00";
    $data_final = substr($dados->data_final, 0, 10) . " 23:59:00";
    $where[] = "date between '{$data_inicial}' and '{$data_final}'";
  }

  unset($dados->data_inicial);
  unset($dados->data_final);
  $arrayParams = array();
  foreach($dados as $key => $value) {
    if($value != '' || $value != null){
      $where[] = "{$key} = ?";
      $arrayParams[] = $value;
    }
  }
}

$sql = 'SELECT contestacao.*,DATEDIFF(now(), contestacao.date) as dias, produto.nome as produto, unidades.uf 
        FROM contestacao contestacao 
        left join produto produto on contestacao.produto_id = produto.id 
        left join unidades unidades on contestacao.unidade = unidades.nome';

if (!empty($where)) {
  $sql.= ' WHERE ' . implode(' AND ', $where);
}

$offset = (intval($paginaAtual)-1) * $registrosPorPagina;

$sql .=  " GROUP BY numero_contrato ORDER BY dias DESC";

$count = count($dao->getSQLGeneric($sql, $arrayParams, TRUE));

// Adicionando a paginação
$sql .= " LIMIT {$registrosPorPagina} OFFSET {$offset}";

$registros = $dao->getSQLGeneric($sql, $arrayParams, TRUE);


echo json_encode([
  'registros'=> $registros,
  'paginacao' => [
    'registros_por_pagina' => $registrosPorPagina,
     'pagina_atual'         => $paginaAtual,
     'total_registros'      => $count,
  ]
]);

// $datasources = $dao->getSQLGeneric($sql, $arrayParams);
// echo json_encode($datasources);
?>
