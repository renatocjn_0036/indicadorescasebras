<?php
error_reporting(0);
require_once '../lib/crud.php';

$data = json_decode(file_get_contents("php://input"));

$novo_array_uf = array();
$novo_array_banco = array();
$where = '';
$group = [];

$where = "where date between '".substr($data->indicador->data_inicial,0,10)." 00:00:00' and '".substr($data->indicador->data_final,0,10)." 23:59:59'";
$sql_acumulado_banco = "SELECT banco, count(id) as id, valor_contrato as total from contestacao " . $where . "group by banco order by banco";
$retorno_banco_acumulado = Crud::getInstance()->getSQLGeneric($sql_acumulado_banco);

$where = "where date between '".substr($data->indicador->data_inicial,0,10)." 00:00:00' and '".substr($data->indicador->data_final,0,10)." 23:59:59'";
$sql_acumulado_laudo = "SELECT laudo, count(id) as id, valor_contrato as total from contestacao " . $where . "group by laudo order by laudo";
$retorno_laudo_acumulado = Crud::getInstance()->getSQLGeneric($sql_acumulado_laudo);

unset($data->indicador->data_inicial);
unset($data->indicador->data_final);

foreach ($data->indicador as $key => $value) {

  if(!in_array('TODOS', $value) && !empty($value)){
    $where .= " and $key in (" . join(",", array_to_string($value)) . ")";
  }

  if(!empty($value)){
    $group[] = $key;
  }
}

$sql = "SELECT " . join(",", $group) . ", situacao, tipo, count(id) as id, valor_contrato as total, laudo from contestacao " . $where . " group by situacao, tipo, " . join(", ", $group) . " order by " . join(", ", $group) . "   ";
$retorno = Crud::getInstance()->getSQLGeneric($sql);

foreach($retorno as $key => $value) {
  $array_total['total'] += $value->id;
  $array_total[$value->tipo] += $value->id;
  $array_total[$value->situacao] += $value->id;
  $novo_array_uf[$value->tipo][$value->uf][$value->situacao] += $value->total;
  $novo_array_uf_ambos[$value->uf][$value->situacao] += $value->total;
  $novo_array_banco[$value->banco][$value->situacao] += $value->total;
  $array_laudo[$value->uf][$value->laudo] += $value->total;
  $array_situacao_x_laudo[$value->situacao][$value->laudo] += $value->total;
}

$json = [
  'array_uf' => prepare_array($novo_array_uf),
  'array_uf_ambos' => prepare_array_multidimessional($novo_array_uf_ambos),
  'array_banco' =>prepare_array_multidimessional($novo_array_banco),
  'array_laudo' => prepare_array_laudo($array_laudo),
  'array_situacao_x_laudo' =>prepare_array_laudo($array_situacao_x_laudo),
  'array_banco_acumulado'=> $retorno_banco_acumulado,
  'array_laudo_acumulado' => $retorno_laudo_acumulado,
  'array_total' => $array_total
];

echo json_encode($json);

function reduce_valor($array, $item){
  return $total = array_reduce($array, function($valor, $produto) use ($item){
    if(in_array($item, (array)$produto)) {
      $valor += $produto->total;
    }
    return $valor;
  });
}

function array_to_string($array){
  return $novo_array = array_map(function($map){
    $map = "'$map'";
    return $map;
  }, $array);
}

function prepare_array($array){
  foreach ($array as $key => $value) {
    $array_keys = array_keys($value);
    for ($i=0; $i < count($array_keys); $i++){
      $ob = new stdClass();
      $ob->tipo = $key;
      $ob->row = $array_keys[$i];
      $ob->aguardando_retorno = isset($value[$array_keys[$i]][0]) ? $value[$array_keys[$i]][0] : null;
      $ob->concluido = isset($value[$array_keys[$i]][1]) ? $value[$array_keys[$i]][1] : null;
      $ob->concluido_sem_retorno = isset($value[$array_keys[$i]][2]) ? $value[$array_keys[$i]][2] : null;
      $novo_array[] = $ob;
    }
  }
  return $novo_array;
}


function prepare_array_multidimessional($array){
  foreach ($array as $key => $value) {
      $ob = new stdClass();
      $ob->row = $key;
      $ob->aguardando_retorno = isset($value[0]) ? $value[0] : null;
      $ob->concluido = isset($value[1]) ? $value[1] : null;
      $ob->concluido_sem_retorno = isset($value[2]) ? $value[2] : null;
      $novo_array[] = $ob;
  }
  return $novo_array;
}

function prepare_array_laudo($array){
  foreach ($array as $key => $value) {
      $ob = new stdClass();
      $ob->row = traduzir_situacao($key);
      $ob->Improcedente = isset($value['Improcedente']) ? $value['Improcedente'] : null;
      $ob->Procedente = isset($value['Procedente']) ? $value['Procedente'] : null;
      $ob->Aguardando = isset($value['Aguard Laudo Banco']) ? $value['Aguard Laudo Banco'] : null;
      $novo_array[] = $ob;
  }

  return $novo_array;
}

function traduzir_situacao($key){

  switch ($key) {
    case '0':
      return 'Aguardando Retorno';
      break;
    case '1':
    return 'Concluído';
      break;
    case '2':
    return 'Concluído sem Retorno';
      break;
    default:
      return $key;
      break;
  }
};
?>
