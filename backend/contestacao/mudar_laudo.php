<?php
require_once '../lib/crud.php';
require_once '../lib/email.class.php';

$data = json_decode(file_get_contents("php://input"));
$situacao = $data->laudo;

$ob = new stdClass();
$ob->laudo = $situacao->laudo;
$ob->data_laudo = date('Y-m-d');
$retorno = Crud::getInstance('contestacao')->update($ob, array('id' => $situacao->id));
$emails = Crud::getInstance()->getSQLGeneric('SELECT * from email_contestacao where id_contestacao = ?', array($situacao->id), TRUE);
$dados = Crud::getInstance()->getSQLGeneric('SELECT * from contestacao where id = ?', array($situacao->id), false);
if ($dados->tipo == 'Contestacao') {
  $assunto = 'COMUNICADO DE CONTESTAÇÃO Nº ' . $dados->numero_contrato;
}
else
if ($dados->tipo == 'Documental') {
  $assunto = 'NOTIFICAÇÃO DE PENDÊNCIA Nº ' . $dados->numero_contrato;
}

if($situacao->laudo == 'Procedente' || $situacao->laudo == 'Improcedente'){
  $cor = $situacao->laudo == 'Improcedente' ? 'green' : 'red';
  $template = "email_laudo.tpl";

  $array_template = array(
    "cliente" => $dados->cliente,
    "cpf" => $dados->cpf,
    "banco" => $dados->banco,
    "contrato" => $dados->numero_contrato,
    "situacao" => $situacao->laudo,
    "img_rodape" => 'http://www.ltpromotora.com.br/imagem/rodape_laudo_2.JPG',
    "cor" => $cor
  );

  if(!empty($emails)){
    $email = new Email($template);
    $email->preencherTemplate($array_template);
    $email->setDestinatarios($emails);
    $email->setAssunto($assunto);
    $email->enviar();
  }
}

echo json_encode($retorno_array = [
  'retorno'=> $retorno['retorno'],
  'id'=>null
]);

?>
