<?php
require_once '../lib/crud.php';
require_once '../lib/email.class.php';

$data = json_decode(file_get_contents("php://input"));
$situacao = $data->situacao;

$retorno = Crud::getInstance('contestacao')->update(array('situacao' => $situacao->tipo), array('id' => $situacao->id));
$dados = Crud::getInstance()->getSQLGeneric('SELECT * from contestacao where id = ?', array($situacao->id), false);
$emails = Crud::getInstance()->getSQLGeneric('SELECT * from email_contestacao where id_contestacao = ?', array($situacao->id), TRUE);
$email_enviado = null;

if ($dados->tipo == 'Contestacao') {
  $assunto = 'COMUNICADO DE CONTESTAÇÃO Nº ' . $dados->numero_contrato;
}
else
if ($dados->tipo == 'Documental') {
  $assunto = 'NOTIFICAÇÃO DE PENDÊNCIA Nº ' . $dados->numero_contrato;
}

$array_template = array(
  "banco" => strtoupper($dados->banco),
  "numero_contrato" => $dados->numero_contrato,
  "titulo" => null,
  "sub-titulo" => null,
  "sub-titulo2" => null,
  "img_cabecalho" => null,
  "img_rodape" => null
);

if($situacao->tipo == 1){

  $array_template['titulo'] = 'RESPONDIDA AO BANCO!';
  $array_template['sub-titulo'] = 'Enviamos sua defesa/pendência ao Banco para que auxilie na análise da reclamação';
  $array_template['sub-titulo2'] = 'Agradecemos seu empenho';
  $array_template['img_cabecalho'] = 'http://www.ltpromotora.com.br/imagem/concluido-cabecalho_1.JPG';
  $array_template['img_rodape'] = 'http://www.ltpromotora.com.br/imagem/concluido-rodape_2.JPG';

  $email_enviado = enviar_email('email_situacao.tpl', $array_template, $emails, $assunto);

}else if($situacao->tipo == 2){

  $array_template['titulo'] = 'NÃO RESPONDIDA NO TEMPO DETERMINADO!';
  $array_template['sub-titulo'] = 'Sua Contestação foi concluída sem o retorno da filial e isso pode gerar um prejuízo financeiro para a empresa por omissão';
  $array_template['sub-titulo2'] = 'Precisamos de comprometimento dos responsáveis a fim de evitar qualquer perca na operação';
  $array_template['img_cabecalho'] = 'http://www.ltpromotora.com.br/imagem/concluido-sem-retorno-cabecalho.jpg';
  $array_template['img_rodape'] = 'http://www.ltpromotora.com.br/imagem/concluido-sem-retorno-rodape.jpg';

  $email_enviado = enviar_email('email_situacao.tpl', $array_template, $emails, $assunto);

}

echo json_encode($retorno_array = [
  'retorno'=> $retorno['retorno'],
  'retorno_email'=> $email_enviado,
  'id'=>null
]);

function enviar_email($template,$array,$emails, $assunto){

  $email = new Email($template);
  $email->preencherTemplate($array);
  $email->setDestinatarios($emails);
  $email->setAssunto($assunto);
  return $email->enviar();

};
?>
