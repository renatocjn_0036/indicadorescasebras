<?php
require_once '../lib/crud.php';
require_once '../lib/email.class.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));
$template = "email.tpl";



if(!empty($data->lista_email)){
  $lista_email = $data->lista_email;
}

if (!empty($data->dados)) {
    $dados = $data->dados;
  
    $array_template = array(
      "cpf" => $dados->cpf,
      "banco" => strtoupper($dados->banco),
      "numero_contrato" => $dados->numero_contrato,
      "cliente" => strtoupper($dados->cliente),
      "unidade" => strtoupper($dados->unidade),
      "nome_agente" => strtoupper($dados->nome_agente),
      "data_atual" => date('d/m/Y'),
      "observacao" => nl2br($dados->observacao),
      "laudo" => $dados->laudo,
      "titulo" => null,
      "sub-titulo" => null,
      "texto" => null,
      "img_cabecalho" => null,
      "img_rodape" =>null
    );

    if(!empty($dados->id)){
      inserir_email($dados->id, $lista_email);
      atualizar($dados);
      exit;
    }

    $retorno_contratos = Crud::getInstance()->getSQLGeneric("SELECT * FROM contestacao where numero_contrato = ? and tipo = ?", array($dados->numero_contrato, $dados->tipo), false);
    if(!empty($retorno_contratos)){

        echo json_encode($json = [
          'retorno'=>false,
          'msg'=>'Número de Contrato Já Cadastrado'
        ]);

        exit;
    }

    $dados->date = date('Y-m-d H:i:s');
    $dados->situacao = 0;
    $dados->laudo = 'Aguard Laudo Banco';
    $retorno = Crud::getInstance('contestacao')->insert($dados);
    $json = [
      'retorno'=>true,
      'msg'=>'Contestação Cadastrada Com Sucesso'
    ];

    inserir_email($retorno['id'], $lista_email);

    if($dados->tipo == 'Contestacao'){

      $assunto = 'COMUNICADO DE CONTESTAÇÃO Nº '.$dados->numero_contrato;
      $array_template['titulo'] = $assunto;
      $array_template['sub-titulo'] = 'Orientações';
      $array_template['texto'] = 'Solicitamos que envie o retorno com os subsídios para o email';
      $array_template['img_cabecalho'] = 'http://www.ltpromotora.com.br/imagem/contestacao_cabecalho.jpg';
      $array_template['img_rodape'] = 'http://www.ltpromotora.com.br/imagem/contestacao-rodape.jpg';

    }else if($dados->tipo == 'Documental'){

      $assunto = 'NOTIFICAÇÃO DE PENDÊNCIA Nº '.$dados->numero_contrato;
      $array_template['titulo'] = $assunto;
      $array_template['sub-titulo'] = 'PENDÊNCIA';
      $array_template['texto'] = 'Para que o banco em questão possa realizar a defesa da ação, precisamos que as seguintes pendências sejam enviadas imediatamente para o email';
      $array_template['img_cabecalho'] = 'http://www.ltpromotora.com.br/imagem/documental-cabecalho.jpg';
      $array_template['img_rodape'] =  'http://www.ltpromotora.com.br/imagem/documental-rodape.png';

    }

    $email = new Email($template);
    $email->preencherTemplate($array_template);
    $email->setDestinatarios($lista_email);
    $email->setAssunto($assunto);
    $email->enviar();

    echo json_encode($json);
    exit;
}

function inserir_email($id, $lista_email){

  Crud::getInstance('email_contestacao')->delete(array('id_contestacao' => $id));

  foreach ($lista_email as $key => $value) {
    $ob = new stdClass();
    $ob->email = $value->email;
    $ob->id_contestacao = $id;
    Crud::getInstance('email_contestacao')->insert($ob);

  }
}

function atualizar($dados){
  $id = $dados->id;
  unset($dados->id);
  unset($dados->dias);
  unset($dados->date);
  unset($dados->situacao);
  Crud::getInstance('contestacao')->update($dados, array('id' => $id));

  $json = [
    'retorno'=>true,
    'msg'=>'Contestação Atualizada Com Sucesso'
  ];
  echo json_encode($json);

}



?>
