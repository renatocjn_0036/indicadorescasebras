<?php
require_once '../lib/crud.php';
$dao = Crud::getInstance();
$arrayParams = array();
$where = [];

$data = json_decode(file_get_contents("php://input"));

if(!empty($data->uf)){
  $where[] = "uf = ?";
  $arrayParams[] = $data->uf;
}


// if(!empty($data->uf)) {
//   $filtros = $data->uf;
//   foreach($filtros as $value) {
//     print_r($value);exit;
//     if($value != '' || $value != null){
//       $where[] = "{$key} = ?";
//       $arrayParams[] = $value;
//     }
//   }
// }

$sql = 'SELECT * FROM unidades';

if (!empty($where)) {
  $sql.= ' WHERE ' . implode(' AND ', $where);
}

$sql .= ' ORDER BY nome'; 

$unidades = $dao->getSQLGeneric($sql, $arrayParams, TRUE);

echo json_encode($unidades);
?>
