<?php

require_once '../lib/crud.php';

$dao = Crud::getInstance();

$data = json_decode(file_get_contents("php://input"));

$attributes = $dao->getSQLGeneric("select * from contract_attribute_column_numbers where datasource_id = {$data->id}");
echo json_encode($attributes);

?>
