<?php
error_reporting(0);
require_once '../lib/crud.php';


$data = json_decode(file_get_contents("php://input"));
$datasources = $data->datasources;
$attribute = $data->attribute;

$Dados = array();
$Cond = array();

foreach ($attribute as $key => $value) {
  $Dados['column_number'] = $value;
  $Cond['attribute_name'] = $key;
  $Cond['datasource_id'] = $datasources->id;
  Crud::getInstance('contract_attribute_column_numbers')->update($Dados,$Cond);

}

?>
