<?php
require_once '../lib/crud.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));
$arrayParams = [];
$where = [];

if (!empty($data->dados)) {
  $dados = $data->dados;

  if (isset($dados->data_inicial) && isset($dados->data_final)) {
    $data_inicial = substr($dados->data_inicial, 0, 10);
    $data_final = substr($dados->data_final, 0, 10);
    $where[] = "data_credito between '{$data_inicial}' and '{$data_final}'";
  }


  unset($dados->data_inicial);
  unset($dados->data_final);
  $arrayParams = array();
  foreach($dados as $key => $value) {
    $where[] = "{$key} = ?";
    $arrayParams[] = $value;
  }
}

$sql = 'SELECT * FROM contratos';
if (!empty($where)) {
  $sql.= ' WHERE ' . implode(' AND ', $where) . ' and data_credito != "null"';
}

$datasources = $dao->getSQLGeneric($sql, $arrayParams);
echo json_encode($datasources);
?>
