<?php
require_once '../lib/crud.php';

$data = json_decode(file_get_contents("php://input"));
$contratos = $data->dados;
$pendencia = $data->pendencia;

$count = 0;
$ob = new stdClass();
$ob->id_tipo_pendencia = $pendencia;

foreach ($contratos as $key => $value) {
  Crud::getInstance('contratos')->update($ob, array('id' => $value->id));
  $count++;
}

echo json_encode($retorno_array = [
  'retorno'=> true,
  'count'=> $count
]);
?>
