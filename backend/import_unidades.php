<?php 
require_once __DIR__ . '/lib/crud.php';
require_once __DIR__ . '/lib/importers/function.php';

$file = 'unidades.csv';

$fp = fopen($file, "r");
if (!$fp) {
  throw new Exception('Arquivo de importação não encontrado');
}

$x = 0;
while (($linhaQuebrada = fgetcsv($fp, 0, ";")) !== FALSE) { 
    if($x > 0){
        $ob->uf = $linhaQuebrada[0];
        $ob->nome = trocar_acentos($linhaQuebrada[1]);
        Crud::getInstance('unidades')->insert($ob);
    }
    
    $x++;
}
?>