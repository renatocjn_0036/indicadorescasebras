<?php

require_once '../lib/crud.php';

$dao = Crud::getInstance();


$sql = "select datasources.*, data_import.datasource_id, data_import.id,date_format(data_import.date_import, '%d/%m/%Y %H:%i:%s') as data_formatada,data_import.failed,data_import.err_message,
data_import.situacao_contrato, s.tipo
from datasources datasources join data_import data_import on datasources.id = data_import.datasource_id left join situacao s on data_import.situacao_contrato = s.id order by date_import desc";


$data_imports = $dao->getSQLGeneric($sql);
echo json_encode($data_imports);

?>
