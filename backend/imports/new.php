<?php
require_once '../lib/crud.php';

$datasource = $_POST['datasource'];
$situacao_contrato = $_POST['situacao_contrato'];
$import_file = $_FILES['import_file'];

$errors = array();
$datasources_dao = Crud::getInstance('datasources');

if (empty($datasource)) {
  $errors['datasource'] = "Não pode ser vazio";
} else if ($datasources_dao->Verificar('select * from datasources where id = ?', $datasource) != 1) {
  $errors['datasource'] = "Fonte não encontrada";
}

$extensoes_permitidas = array('csv');
$exp = explode('.', $import_file['name']);
$extensao = end($exp);
if (empty($import_file)) {
  $errors['import_file'] = "Arquivo deve ser enviado";
} else if (!in_array($extensao, $extensoes_permitidas)) {
  $errors['import_file'] = "Extensão de arquivo inválida";
} else {
  $filename = $import_file['tmp_name'];
  $import_file['newname'] = __DIR__.'/../../uploads/'.uniqid('import_file_', true); //FIXME retirar o subdiretorio do caminho
  move_uploaded_file($filename, $import_file['newname']);
}

header("Content-type: text/html; charset=utf-8");
if (!empty($errors)) {
  header("Location: /indicadorescasebras/#/importacao?erros=".json_encode($errors)); //FIXME Testar
} else {
  $data_import_dao = Crud::getInstance('data_import');
  // $ds = Crud::getInstance()->getSQLGeneric("select * from datasources where id = ?", array($datasource), FALSE);
  $new_data_import = $data_import_dao->insert(array(
    'datasource_id' => $datasource,
    'import_file_path' => $import_file['newname'],
    'date_import' => date('Y-m-d H:i:s'),
    'situacao_contrato' => $situacao_contrato
  ));

  require_once '../lib/background_jobs.php';
  echo ImportadorDeArquivo::importar_arquivo_assync($new_data_import['id']);
  header("Location: /indicadorescasebras/#/importacao"); //FIXME remover subdiretorio
}

?>
