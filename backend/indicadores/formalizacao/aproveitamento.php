<?php
error_reporting(0);
require_once '../../lib/crud.php';
require_once '../../lib/importers/function.php';

$data = json_decode(file_get_contents("php://input"));


$where = " '".substr($data->indicador->data_inicial,0,10)." 00:00:00' and '".substr($data->indicador->data_final,0,10)." 23:59:59'";
// $total_harpia_situacao = total_harpia(true, $where);
//
// $total_work = "SELECT count(id) as total from contratos where data_operacao between " . $where . " and produto NOT IN ('PORTABILIDADE','PORTAB/REFIN', 'SAQUE COMPL.', 'RECOMPRA')
// and (valor > 2000 or (valor >= 1500 and valor < 2000 and produto = 'CARTAO')) and devname_banco <> 'ITAU' ";

unset($data->indicador->data_inicial);
unset($data->indicador->data_final);
foreach ($data->indicador as $key => $value) {

  if(!in_array('TODOS', $value) && !empty($value)){
    $where .= " and $key in (" . join(",", array_to_string($value)) . ")";
  }

  if(!empty($value)){
    $group[] ="contrato.".$key;
  }
}

$sql = "SELECT " . join(",", $group) . ",contrato.data_pendencia,contrato.dias_pendencia,contrato.data_credito, contrato.id_tipo_pendencia,contrato.desc_pendencia, count(contrato.id) as total, situacao.tipo as tipo_pendencia
from contratos contrato left join situacao situacao on contrato.id_tipo_pendencia = situacao.id where data_operacao between " . $where . " group by desc_pendencia, id_tipo_pendencia,dias_pendencia, " . join(", ", $group) . " order by " . join(", ", $group) . "   ";

$retorno = Crud::getInstance()->getSQLGeneric($sql);

$json = [
    'indicador_aproveitamento' => !empty($retorno) ? aproveitamento($retorno) : 0
  ];


echo json_encode($json);

function aproveitamento($array){
  foreach ($array as $key => $value) {

    if(!empty($value->data_credito) || !empty($value->desc_pendencia)){
      $total_pagos += $value->total;
    }

      $total += $value->total;
  }

  return round($total_pagos/$total*100,2);
}

?>
