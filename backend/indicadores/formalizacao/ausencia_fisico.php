<?php
// TODO FORMALIZACAO
error_reporting(0);
require_once '../../lib/crud.php';
require_once '../../lib/importers/function.php';

$data = json_decode(file_get_contents("php://input"));
$indicadores = $data->indicadores;
$select = $indicadores->agrupamento;
$data_inicial = substr($indicadores->data_inicial, 0, 10);
$data_final = substr($indicadores->data_final, 0, 10);
$filtros = $data->filtros;

$x = 1;

foreach($select as $key => $value) {
  if ($x == 1) {
    $arraySelect[] = "$value as agrupamento";
    $x = $x + 1;
  }
  else {
    $arraySelect[] = "$value as subagrupamento";
  }
}

$sql = "SELECT " . join(",", $arraySelect) . ",count(c.id) as total,
CASE WHEN c.pendency = 1 THEN 'Regularizado'
WHEN c.pendency = 2 THEN 'Parcial'
WHEN c.pendency = 3 THEN 'Completa'
ELSE '' END AS pendency,
c.state from contracts c join import_contracts ic on c.id = ic.contract_id join data_import di on di.id = ic.data_import_id where di.datasource_id = 4 AND
c.date between '" . $data_inicial . "'  and '" . $data_final . "' and c.state = 1 ";

if (!empty($filtros)) {
  $sql .= fazer_filtro($filtros);
}
$sql .= "  group by  " . join(",", $select) . ", c.pendency";

$retorno = Crud::getInstance()->getSQLGeneric($sql);
$total_finalizados = array_reduce($retorno, function($valor, $produto){

  return $valor += $produto->total;

});

$total_regularizados = array_reduce($retorno, function($valor, $produto){
    if($produto->pendency == 'Regularizado'){
      $valor += $produto->total;
    }
    return $valor;
});
$total_completa = array_reduce($retorno, function($valor, $produto){

    if($produto->pendency == 'Completa'){
      $valor += $produto->total;
    }
    return $valor;
});
$total_parcial = array_reduce($retorno, function($valor, $produto){
    if($produto->pendency == 'Parcial'){
      $valor += $produto->total;
    }
    return $valor;
});


$agrupamentos = array_associativo_subagrupamento($retorno);

foreach($agrupamentos as $uf => $value) {
  $array_keys = array_keys($value);
  for ($i = 0; $i < count($array_keys); $i++) {
    $array_map = array_map( function ($map){

        $map['perc_regularizado'] = @round($map['Regularizado']/$map['total'] * 100, 2);

        $map['perc_completa'] = @round($map['Completa'] /$map['total']*100, 2);

        $map['perc_parcial'] = @round($map['Parcial'] /$map['total'] * 100, 2);

      return $map;
    }

    , $value);
    $ob = new stdClass();
    $ob->uf = $uf;
    $ob->rows = !empty($array_keys[$i]) ? $uf . " - " . $array_keys[$i] : $uf;
    $ob->subagrupamento = $array_keys[$i];

    $ob->regularizado = $array_map[$array_keys[$i]]['Regularizado'];
    $ob->perc_regularizado = $array_map[$array_keys[$i]]['perc_regularizado'];

    $ob->completa = $array_map[$array_keys[$i]]['Completa'];
    $ob->perc_completa = $array_map[$array_keys[$i]]['perc_completa'];


    $ob->parcial = $array_map[$array_keys[$i]]['Parcial'];
    $ob->perc_parcial = $array_map[$array_keys[$i]]['perc_parcial'];


    $ob->total_by_uf = $array_map[$array_keys[$i]]['total'];
    $ob->total_finalizados = $total_finalizados;
    $novo_array[] = $ob;
  }
}

echo json_encode(unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($novo_array))));

function array_associativo_subagrupamento(array $array){
  $agrupamentos = array();
  $agrupamento = '';
  $subagrupamento = '';

  foreach($array as $key => $value) {
    if ($value->agrupamento != $agrupamento) {
      $agrupamento = $value->agrupamento;
      $agrupamentos[$agrupamento] = array();
    }

    if ($value->subagrupamento != $subagrupamento || $value->agrupamento != $agrupamento) {
      $subagrupamento = $value->subagrupamento;
      $agrupamentos[$agrupamento][$subagrupamento] = array();
    }
    $agrupamentos[$agrupamento][$subagrupamento]['total'] += $value->total;
    $agrupamentos[$agrupamento][$subagrupamento][$value->pendency] = $value->total;
  }
  return $agrupamentos;
}
?>
