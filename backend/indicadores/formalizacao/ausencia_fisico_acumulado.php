<?php
// TODO FORMALIZACAO
// error_reporting(0);
require_once '../../lib/crud.php';

$data = json_decode(file_get_contents("php://input"));
$indicadores = $data->indicadores;
$data_inicial = substr($indicadores->data_inicial, 0, 10);
$data_final = substr($indicadores->data_final, 0, 10);


$sql = "SELECT count(c.id) as total,
CASE WHEN c.pendency = 1 THEN 'Regularizado'
WHEN c.pendency = 2 THEN 'Parcial'
WHEN c.pendency = 3 THEN 'Completa'
ELSE '' END AS pendency,
c.state from contracts c join import_contracts ic on c.id = ic.contract_id join data_import di on di.id = ic.data_import_id where di.datasource_id = 4 AND
c.date between '" . $data_inicial . "'  and '" . $data_final . "' and c.state = 1  group by c.pendency, c.state ";
$retorno = Crud::getInstance()->getSQLGeneric($sql);

if(!empty($retorno)){

  $total = array_reduce($retorno, function($valor, $sub) {
    $valor += $sub->total;
    return $valor;
  });

$retornoMapeado = array_map(function ($value){
   global $total;
   $value->perc = round($value->total/$total*100, 2);

  return $value;
},$retorno);

print_r(json_encode($retornoMapeado));

}

?>
