<?php
error_reporting(0);
require_once '../../lib/crud.php';
require_once '../../lib/importers/function.php';

$data = json_decode(file_get_contents("php://input"));
$indicadores = $data->indicadores;
$data_inicial = substr($indicadores->data_inicial, 0, 10);
$data_final = substr($indicadores->data_final, 0, 10);
$select = $indicadores->agrupamento;

$x = 1;

foreach($select as $key => $value) {
  if ($x == 1) {
    $select = "$value";
  }
  $x = $x + 1;
}
$sql = "SELECT count(c.id) as total, $select as agrupamento,c.pendency, DATEDIFF(date_format(now(), '%Y-%m-%d'), c.date) as dias_pendente,
c.state from contracts c join import_contracts ic on c.id = ic.contract_id join data_import di on di.id = ic.data_import_id where di.datasource_id = 4
and c.state = 1 and c.pendency in (2,3) group by dias_pendente order by $select, dias_pendente";

$retorno = Crud::getInstance()->getSQLGeneric($sql);

  $total_0_30 = 0;
  $total_30_45 = 0;
  $total_45_91 = 0;
  $total_91_120 = 0;
  $total_120 = 0;
  $total_geral = 0;

  foreach ($retorno as $key => $value) {
    $value->dias_pendente = intval($value->dias_pendente);
    $total_geral += intval($value->total);

    if($value->dias_pendente < 30){
      $total_0_30 += intval($value->total);
      $novo_array['0-30'][$value->agrupamento] += intval($value->total);
    }
    if($value->dias_pendente>30 && $value->dias_pendente<45){
      $total_30_45 += intval($value->total);
      $novo_array['30-45'][$value->agrupamento] += intval($value->total);
    }
    if($value->dias_pendente > 45 && $value->dias_pendente < 91){
      $total_45_91 += intval($value->total);
      $novo_array['46-90'][$value->agrupamento] += intval($value->total);
    }
    if($value->dias_pendente > 90 && $value->dias_pendente < 121){
      $total_91_120 += intval($value->total);
      $novo_array['91-120'][$value->agrupamento] += intval($value->total);
    }
    if($value->dias_pendente > 120){
      $total_120 += intval($value->total);
      $novo_array['120'][$value->agrupamento] += intval($value->total);
    }

  }
  
  print_r($novo_array);
  // foreach($novo_array as $agrupamento => $value) {
  //   $array_keys = array_keys($value);
  //   for ($i = 0; $i < count($array_keys); $i++) {
  //     $array_map = array_map( function ($map){
  //       print_r($map);
  //         // $map['perc_regularizado'] = @round($map['Regularizado']/$map['total'] * 100, 2);
  //         //
  //         // $map['perc_completa'] = @round($map['Completa'] /$map['total']*100, 2);
  //         //
  //         // $map['perc_parcial'] = @round($map['Parcial'] /$map['total'] * 100, 2);
  //
  //       return $map;
  //     }
  //
  //     , $value);
      // $ob = new stdClass();
      // $ob->uf = $uf;
      // $ob->rows = $uf . " - " . $array_keys[$i];
      // $ob->subagrupamento = $array_keys[$i];
      //
      // $ob->regularizado = $array_map[$array_keys[$i]]['Regularizado'];
      // $ob->perc_regularizado = $array_map[$array_keys[$i]]['perc_regularizado'];
      //
      // $ob->completa = $array_map[$array_keys[$i]]['Completa'];
      // $ob->perc_completa = $array_map[$array_keys[$i]]['perc_completa'];
      //
      //
      // $ob->parcial = $array_map[$array_keys[$i]]['Parcial'];
      // $ob->perc_parcial = $array_map[$array_keys[$i]]['perc_parcial'];
      //
      //
      // $ob->total_by_uf = $array_map[$array_keys[$i]]['total'];
      // $ob->total_finalizados = $total_finalizados;
      // $novo_array[] = $ob;
  //   }
  // }

  // echo json_encode($novo_array);
// $agrupamento = '';
// $agrupamentos = array();
// foreach ($retorno as $key => $value) {
//   if ($value->agrupamento != $agrupamento) {
//     $agrupamento = $value->agrupamento;
//     $agrupamentos[$agrupamento] = array();
//   }
//   $agrupamentos[$agrupamento][$value->dias_pendente] = $value->total;
// }
//
// print_r($agrupamentos);
// exit;

?>
