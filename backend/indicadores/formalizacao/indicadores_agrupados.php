<?php
error_reporting(0);
require_once '../../lib/crud.php';
require_once '../../lib/importers/function.php';

$data = json_decode(file_get_contents("php://input"));


// $where = " '".substr($data->indicador->data_inicial,0,10)." 00:00:00' and '".substr($data->indicador->data_final,0,10)." 23:59:59'";
// $total_harpia_situacao = total_harpia(true, $where);
//
// $total_work = "SELECT count(id) as total from contratos where data_operacao between " . $where . " and produto NOT IN ('PORTABILIDADE','PORTAB/REFIN', 'SAQUE COMPL.', 'RECOMPRA')
// and (valor > 2000 or (valor >= 1500 and valor < 2000 and produto = 'CARTAO')) and devname_banco <> 'ITAU' ";

unset($data->indicador->data_inicial);
unset($data->indicador->data_final);
foreach ($data->indicador as $key => $value) {

  if(!in_array('TODOS', $value) && !empty($value)){
    $where .= "where $key in (" . join(",", array_to_string($value)) . ")";
  }

  if(!empty($value)){
    $group[] ="contrato.".$key;
  }
}

$sql = "SELECT " . join(",", $group) . ",contrato.data_pendencia,contrato.dias_pendencia,contrato.data_credito, contrato.id_tipo_pendencia,contrato.desc_pendencia, count(contrato.id) as total, situacao.tipo as tipo_pendencia
from contratos contrato left join situacao situacao on contrato.id_tipo_pendencia = situacao.id " . $where . " group by desc_pendencia, id_tipo_pendencia,dias_pendencia, " . join(", ", $group) . " order by " . join(", ", $group) . "   ";

$retorno = Crud::getInstance()->getSQLGeneric($sql);

$retorno = traducao_pendencia($retorno);
foreach($retorno as $key => $value) {
  $novo_array_uf[$value->uf][$value->pendencia] += $value->total;
  $novo_array_banco[$value->devname_banco][$value->pendencia] += $value->total;
  $novo_array_work[$value->tipo_pendencia] += $value->total;
}

$json = [
    'indicador_aproveitamento' => aproveitamento($retorno),
    'indicador_pendencia' => indicador_pendencia($retorno),
    'indicador_banco' => prepare_array($novo_array_banco),
    'indicador_pendencia_work' => prepare_array_2($novo_array_work),
    'indicador_uf' => prepare_array($novo_array_uf),
    'dias_atraso' => dias_atraso($retorno)
  ];

echo json_encode($json);


function dias_atraso($array){

  foreach ($array as $key => $value) {

    if($value->tipo_pendencia != "Reg Empresa" && !empty($value->tipo_pendencia)){
      $total += $value->total;
      if($value->dias_pendencia < 31){
        $total_0_30 += $value->total;
      }else if($value->dias_pendencia > 30 && $value->dias_pendencia < 46){
        $total_30_45 += $value->total;
      }else if($value->dias_pendencia > 45 && $value->dias_pendencia < 91){
        $total_45_90 += $value->total;
      }else if($value->dias_pendencia > 90){
        $total_90 += $value->total;
      }
    }
  }

  return array(
    'zero_trinta' => !empty($total_0_30) ? round($total_0_30/$total*100, 2): 0,
    'trinta_quarenta' => !empty($total_30_45) ? round($total_30_45/$total*100, 2) : 0,
    'quarenta_noventa' => !empty($total_45_90) ? round($total_45_90/$total*100, 2) : 0,
    'maisnoventa' => !empty($total_90) ? round($total_90/$total*100, 2) : 0,
  );
}

function prepare_array_2($array){
  foreach ($array as $key => $value) {
      if(!empty($key)){
        $ob = new stdClass();
        $ob->row = $key;
        $ob->total = $value;
        $novo_array[] = $ob;
      }
    }
    return $novo_array;
  }


function traducao_pendencia($retorno){
  return $arrayMapeado = array_map(function($map){
      $array_pendencia_completa = ['FISICO NAO ENTREGUE AO BANCO', 'FALTA', '407 - PENDENCIA DE DOCUMENTO FISICO', 'DOCUMENTO NAO RECEPCIONADO', 'FISICO PENDENTE'];
      if(!empty($map->desc_pendencia)){
        $key = in_array(trim($map->desc_pendencia), $array_pendencia_completa);
        if($key){
          $map->pendencia = 'completa';
        }else{
          $map->pendencia = 'parcial';
        }
      }
      return $map;
    }, $retorno);
}

function aproveitamento($array){
  foreach ($array as $key => $value) {
    $total += $value->total;
  }

  return array(
    'total' =>$total,
    'total_pagos' => total_pagos($array),
    'aproveitamento' => round(total_pagos($array)/$total*100,2)
  );

  return $array_aproveitamento;
}

function indicador_pendencia($array){
  $novo_array = traducao_pendencia($array);
  foreach ($novo_array as $key => $value) {
    if($value->pendencia == 'completa'){
      $total_pendencia_completa += $value->total;
    }else if($value->pendencia == 'parcial'){
      $total_pendencia_parcial += $value->total;
    }
  }
  return array(
    'perc_pendencia_parcial' => round($total_pendencia_parcial/total_pagos($array)*100,2),
    'perc_pendencia_completa' => round($total_pendencia_completa/total_pagos($array)*100,2),
    'total_pendencia' =>$total_pendencia_parcial+$total_pendencia_completa,
    'total_parcial' =>$total_pendencia_parcial,
    'total_completa' =>$total_pendencia_completa,
  );

}

function total_pagos($array){
  $total = 0;
  foreach ($array as $key => $value) {
    if(!empty($value->data_credito) || !empty($value->desc_pendencia)){
      $total += $value->total;
    }
  }
  return $total;
}

function prepare_array($array){
  foreach ($array as $key => $value) {
      $ob = new stdClass();
      $ob->row = $key;
      $ob->parcial = isset($value['parcial']) ? $value['parcial'] : null;
      $ob->completa = isset($value['completa']) ? $value['completa'] : null;
      $novo_array[] = $ob;
    }
    return $novo_array;
  }


  function array_to_string($array){
    return $novo_array = array_map(function($map){
      $map = "'$map'";
      return $map;
    }, $array);
  }
?>
