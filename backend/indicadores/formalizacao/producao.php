<?php
// TODO FORMALIZACAO
error_reporting(0);
require_once '../../lib/crud.php';
require_once '../../lib/importers/function.php';

$data = json_decode(file_get_contents("php://input"));
$indicadores = $data->indicadores;
$select = $indicadores->agrupamento;
$data_inicial = substr($indicadores->data_inicial, 0, 10);
$data_final = substr($indicadores->data_final, 0, 10);
$filtros = $data->filtros;

$x = 1;

foreach($select as $key => $value) {
  if ($x == 1) {
    $arraySelect[] = "$value as agrupamento";
    $x = $x + 1;
  }
  else {
    $arraySelect[] = "$value as subagrupamento";
  }
}

$sql = "select " . join(",", $arraySelect) . ",count(c.id) as total,c.pendency,
CASE WHEN c.state = 1 THEN '1'
ELSE '0' END AS state
from contracts c join import_contracts ic on c.id = ic.contract_id join data_import di on di.id = ic.data_import_id where di.datasource_id = 4 AND
c.date between '" . $data_inicial . "'  and '" . $data_final . "' ";
if (!empty($filtros)) {
  $sql .= fazer_filtro($filtros);
}
$sql .= " group by " . join(",", $select) . ", c.state";

$retorno = Crud::getInstance()->getSQLGeneric($sql);
// print_r($retorno);
// exit;

$agrupamentos = array();
$agrupamento = '';
$subagrupamento = '';

foreach($retorno as $key => $value) {
  if ($value->agrupamento != $agrupamento) {
    $agrupamento = $value->agrupamento;
    $agrupamentos[$agrupamento] = array();
  }

  if ($value->subagrupamento != $subagrupamento || $value->agrupamento != $agrupamento) {
    $subagrupamento = $value->subagrupamento;
    $agrupamentos[$agrupamento][$subagrupamento] = array();
  }
  $agrupamentos[$agrupamento][$subagrupamento]['total'] += $value->total;
  $agrupamentos[$agrupamento][$subagrupamento][$value->state] = $value->total;
}


foreach($agrupamentos as $uf => $value) {
  $array_keys = array_keys($value);
  for ($i = 0; $i < count($array_keys); $i++) {
    $array_map = array_map(
    function ($map)
    {

        $map['perc_pago'] = @round($map['1'] / $map['total'] * 100, 2);

        $map['perc_nao_pago'] = @round($map['0'] / $map['total'] * 100, 2);


      return $map;
    }

    , $value);

    $ob = new stdClass();
    $ob->uf = $uf;
    $ob->rows = !empty($array_keys[$i]) ? $uf . " - " . $array_keys[$i] : $uf;
    $ob->subagrupamento = $array_keys[$i];
    $ob->pago = $array_map[$array_keys[$i]]['1'];
    $ob->nao_pago = $array_map[$array_keys[$i]]['0'];
    $ob->perc_pago = $array_map[$array_keys[$i]]['perc_pago'];
    $ob->perc_nao_pago = $array_map[$array_keys[$i]]['perc_nao_pago'];
    $ob->total = $array_map[$array_keys[$i]]['total'];
    $novo_array[] = $ob;
  }
}
// print_r($novo_array);
// exit;
print_r(json_encode(unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($novo_array)))));

?>
