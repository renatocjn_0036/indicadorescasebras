<?php
// TODO FORMALIZACAO
// error_reporting(0);
require_once '../../lib/crud.php';
require_once '../../lib/importers/function.php';

$data = json_decode(file_get_contents("php://input"));
$indicadores = $data->indicadores;
$data_inicial = substr($indicadores->data_inicial, 0, 10);
$data_final = substr($indicadores->data_final, 0, 10);



$sql = "select count(c.id) as total,
CASE WHEN c.state = 1 THEN '1'
ELSE '0' END AS state
from contracts c join import_contracts ic on c.id = ic.contract_id join data_import di on di.id = ic.data_import_id where di.datasource_id = 4 AND
c.date between '" . $data_inicial . "'  and '" . $data_final . "' ";

$sql .= " group by c.state";

$retorno = Crud::getInstance()->getSQLGeneric($sql);

$total = array_reduce($retorno, function($valor, $sub) {
  $valor += $sub->total;
  return $valor;
});

$array_filter = array_filter($retorno, function($r) use($total){
// print_r($r);
if($r->state == 1){

  $r->total_acumulado = $total;
  $r->perc = round($r->total/$total*100,2);

  return $r;
}

});

print_r(json_encode($array_filter));
// $retornoMapeado = array_map(function ($value) use($total){
//
//  $value->perc = round($value->total/$total*100, 2);
//
// return $value;
// },$retorno);
//
// print_r(json_encode($retornoMapeado
?>
