<?php

require_once '../../lib/crud.php';
$data = json_decode(file_get_contents("php://input"));
$agrupamento = $data->agrupamento;

if(isset($data->filtros_uf)){
foreach ($data->filtros_uf as $key => $value) {
    $array_uf[] = "'$value->name'";
  }
}

$sql = "SELECT $agrupamento from glpi";

if(!empty($_SESSION['filiais']) && empty($array_uf)){

  $sql .= " where uf in (" . join(",", $_SESSION['filiais']) . ") ";

}else if(!empty($array_uf)){

  $sql .= " where uf in (" . join(",", $array_uf) . ") ";
}

$sql .= " group by $agrupamento";

$retorno = Crud::getInstance()->getSQLGeneric($sql);


for($i=0; $i<count($retorno); $i++){
  $arrayTeste = array_values((array) $retorno[$i]);
  $array[$i] = new stdClass();
  $array[$i]->key = $i;
  $array[$i]->name = $arrayTeste[0];
}
print_r(json_encode($array));

?>
