<?php
// TODO GLPI

error_reporting(0);
require_once '../../lib/crud.php';

$data = json_decode(file_get_contents("php://input"));
$indicadores = $data->indicadores;
$filtrosUnidade = $data->filtrosUnidade;
$filtrosUF = $data->filtrosUF;
$select = $indicadores->agrupamento;
$data_inicial = substr($indicadores->data_inicial, 0, 10);
$data_final = substr($indicadores->data_final, 0, 10);

$sqlTotal = "select subcategoria, count(id) as total from glpi where data_fechamento between
'" . $data_inicial . "' and '" . $data_final . "' group by subcategoria";
$retorno_total = Crud::getInstance()->getSQLGeneric($sqlTotal);
$total_criacao_usuario = 0;
$total_bloqueio_usuario = 0;
$total_desbloqueio_usuario = 0;
$total_reinicializacao = 0;
$total_alteracao_de_permissao = 0;

foreach($retorno_total as $key => $value) {
  if ($value->subcategoria == 'ALTERACAO DE PERMISSAO') {
    $total_alteracao_de_permissao = $value->total;
  }

  if ($value->subcategoria == 'BLOQUEIO DE USUARIO') {
    $total_bloqueio_usuario = $value->total;
  }

  if ($value->subcategoria == 'CRIACAO DE USUARIO') {
    $total_criacao_usuario = $value->total;
  }

  if ($value->subcategoria == 'DESBLOQUEIO DE USUARIO') {
    $total_desbloqueio_usuario = $value->total;
  }

  if ($value->subcategoria == 'REINICIALIZACAO') {
    $total_reinicializacao = $value->total;
  }
}

$sql = "select {$select} as agrupamento, subcategoria, count(id) as total from glpi where data_fechamento between '" . $data_inicial . "' and '" . $data_final . "' ";

if (!empty($filtrosUnidade)) {
  foreach($filtrosUnidade as $key => $value) {
    $array[] = "'$value->name'";
  }

  $sql .= " and {$select} in (" . join(",", $array).")";

}else if(!empty($filtrosUF))
{
  foreach($filtrosUF as $key => $value) {
    $array[] = "'$value->name'";
  }
  $sql .= " and uf in (" . join(",", $array) . ")
  ";
}

$sql .= " group by subcategoria, {$select} order by {$select}";

$retorno = Crud::getInstance()->getSQLGeneric($sql);

if (!empty($retorno)) {
  $agrupamentos = array();
  $select = '';
  foreach($retorno as $key => $value) {
    if ($value->agrupamento != $select) {
      $select = $value->agrupamento;
      $agrupamentos[$select] = array();
    }
    $agrupamentos[$select][$value->subcategoria] = $value->total;
  }

  $agrupamentosMapeados = array_map(
  function ($agrupamento)
  {
    global $total_criacao_usuario, $total_bloqueio_usuario, $total_desbloqueio_usuario, $total_reinicializacao, $total_alteracao_de_permissao;
    if (!empty($agrupamento['CRIACAO DE USUARIO'])) {
      $agrupamento['perc_criacao'] = round($agrupamento['CRIACAO DE USUARIO'] / $total_criacao_usuario * 100, 2);
      $agrupamento['total_criacao'] = $total_criacao_usuario;
    }

    if (!empty($agrupamento['BLOQUEIO DE USUARIO'])) {
      $agrupamento['perc_bloqueio'] = round($agrupamento['BLOQUEIO DE USUARIO'] / $total_bloqueio_usuario * 100, 2);
      $agrupamento['total_bloqueio'] = $total_bloqueio_usuario;
    }

    if (!empty($agrupamento['DESBLOQUEIO DE USUARIO'])) {
      $agrupamento['perc_desbloqueio'] = round($agrupamento['DESBLOQUEIO DE USUARIO'] / $total_desbloqueio_usuario * 100, 2);
      $agrupamento['total_desbloqueio'] = $total_desbloqueio_usuario;
    }

    if (!empty($agrupamento['REINICIALIZACAO'])) {
      $agrupamento['perc_reinicializacao'] = round($agrupamento['REINICIALIZACAO'] / $total_reinicializacao * 100, 2);
      $agrupamento['total_reinicializacao'] = $total_reinicializacao;
    }

    if (!empty($agrupamento['ALTERACAO DE PERMISSAO'])) {
      $agrupamento['perc_permissao'] = round($agrupamento['ALTERACAO DE PERMISSAO'] / $total_alteracao_de_permissao * 100, 2);
      $agrupamento['total_permissao'] = $total_alteracao_de_permissao;
    }

    return $agrupamento;
  }

  , $agrupamentos);
  print_r(json_encode($agrupamentosMapeados));
}
else {
  http_response_code(404);
}

?>
