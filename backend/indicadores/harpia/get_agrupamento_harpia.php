<?php
require_once '../../lib/crud.php';

if(!empty($_SESSION['filiais'])){
  $sql_filial = " where uf in (" . join(",", $_SESSION['filiais']) . ") ";
}

$data = json_decode(file_get_contents("php://input"));
if (!empty($data)) {
  $array = array();
    $x = 0;
  foreach($data->agrupamento as $key => $value) {

    $sql = "SELECT DISTINCT ".substr($value,2)." as agrupamento from contracts ";
    // $sql .= $sql_filial;
    $sql .= !empty($sql_filial) ? $sql_filial : '';
    $sql .= " ORDER BY ".substr($value,2)."";

    $retorno = Crud::getInstance()->getSQLGeneric($sql);

    for($i=0; $i<count($retorno);$i++){
      $array[$x] = new stdClass();
      $array[$x]->key = $x;
      $array[$x]->name = $retorno[$i]->agrupamento;
      $array[$x]->group = traducao_group(substr($value,2));
      $x++;
    }

}
echo json_encode($array);

}

function traducao_group($group){
  switch ($group) {
    case 'devname_bank':
      return 'Bancos';
      break;
    case 'company_unit':
      return 'Unidades';
      break;
    case 'operation':
      return 'Operação';
      break;
    case 'devname_partnership':
      return 'Convênio';
      break;
    default:
      return $group;
      break;
  }
}



?>
