
<?php
require_once '../../lib/crud.php';

$data = json_decode(file_get_contents("php://input"));
$data = $data->indicadores;
// $select = $data->agrupamento;
$data_inicial = substr($data->data_inicial, 0, 10);
$data_final = substr($data->data_final, 0, 10);
$sql = "select c.state,count(c.id) as total from contracts c join import_contracts ic on c.id = ic.contract_id join data_import di on di.id = ic.data_import_id where di.datasource_id = 3 AND
c.updated_at between '" . $data_inicial . "'  and '" . $data_final . "' group by state ";
// echo $sql;
// exit;
$retorno = Crud::getInstance()->getSQLGeneric($sql);

if (!empty($retorno)) {

  $total = array_reduce($retorno, function($valor, $sub) {
    $valor += $sub->total;
    return $valor;
  });



  $retornoMapeado = array_map(function ($value){
     global $total;
     $value->perc = round($value->total/$total*100, 2);

    return $value;
  },$retorno);

  print_r(json_encode($retornoMapeado));
}
else {
  http_response_code(404);
}

?>
