<?php
error_reporting(0);
require_once '../../lib/crud.php';
require_once '../../lib/importers/function.php';

$data = json_decode(file_get_contents("php://input"));

$novo_array_uf = array();
$novo_array_banco = array();
$where = '';
$total_aprovados = 0;
$total_pendentes = 0;
$total_recusados = 0;
$total = 0;
$group = [];

$where = "where ultima_data between '".substr($data->indicador->data_inicial,0,10)." 00:00:00' and '".substr($data->indicador->data_final,0,10)." 23:59:59'";
$sql_total = "SELECT situacao, count(id) as total from harpia " . $where . " group by situacao";
$array_total = Crud::getInstance()->getSQLGeneric($sql_total);

foreach ($array_total as $key => $value) {
  $total += $value->total;
  if($value->situacao == 'APROVADO'){
      $total_aprovados = $value->total;
  }
  if($value->situacao == 'PENDENTE'){
      $total_pendentes = $value->total;
  }
  if($value->situacao == 'RECUSADO'){
      $total_recusados = $value->total;
  }
}

unset($data->indicador->data_inicial);
unset($data->indicador->data_final);

foreach ($data->indicador as $key => $value) {

  if(!in_array('TODOS', $value) && !empty($value)){
    $where .= " and $key in (" . join(",", array_to_string($value)) . ")";
  }

  if(!empty($value)){
    $group[] = $key;
  }
}

$sql = "SELECT " . join(",", $group) . ", situacao, count(id) as total from harpia " . $where . " group by situacao, " . join(", ", $group) . " order by " . join(", ", $group) . "   ";
$retorno = Crud::getInstance()->getSQLGeneric($sql);

foreach($retorno as $key => $value) {
  $novo_array_uf[$value->uf][$value->situacao] = $value->total;
  $novo_array_banco[$value->devname_banco][$value->situacao] += $value->total;
}

$json = [
  'array_uf' => prepare_array($novo_array_uf, $total_aprovados, $total_recusados, $total_pendentes),
  'array_banco' =>prepare_array($novo_array_banco, $total_aprovados, $total_recusados, $total_pendentes),
  'array_total' => $array_total,
  'total' => $total,
  'total_aprovados' => $total_aprovados,
  'total_pendentes' => $total_pendentes,
  'total_recusados' => $total_recusados

];

echo json_encode($json);

function prepare_array($array, $total_aprovados, $total_pendentes, $total_recusados){
  foreach ($array as $key => $value) {
      $ob = new stdClass();
      $ob->row = $key;
      $ob->aprovado = isset($value['APROVADO']) ? $value['APROVADO'] : 0;
      $ob->recusado = isset($value['RECUSADO']) ? $value['RECUSADO'] : 0;
      $ob->pendente = isset($value['PENDENTE']) ? $value['PENDENTE'] : 0;
      $ob->total_aprovados = $total_aprovados;
      $ob->total_recusados = $total_recusados;
      $ob->total_pendentes = $total_pendentes;
      $novo_array[] = $ob;
    }
    return $novo_array;
  }

  function array_to_string($array){
    return $novo_array = array_map(function($map){
      $map = "'$map'";
      return $map;
    }, $array);
  }




?>
