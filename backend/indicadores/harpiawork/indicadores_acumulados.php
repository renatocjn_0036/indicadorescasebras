<?php
// error_reporting(0);
require_once '../../lib/crud.php';

$data = json_decode(file_get_contents("php://input"));
$data = $data->indicadores;
$agrupamento = $data->agrupamento;
$data_inicial = substr($data->data_inicial, 0, 10);
$data_final = substr($data->data_final, 0, 10);


$sql = "select count(c.id) as total,c.state, di.datasource_id as datasource  from contracts c join import_contracts ic on c.id = ic.contract_id join data_import di on di.id = ic.data_import_id
        where di.datasource_id = 3 AND
(c.date between '" . $data_inicial . "'  and '" . $data_final . "' or c.date is null) group by state";

$retornoHarpia = Crud::getInstance()->getSQLGeneric($sql);


$sqlWork = "select count(c.id) as total, di.datasource_id as datasource from contracts c join import_contracts ic on c.id = ic.contract_id join data_import di on di.id = ic.data_import_id where di.datasource_id = 4 AND
c.date between '" . $data_inicial . "'  and '" . $data_final . "'
and c.operation NOT IN ('PORTABILIDADE','PORTAB/REFIN', 'SAQUE COMPL.', 'RECOMPRA')
and (c.value > 2000 or (c.value >= 1500 and c.value < 2000 and c.operation = 'CARTAO')) and c.devname_bank <> 'ITAU'  ";
$retornoWork = Crud::getInstance()->getSQLGeneric($sqlWork);


$totalWork = array_reduce($retornoWork, function($key, $value){
  return $value->total;
});

$arrayMapeado = array_map(function ($array) use ($totalWork){

$array->perc = round($array->total/$totalWork*100,2);
$array->total_work = $totalWork;

return $array;
}, $retornoHarpia);

echo json_encode($arrayMapeado);

?>
