<?php
error_reporting(0);
require_once '../../lib/crud.php';
require_once '../../lib/importers/function.php';

$data = json_decode(file_get_contents("php://input"));


$where = " '".substr($data->indicador->data_inicial,0,10)." 00:00:00' and '".substr($data->indicador->data_final,0,10)." 23:59:59'";
$total_harpia_situacao = total_harpia(true, $where);

$total_work = "SELECT count(id) as total from contratos where data_operacao between " . $where . " and produto NOT IN ('PORTABILIDADE','PORTAB/REFIN', 'SAQUE COMPL.', 'RECOMPRA')
and (valor > 2000 or (valor >= 1500 and valor < 2000 and produto = 'CARTAO')) and devname_banco <> 'ITAU' ";

unset($data->indicador->data_inicial);
unset($data->indicador->data_final);

foreach ($data->indicador as $key => $value) {

  if(!in_array('TODOS', $value) && !empty($value)){
    $where .= " and $key in (" . join(",", array_to_string($value)) . ")";
  }

  if(!empty($value)){
    $group[] = $key;
  }
}

$sqlHarpia = "SELECT " . join(",", $group) . ", situacao, count(id) as total from harpia where ultima_data between " . $where . " group by situacao, " . join(", ", $group) . " order by " . join(", ", $group) . "   ";
$retorno_harpia = Crud::getInstance()->getSQLGeneric($sqlHarpia);

$sqlWork = "select " . join(",", $group) . ",count(id) as total from contratos where data_operacao between ". $where ."  and produto NOT IN ('PORTABILIDADE','PORTAB/REFIN', 'SAQUE COMPL.', 'RECOMPRA')
and (valor > 2000 or (valor >= 1500 and valor < 2000 and produto = 'CARTAO')) and devname_banco <> 'ITAU' group by " . join(", ", $group) . " order by " . join(", ", $group) . "  ";
$retorno_work = Crud::getInstance()->getSQLGeneric($sqlWork);

foreach($retorno_harpia as $key => $value) {
  $novo_array_uf[$value->uf][$value->situacao] += $value->total;
  $novo_array_banco[$value->devname_banco][$value->situacao] += $value->total;
}

foreach($retorno_work as $key => $value) {
  $novo_array_uf[$value->uf]['total_work'] += $value->total;
  $novo_array_banco[$value->devname_banco]['total_work'] += $value->total;
}

$json = [
    'array_uf' => prepare_array($novo_array_uf),
    'array_banco' =>prepare_array($novo_array_banco),
    'total_harpia_situacao' => $total_harpia_situacao,
    'total_work' => Crud::getInstance()->getSQLGeneric($total_work)[0]
  ];

echo json_encode($json);

function total_harpia($groupBy = false, $where){
  $sql = "SELECT situacao, count(id) as total from harpia where ultima_data between " . $where . " ";

  if($groupBy){
    $sql .= "group by situacao";
  }

  return Crud::getInstance()->getSQLGeneric($sql);
}

function prepare_array($array){
  foreach ($array as $key => $value) {
      $ob = new stdClass();
      $ob->row = $key;
      $ob->aprovado = isset($value['APROVADO']) ? $value['APROVADO'] : null;
      $ob->recusado = isset($value['RECUSADO']) ? $value['RECUSADO'] : null;
      $ob->pendente = isset($value['PENDENTE']) ? $value['PENDENTE'] : null;
      $ob->total_work = $value['total_work'];

      if($value['total_work'] != null){
        $novo_array[] = $ob;
      }
    }
    return $novo_array;
  }

  function array_to_string($array){
    return $novo_array = array_map(function($map){
      $map = "'$map'";
      return $map;
    }, $array);
  }
?>
