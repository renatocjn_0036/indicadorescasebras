<?php
error_reporting(0);
require_once '../../lib/crud.php';
error_reporting(0);
$data = json_decode(file_get_contents("php://input"));
$indicadores = $data->indicadores;
$data_inicial = substr($indicadores->data_inicial, 0, 10);
$data_final = substr($indicadores->data_final, 0, 10);

// $sql = "SELECT {$filtro} from glpi where uf in (" . join(",", $array) . ") group by {$filtro} ";

$sql = "SELECT uf as agrupamento, count(id) as total,state_itau from monitoramento where
date between '" . $data_inicial . "'  and '" . $data_final . "' and bank = 'Ibconsig' group by  state_itau, uf order by uf ";

$retorno = Crud::getInstance()->getSQLGeneric($sql);

// print_r($retorno);
// exit;

$total_confirmados = array_reduce($retorno, function($valor, $produto){
    if($produto->state_itau == 'NAO CANCELAR'){
      $valor += $produto->total;
    }
    return $valor;
});

$total_cancelados = array_reduce($retorno, function($valor, $produto){
    if($produto->state_itau == 'CANCELAR'){
      $valor += $produto->total;
    }
    return $valor;
});

$agrupamentos = array();
$agrupamento = '';
foreach($retorno as $key => $value) {
  if ($value->agrupamento != $agrupamento) {
    $agrupamento = $value->agrupamento;
    $agrupamentos[$agrupamento] = array();
  }

  $agrupamentos[$agrupamento]['total_contratos'] += $value->total;
  $agrupamentos[$agrupamento][$value->state_itau] = $value->total;
}

foreach ($agrupamentos as $uf => $value) {
  $array_map = array_map(function ($map){

    $map['perc_confirmado'] = @round($map['NAO CANCELAR']/$map['total_contratos']*100,2);
    $map['perc_cancelado'] = @round($map['CANCELAR']/$map['total_contratos']*100,2);

    return $map;
  }, $agrupamentos);

  $ob = new stdClass();
  $ob->uf = $uf;
  $ob->confirmado = $array_map[$uf]['NAO CANCELAR'];
  $ob->perc_confirmado = $array_map[$uf]['perc_confirmado'];

  $ob->cancelado = $array_map[$uf]['CANCELAR'];
  $ob->perc_cancelado = $array_map[$uf]['perc_cancelado'];

  $ob->sem_retorno = $array_map[$uf]['SEM RETORNO'];
  $ob->total_by_uf = $array_map[$uf]['total_contratos'];
  $ob->total_confirmados = $total_confirmados;
  $ob->total_cancelados = $total_cancelados;
  $novo_array[] = $ob;
}
// print_r($novo_array);
// exit;
print_r(json_encode(unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($novo_array)))));
exit;
//
//
// $total = array_reduce($retorno, function($valor, $sub) {
//   $valor += $sub->total;
//   return $valor;
// });
//
// // print_r($agrupamentos);
// // exit;
// //
// $arrayMapeados = array_map(function ($map) use ($total){
//
// $map->perc = round($map->total/$total*100,2);
// // $map['perc_devolvidos'] = round($map['0']/$map['total']*100,2);
//
//   return $map;
// }, $retorno);
// print_r(json_encode($arrayMapeados));
?>
