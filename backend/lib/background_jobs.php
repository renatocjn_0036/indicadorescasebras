<?php
// error_reporting(1);
//
// ini_set('display_errors',1);
// ini_set('display_startup_erros',1);
// error_reporting(E_ALL);


require_once __DIR__.'/../../vendor/autoload.php';
require_once __DIR__.'/crud.php';
require_once __DIR__.'/importers/import_workbank.php';
require_once __DIR__.'/importers/generic_bank_importer.php';
require_once __DIR__.'/importers/import_glpi.php';
require_once __DIR__.'/importers/import_harpia.php';
require_once __DIR__.'/importers/import_monitoramento.php';
require_once __DIR__.'/importers/import_portabilidade.php';

class ImportadorDeArquivo {
  public static function importar_arquivo_assync($data_import_id) {
    return Resque::enqueue('default', 'ImportadorDeArquivo', array('data_import_id' => $data_import_id));
  }

  public function perform() {
    $data_import_id = $this->args['data_import_id'];

    echo "buscar ID\n";
    $data_import = Crud::getInstance()->getSQLGeneric('select * from data_import inner join datasources on data_import.datasource_id = datasources.id where data_import.id = ?', array($data_import_id), FALSE);
    var_dump($data_import);
    if (!empty($data_import)) {
      try {
        switch ($data_import->type) {
          case "harpia":
            echo "iniciar importação HARPIA\n";
            import_harpia($data_import->datasource_id, $data_import_id, $data_import->import_file_path);
            break;
          case "workbank":
            echo "iniciar importação Workbank\n";
            import_workbank($data_import->datasource_id, $data_import_id, $data_import->import_file_path);
            break;
          case "glpi":
            echo "iniciar importação GLPI\n";
            importar_glpi($data_import->datasource_id, $data_import_id, $data_import->import_file_path);
            break;
          case 'pendencia':
            echo "iniciar importação Pendencia\n";
            importar_pendencias_generico($data_import->datasource_id, $data_import_id, $data_import->import_file_path, $data_import->situacao_contrato);
            break;
          case 'monitoramento':
            echo "iniciar importação Monitoramento\n";
            importar_monitoramento_generico($data_import->datasource_id, $data_import_id, $data_import->import_file_path);
            break;
          case 'portabilidade':
            echo "iniciar importação Portabilidade\n";
            import_portabilidade($data_import->datasource_id, $data_import_id, $data_import->import_file_path);
            break;
          default:
          echo "Tipo de importacao nao encotrada\n";
            break;
        }
        echo "Update not failed\n";
        Crud::getInstance('data_import')->update(array('failed' => 0), array('id' => $data_import_id));
      } catch (Exception $e) {
        echo "Exception\n";
        Crud::getInstance('data_import')->update(array('failed' => 1, 'err_message' => $e->getMessage()), array('id' => $data_import_id));
      } finally {
        echo "Finally\n";
        unlink($data_import->import_file_path);
      }
    } else {
      echo "Import com id {$data_import_id} não encontrado!\n";
    }
  }
}


// public function index($id)
// {
//
// }
?>
