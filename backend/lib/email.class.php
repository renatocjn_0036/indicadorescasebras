<?php
require_once '../../vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

Class Email{


private $template = null;
private $body = null;
private $mail = null;
private $assunto = null;
private $emails = null;

public function __construct($template){
  $this->mail = new PHPMailer(true);

  if(empty($template)){
    throw new Exception("O Template não pode ser vazio");
  }
  $this->template = file_get_contents($template);

}

private function array_keys($array){

  return array_map(function($arr){
      return "%%{$arr}%%";
  }, array_keys($array));

}

private function array_values($array){
  return array_values($array);
}

public function preencherTemplate($dados){
  $this->body = str_replace($this->array_keys($dados), $this->array_values($dados), $this->template);
}

public function setDestinatarios($emails){
  $this->emails = $emails;
}

public function setAssunto($assunto){
  $this->assunto = $assunto;
}


public function enviar(){
  if(empty($this->assunto)){
    throw new Exception("O Assunto não pode ser vazio");
  }

  if(empty($this->emails)){
    throw new Exception("O E-mail não pode ser vazio");
  }
  $this->mail->Body = $this->body;
  $this->mail->SMTPDebug = 1;
  $this->mail->isSMTP();
  $this->mail->Host = '10.50.0.16';
  $this->mail->SMTPAuth = true;
  $this->mail->Username = 'luizmedeiros@casebras.com.br';
  $this->mail->Password = 'case123';
  $this->mail->SMTPSecure = 'tls';
  $this->mail->Port = 587;

  $this->mail->SMTPOptions = array(
    'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
      'allow_self_signed' => true
    )
  );
  $this->mail->setFrom('fraudes@casebras.com.br', 'Casebrás - Qualidade');
  $this->mail->ClearAllRecipients();
    if(is_array($this->emails)){
      foreach($this->emails as $key => $value) {
          $this->mail->AddCC($value->email);
      }
    }else{
      $this->mail->AddCC($this->emails);
    }
  $this->mail->addReplyTo('qualidade@casebras.com.br');
  $this->mail->isHTML(true);
  $this->mail->CharSet = 'utf-8';
  $this->mail->Subject = $this->assunto;
  $enviado = $this->mail->Send();
  $this->mail->ClearAttachments();

  if (!$enviado) {
    return $mail->ErrorInfo;
  }
  else {
    return true;
  }
}


}
?>
