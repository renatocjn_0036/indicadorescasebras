<?php

function is_utf8( $string ){
return preg_match( '%^(?:
	 [\x09\x0A\x0D\x20-\x7E]
	| [\xC2-\xDF][\x80-\xBF]
	| \xE0[\xA0-\xBF][\x80-\xBF]
	| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}
	| \xED[\x80-\x9F][\x80-\xBF]
	| \xF0[\x90-\xBF][\x80-\xBF]{2}
	| [\xF1-\xF3][\x80-\xBF]{3}
	| \xF4[\x80-\x8F][\x80-\xBF]{2}
	)*$%xs',
	$string
);
}

/**
* Remove os acentos de uma string
* @param string $string
* @return string
*/
function trocar_acentos( $string ){
return strtoupper(preg_replace(
	array(
		//Maiúsculos
		'/\xc3[\x80-\x85]/',
		'/\xc3\x87/',
		'/\xc3[\x88-\x8b]/',
		'/\xc3[\x8c-\x8f]/',
		'/\xc3([\x92-\x96]|\x98)/',
		'/\xc3[\x99-\x9c]/',

		//Minúsculos
		'/\xc3[\xa0-\xa5]/',
		'/\xc3\xa7/',
		'/\xc3[\xa8-\xab]/',
		'/\xc3[\xac-\xaf]/',
		'/\xc3([\xb2-\xb6]|\xb8)/',
		'/\xc3[\xb9-\xbc]/',
	),
	str_split( 'ACEIOUaceiou' , 1 ),
	is_utf8( $string ) ? $string : utf8_encode( $string )
));
}

function exceptions_error_handler($severity, $message, $filename, $lineno)
{
  if (error_reporting() == 0) {
    return;
  }

  if (error_reporting() & $severity) {
    throw new ErrorException($message, 0, $severity, $filename, $lineno);
  }
}


function traducaoSubCategoria($subcategoria){

	switch (trocar_acentos(trim($subcategoria))) {

		case 'INCLUSAO DE PERMISSAO':
		case 'ALTERACAO DE PERMISSAO':
		case 'EXCLUSAO DE PERMISSAO':
			return 'ALTERACAO DE PERMISSAO';
			break;
		case 'REINICIALIZACAO DE SENHA':
		case 'REINICIALIZACAO DE USUARIO':
			return 'REINICIALIZACAO';
			break;
		default:
			return trocar_acentos($subcategoria);
			break;
		}

	}

function moeda($get_valor) {
	$source = array('.', ',', "R$");
	$replace = array('', '.', "");

	$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto

	return floatval($valor); //retorna o valor formatado para gravar no banco

}

/*function encodeToUtf8($string) {
    return mb_convert_encoding($string, "UTF-8", mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true));
}

function trocar_acentos($var) {
    $var = encodeToUtf8(strtoupper($var));

    $var = str_replace(array("Â", "Á", "À", "Â", "Ã"), "A", $var);
    $var = str_replace(array("É", "È", "Ê"), "E", $var);
    $var = str_replace(array("Í", "Ì", "Î"), "I", $var);
    $var = str_replace(array("Ó", "Ò", "Ô", "Õ"), "O", $var);
    $var = str_replace(array("Ú", "Ù", "Û"), "U", $var);
    $var = str_replace(array("Ç"), "C", $var);

    return trim(utf8_decode($var));
}
*/

function TraducaoBanco($banco){
  $dao = Crud::getInstance();
  $arrayCond = new StdClass();
  $bancos = $dao->getSQLGeneric("select * from bank",$arrayCond, TRUE);
  foreach ($bancos as $key => $value) {
    $pos = strpos(strtolower($banco),strtolower($value->name));
    if ($pos !== false) {
      return $value->devname;
    }

  }

}

function traducaoPartnership($partnership){
//	$partnership = trocar_acentos($partnership);
	$dao = Crud::getInstance();
	$partnerships = $dao->getSQLGeneric("select * from partnership");

	foreach ($partnerships as $key => $value) {
		$pos = strpos(trocar_acentos($partnership),$value->name);
		if ($pos !== false) {
			return $value->devname;
		}

	}
}

function inserir_tabela_relacao($id_contrato, $id_data_import, $pendency = null)
{
  $Obj = new stdClass();
  $Obj->contract_id = $id_contrato;
  $Obj->data_import_id = $id_data_import;
	$Obj->pendency = $pendency;
  $dao = Crud::getInstance('import_contracts');
  $dao->insert($Obj);
}

function DateFormat($date){
	if (preg_match("/\d{2}\/\d{2}\/\d{4} \d{2}:\d{2}:\d{2}/", $date))
		return 'd/m/Y H:i:s';
	elseif(preg_match("/\d{2}\/\d{2}\/\d{4} \d{2}:\d{2}/", $date))
		return 'd/m/Y H:i';
 	elseif (preg_match("/\d{4}-\d{2}-\d{2}/", $date))
    return 'Y-m-d';
 	elseif (preg_match("/\d{2}\/\d{2}\/\d{4}/", $date))
    return 'd/m/Y';
 	elseif (preg_match("/\d{2}-\d{2}-\d{4}/", $date))
    return 'd-m-Y';
 	else return FALSE;
}

function pendencias ($linhaQuebrada){

    switch (trocar_acentos($linhaQuebrada)) {
      case "":
      case 'FISICO':
      case 'EM ABERTO':
      case 'ENTREGA':
      case 'PENDENTE DE RECEBIMENTO':
			case 'FALTA':
      case 'NAO PROCESSADO':
        return 3;
        break;
      case 'DOCUMENTACAO':
      case 'REABERTO':
      case 'RECEBIDO':
      case 'FORMALIZADO COM APONTAMENTO':
          return 2;
      default:
        return 2;
        break;
    }
  }

	function formatDate($date){
	  // var_dump($date);
	  $arrayDate = explode(" ", $date);
	  $arrayDate[0] = date("Y-m-d",strtotime(str_replace('/','-',$arrayDate[0])));
	  $dataFormatada = implode(" ", $arrayDate);
	  return $dataFormatada;
	}


	function fazer_filtro($filtros){
	  $sqlFiltro = "";
	  foreach($filtros as $key => $value) {
	    if ($value->group == 'uf') {
	      $uf[] = "'$value->name'";
	    }

	    if ($value->group == 'Bancos') {
	      $devname_bank[] = "'$value->name'";
	    }

	    if ($value->group == 'Unidades') {
	      $company_unit[] = "'$value->name'";
	    }

	    if ($value->group == 'Operação') {
	      $operation[] = "'$value->name'";
	    }

	    if ($value->group == 'Convênio') {
	      $devname_partnership[] = "'$value->name'";
	    }
	  }
	  if (!empty($uf)) {
	    $sqlFiltro.= " and uf in (" . join(",", $uf) . ")";
	  }

	  if (!empty($devname_bank)) {
	    $sqlFiltro.= " and devname_bank in (" . join(",", $devname_bank) . ")";
	  }

	  if (!empty($company_unit)) {
	    $sqlFiltro.= " and company_unit in (" . join(",", $company_unit) . ")";
	  }

	  if (!empty($operation)) {
	    $sqlFiltro.= " and operation in (" . join(",", $operation) . ")";
	  }

	  if (!empty($devname_partnership)) {
	    $sqlFiltro.= " and devname_partnership in (" . join(",", $devname_partnership) . ")";
	  }
	  return $sqlFiltro;
	}

?>
