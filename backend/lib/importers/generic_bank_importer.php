<?php
error_reporting(1);
set_error_handler('exceptions_error_handler');

require_once __DIR__ . '/../crud.php';

require_once 'function.php';

function importar_pendencias_generico($datasource, $data_import_id, $import_file_path,$situacao_contrato)
{
  $fp = fopen($import_file_path, "r");
  if (!$fp) {
    throw new Exception('Arquivo de importação não encontrado');
  }

  $attribute = Crud::getInstance()->getSQLGeneric("select * from contract_attribute_column_numbers where datasource_id = {$datasource} and column_number > 0");
  $arrayAtt = array();

  foreach($attribute as $key => $value) {
    $arrayAtt[$value->attribute_name] = $value->column_number - 1;
  }

  $header = Crud::getInstance()->getSQLGeneric("select header_lines from datasources where id = {$datasource}") [0]->header_lines;
  $array_atual = array();
  $x = 0;
  while (($linhaQuebrada = fgetcsv($fp, 1000, ";")) !== FALSE) {
    try {
      if ($x > $header - 1) {
        foreach($arrayAtt as $key => $value) {
          if ($key == 'data_pendencia') {
              $linhaQuebrada[$value] = formatDate($linhaQuebrada[$value]);
          }

          if ($key == 'agente') {
            $partes = explode("|", $linhaQuebrada[$value]);
            $linhaQuebrada[$value] = $partes[0];
          }
          $arrayDados['id_tipo_pendencia'] = $situacao_contrato;
          $arrayDados[$key] = trocar_acentos($linhaQuebrada[$value]);
        }
          if(!empty($arrayDados['numero'])){
            $dao = Crud::getInstance('contratos')->update($arrayDados, array('numero' => $arrayDados['numero']));
            if($dao['retorno']){
              inserir_tabela_relacao($dao['id'], $data_import_id, $arrayDados['desc_pendencia']);
            }
          }
      }
    }
    catch(Exception $e) {
      echo $e->getMessage();
      throw new Exception('Erro na fonte de dados da planilha');
    }

    $x = $x + 1;
  }

}

?>