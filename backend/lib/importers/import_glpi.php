<?php
set_error_handler('exceptions_error_handler');

require_once __DIR__ . '/../crud.php';

require_once 'function.php';

function importar_glpi($datasource, $data_import_id, $import_file_path)
{
  $fp = fopen($import_file_path, "r");
  if (!$fp) {
    throw new Exception('Arquivo de importação não encontrado');
  }

  $header = Crud::getInstance()->getSQLGeneric("select header_lines from datasources where id = {$datasource}") [0]->header_lines;
  $attribute = Crud::getInstance()->getSQLGeneric("select * from contract_attribute_column_numbers where datasource_id = {$datasource} and column_number > 0");
  $arrayAtt = array();
  foreach($attribute as $key => $value) {
    $arrayAtt[$value->attribute_name] = $value->column_number - 1;
  }

  $x = 0;
  while (($linhaQuebrada = fgetcsv($fp, 0, ";")) !== FALSE) {
    try {
      if ($x > $header - 1) {
        foreach($arrayAtt as $key => $value) {
          if ($key == 'data_abertura' || $key == 'data_fechamento') {
            $linhaQuebrada[$value] = formatDate($linhaQuebrada[$value]);
          }

          if ($key == 'titulo' || $key == 'categoria' || $key == 'unidade') {
            $linhaQuebrada[$value] = trocar_acentos($linhaQuebrada[$value]);
          }
          if($key == 'tipo_unidade'){
            $linhaQuebrada[$value] = trocar_acentos($linhaQuebrada[$value]);
          }

          if ($key == 'subcategoria') {
            $linhaQuebrada[$value] = traducaoSubCategoria($linhaQuebrada[$value]);
            if ($linhaQuebrada[$value] == "") {
              $linhaQuebrada[$value] = traducaoSubCategoria($linhaQuebrada[$value - 1]);
            }
          }

          $arrayDados[$key] = trim($linhaQuebrada[$value]);
        }
//       print_r($arrayDados);
	

        $verificar = Crud::getInstance()->getSQLGeneric("select * from glpi where id_chamado = ?", array(
          $arrayDados['id_chamado']
        ) , FALSE);
        if (empty($verificar)) {
          $dao = Crud::getInstance('glpi');
          $idChamado = $dao->insert($arrayDados) ['id'];
          $Obj = new stdClass();
          $Obj->contract_id = $idChamado;
          $Obj->data_import_id = $data_import_id;
          $dao = Crud::getInstance('import_contracts');
          $dao->insert($Obj);
        }
      }
    }

    catch(Exception $e) {
      throw new Exception('Erro na fonte de dados da planilha');
    }

    $x = $x + 1;
  }
}

?>
