<?php
set_error_handler('exceptions_error_handler');

require_once __DIR__ . '/../crud.php';

require_once __DIR__ . '/function.php';

function import_harpia($datasource, $data_import_id, $import_file_path)
{
  $fp = fopen($import_file_path, "r");
  if (!$fp) {
    throw new Exception('Arquivo de importação não encontrado');
  }

  $header = Crud::getInstance()->getSQLGeneric("select header_lines from datasources where id = {$datasource}") [0]->header_lines;
  $attribute = Crud::getInstance()->getSQLGeneric("select * from contract_attribute_column_numbers where datasource_id = {$datasource} and column_number > 0");
  $arrayAtt = array();
  foreach($attribute as $key => $value) {
    $arrayAtt[$value->attribute_name] = $value->column_number - 1;
  }

  $x = 0;
  while (($linhaQuebrada = fgetcsv($fp, 0, ";")) !== FALSE) {
    try {
      if ($x > $header - 1) {
        foreach($arrayAtt as $key => $value) {
          if ($key == "banco") {
            $arrayDados['devname_banco'] = TraducaoBanco($linhaQuebrada[$value]);
          }

          if ($key == 'valor') {
            $linhaQuebrada[$value] = moeda($linhaQuebrada[$value]);
          }

          if ($key == 'ultima_data') {
            if (!empty($linhaQuebrada[$value])) {
              $linhaQuebrada[$value] = formatDate($linhaQuebrada[$value]);
            }
          }

          $arrayDados[$key] = $linhaQuebrada[$value];
        }

         if ($arrayDados['number'] != '') {

          $sql_harpia = "select id from harpia where number = {$arrayDados['numero']}";
          $verificar = Crud::getInstance()->getSQLGeneric($sql_harpia, array() , FALSE);
          if (empty($verificar)) {
            $id_contrato = Crud::getInstance('harpia')->insert($arrayDados) ['id'];
            inserir_tabela_relacao($id_contrato, $data_import_id);
          }
        }
        else {
          $id_contrato = Crud::getInstance('harpia')->insert($arrayDados) ['id'];
          inserir_tabela_relacao($id_contrato, $data_import_id);
        }
      }

         }catch(Exception $e) {
      throw new Exception('Erro na fonte de dados da planilha');
    }

    $x = $x + 1;
  }
}

?>
