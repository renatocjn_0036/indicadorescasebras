<?php
set_error_handler('exceptions_error_handler');

require_once __DIR__ . '/../crud.php';

require_once 'function.php';

function importar_monitoramento_generico($datasource, $data_import_id, $import_file_path)
{
  $fp = fopen($import_file_path, "r");
  if (!$fp) {
    throw new Exception('Arquivo de importação não encontrado');
  }

  $bank = Crud::getInstance()->getSQLGeneric("select devname from datasources where id = {$datasource}") [0]->devname;
  $attribute = Crud::getInstance()->getSQLGeneric("select * from contract_attribute_column_numbers where datasource_id = {$datasource} and column_number > 0");
  $arrayAtt = array();
  foreach($attribute as $key => $value) {
    $arrayAtt[$value->attribute_name] = $value->column_number - 1;
  }

  $header = Crud::getInstance()->getSQLGeneric("select header_lines from datasources where id = {$datasource}") [0]->header_lines;
  $x = 0;
  while (($linhaQuebrada = fgetcsv($fp, 1000, ";")) !== FALSE) {
    try {
      if ($x > $header - 1) {
        foreach($arrayAtt as $key => $value) {
          if ($key == 'value') {
            $linhaQuebrada[$value] = moeda($linhaQuebrada[$value]);
          }

          if ($key == 'date') {
            $linhaQuebrada[$value] = formatDate($linhaQuebrada[$value]);
          }

          if ($key == 'state_itau') {
            $linhaQuebrada[$value] = trocar_acentos($linhaQuebrada[$value]);
          }

          if ($key == 'state') {
            if (empty($linhaQuebrada[$value])) {
              $state = 0;
            }
            else {
              $state = 1;
            }
          }

          $arrayDados['bank'] = $bank;
          $arrayDados[$key] = $linhaQuebrada[$value];
        }

        $verificar = Crud::getInstance()->getSQLGeneric("select * from monitoramento where number = ?", array(
          $arrayDados['number']
        ) , FALSE);
        if (!empty($verificar)) {
          $DadosAtt = new StdClass();
          if ($arrayDados['state_itau']) {
            $DadosAtt->state_itau = $arrayDados['state_itau'];
          }

          $DadosAtt->state = $arrayDados['state'];
          $DadosAtt->date = $arrayDados['date'];
          $Condicao['id'] = $verificar->id;
          $dao = Crud::getInstance('monitoramento')->update($DadosAtt, $Condicao);

          // inserir_tabela_relacao($verificar->id, $data_import_id);

        }
        else {

          // CONTRATO NOVO

          $id_contrato = Crud::getInstance('monitoramento')->insert($arrayDados) ['id'];

          // inserir_tabela_relacao($id_contrato, $data_import_id);

        }

        // $dao = Crud::getInstance('monitoramento')->insert($arrayDados);

      }
    }

    catch(Exception $e) {
      throw new Exception('Erro na fonte de dados da planilha');
    }

    $x = $x + 1;
  }
}

?>
