<?php
error_reporting(1);
set_error_handler('exceptions_error_handler');

require_once __DIR__ . '/../crud.php';

require_once 'function.php';

function importar_pendencia($datasource, $data_import_id, $import_file_path)
{
  $fp = fopen($import_file_path, "r");
  if (!$fp) {
    throw new Exception('Arquivo de importação não encontrado');
  }

  $sql = "SELECT contrato.id,contrato.numero FROM contratos contrato join import_contratcs import on contrato.id = import.contract_id
          join data_import data on data.id = data_import_id where data.datasource_id = {$datasource} order by desc data.date_import limit 1";

  $ultima_importacao = Crud::getInstance()->getSQLGeneric($sql);

  $attribute = Crud::getInstance()->getSQLGeneric("select * from contract_attribute_column_numbers where datasource_id = {$datasource} and column_number > 0");
  $arrayAtt = array();

  foreach($attribute as $key => $value) {
    $arrayAtt[$value->attribute_name] = $value->column_number - 1;
  }

  $header = Crud::getInstance()->getSQLGeneric("select header_lines from datasources where id = {$datasource}") [0]->header_lines;
  $nova_importacao = array();
  $x = 0;
  while (($linhaQuebrada = fgetcsv($fp, 1000, ";")) !== FALSE) {
    try {
      if ($x > $header - 1) {
        foreach($arrayAtt as $key => $value) {
          if ($key == 'data_credito') {
              $linhaQuebrada[$value] = formatDate($linhaQuebrada[$value]);
          }

          $arrayDados[$key] = trocar_acentos($linhaQuebrada[$value]);

          $ob->numero = $arrayDados['numero'];
          $ob->desc_pendencia = $arrayDados['desc_pendencia'];
          unset($arrayDados['desc_pendencia']);
          $nova_importacao[] = $ob;
        }
          if(!empty($arrayDados['numero'])){
            $dao = Crud::getInstance('contratos')->update($arrayDados, array('numero' => $arrayDados['numero']));
            if($dao['retorno']){
              inserir_tabela_relacao($dao['id'], $data_import_id);
            }
          }
      }
    }
    catch(Exception $e) {
      echo $e->getMessage();
      throw new Exception('Erro na fonte de dados da planilha');
    }

    $x = $x + 1;
  }

  $nova_importacao = array_enkeyize($nova_importacao, 'numero');
  $ultima_importacao = array_enkeyize($ultima_importacao, 'numero');

  // A NÃO CONTEM EM B
  $result = array_diff_key($nova_importacao, $ultima_importacao);
  $contratos_sem_atuacao = array_dekeyize($result, 'numero');
  foreach ($contratos_sem_atuacao as $key => $value) {
    $ob = new stdClass();
    $ob->contrato_id = Crud::getInstance()->getSQLGeneric("select id from contratos where numero = {$value->numero}")[0]->id;
    $ob->desc_pendencia = $value->desc_pendencia;
    $ob->pendencia_id = Crud::getInstance()->getSQLGeneric("select id from situacao where tipo = 1")[0]->id;
    $ob->update_at = date('Y-m-d h:i:s');
    Crud::getInstance('log_pendencia')->insert($ob);
  }

  // B NÃO CONTEM EM A
  $result = array_diff_key($ultima_importacao,$nova_importacao);
  $contratos_regularizados = array_dekeyize($result, 'numero');
  foreach ($contratos_regularizados as $key => $value) {
    $ob = new stdClass();
    $ob->contrato_id = Crud::getInstance()->getSQLGeneric("select id from contratos where numero = {$value->numero}")[0]->id;
    // $ob->desc_pendencia = $value->desc_pendencia;
    $ob->pendencia_id = Crud::getInstance()->getSQLGeneric("select id from situacao where tipo = 5")[0]->id;
    $ob->update_at = date('Y-m-d h:i:s');
    Crud::getInstance('log_pendencia')->insert($ob);
  }

  // A CONTEM EM B
  $result = !array_diff_key($ultima_importacao,$nova_importacao);
  $contratos_regularizados = array_dekeyize($result, 'numero');
  foreach ($contratos_regularizados as $key => $value) {
    $ob = new stdClass();
    $ob->contrato_id = Crud::getInstance()->getSQLGeneric("select id from contratos where numero = {$value->numero}")[0]->id;
    // $ob->desc_pendencia = $value->desc_pendencia;
    $ob->pendencia_id = Crud::getInstance()->getSQLGeneric("select id from situacao where tipo = 5")[0]->id;
    $ob->update_at = date('Y-m-d h:i:s');
    Crud::getInstance('log_pendencia')->insert($ob);
  }
}

function array_enkeyize($array, $iten) {
  foreach ($array as $key => $value) {
    foreach ($value as $v_key => $v_value) {
        if ($v_key === $iten){
            $keized[$v_value] = $array[$key];
            unset($keized[$v_value][$iten]);
        }
    }
  }

  return $keized;
}


function array_dekeyize($array, $iten) {
    $i = 0;
  foreach ($array as $key => $value) {
    $dekeized[$i] = $array[$key];
    $dekeized[$i++][$iten] = $key;
  }

  return $dekeized;
}

?>
