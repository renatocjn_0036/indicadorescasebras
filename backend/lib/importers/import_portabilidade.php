<?php
require_once __DIR__ . '/../crud.php';

require_once __DIR__ . '/function.php';

set_error_handler('exceptions_error_handler');

function import_portabilidade($datasource, $data_import_id, $import_file_path)
{
  $fp = fopen($import_file_path, "r");
  if (!$fp) {
    throw new Exception('Arquivo de importação não encontrado');
  }

  $header = Crud::getInstance()->getSQLGeneric("select header_lines from datasources where id = {$datasource}") [0]->header_lines;
  $attribute = Crud::getInstance()->getSQLGeneric("select * from contract_attribute_column_numbers where datasource_id = {$datasource} and column_number > 0");
  $arrayAtt = array();
  foreach($attribute as $key => $value) {
    $arrayAtt[$value->attribute_name] = $value->column_number - 1;
  }

  $x = 0;
  while (($linhaQuebrada = fgetcsv($fp, 0, ";")) !== FALSE) {
    try {
      if ($x > $header - 1) {
        foreach($arrayAtt as $key => $value) {

          if ($key == 'valor_saldo') {
            $linhaQuebrada[$value] = moeda($linhaQuebrada[$value]);
          }

          if ($key == 'cpf') {

            if(strlen($linhaQuebrada[$value]) == 10){
              $linhaQuebrada[$value] =  "0". $linhaQuebrada[$value];
            }

            $linhaQuebrada[$value] = Mask("###.###.###-##",$linhaQuebrada[$value]);
          }

          $arrayDados[$key] = $linhaQuebrada[$value];
        }

        insertOrUpdate($arrayDados);

      }
    }

    catch(Exception $e) {
      echo $e->getMessage();
      throw new Exception('Erro na fonte de dados da planilha');
    }

    $x = $x + 1;
  }
}

function insertOrUpdate(array $rows)
{

  $campos = campos($rows);
  $valores = valores($rows);
  $update = update($rows);

  $sql = "INSERT INTO portabilidade ({$campos}) VALUES ({$valores}) ON DUPLICATE KEY UPDATE {$update}";
  Crud::getInstance()->genericInsert($sql);

}

function campos($rows){

  $campos = implode(',', array_map(
  function ($value)
  {
    return "$value";
  }

  , array_keys($rows)));

  return $campos;
}

function valores($rows){
  $valores = implode(",", array_map(
  function ($value)
  {
    return "'$value'";
  }

  , $rows));

  return $valores;
}

function update($rows){

  $update =  array_filter(array_map(
  function ($value)
  {
    if($value != 'nome_agente'){
      return "$value = VALUES($value)";
    }
  }

  , array_keys($rows)));

  $update = implode(',',$update);

  return $update;
}

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}


?>
