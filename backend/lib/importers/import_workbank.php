<?php
require_once __DIR__ . '/../crud.php';

require_once __DIR__ . '/function.php';

set_error_handler('exceptions_error_handler');

function import_workbank($datasource, $data_import_id, $import_file_path)
{
  $fp = fopen($import_file_path, "r");
  if (!$fp) {
    throw new Exception('Arquivo de importação não encontrado');
  }

  $header = Crud::getInstance()->getSQLGeneric("select header_lines from datasources where id = {$datasource}") [0]->header_lines;
  $attribute = Crud::getInstance()->getSQLGeneric("select * from contract_attribute_column_numbers where datasource_id = {$datasource} and column_number > 0");
  $arrayAtt = array();
  foreach($attribute as $key => $value) {
    $arrayAtt[$value->attribute_name] = $value->column_number - 1;
  }

  $x = 0;
  while (($linhaQuebrada = fgetcsv($fp, 0, ";")) !== FALSE) {
    try {
      if ($x > $header - 1) {
        foreach($arrayAtt as $key => $value) {
          if ($key == "banco") {
            $arrayDados['devname_banco'] = TraducaoBanco($linhaQuebrada[$value]);
          }

          if ($key == 'valor') {
            $linhaQuebrada[$value] = moeda($linhaQuebrada[$value]);
          }

          if ($key == 'data_operacao') {
            $linhaQuebrada[$value] = formatDate($linhaQuebrada[$value]);
          }

          if ($key == 'data_credito' && !empty($linhaQuebrada[$value])) {
            $linhaQuebrada[$value] = formatDate($linhaQuebrada[$value]);
          }
          else
          if ($key == 'data_credito' && empty($linhaQuebrada[$value])) {
            $linhaQuebrada[$value] = null;
          }

          if ($key == 'convenio') {
            $arrayDados['devname_convenio'] = traducaoPartnership($linhaQuebrada[$value]);
            $linhaQuebrada[$value] = trocar_acentos($linhaQuebrada[$value]);
          }

          if ($key == 'operacao') {
            $linhaQuebrada[$value] = trocar_acentos($linhaQuebrada[$value]);
          }

          if ($key == 'unidade') {
            $linhaQuebrada[$value] = trocar_acentos($linhaQuebrada[$value]);
          }

          $arrayDados[$key] = $linhaQuebrada[$value];
        }

        $update_contrato_work = '';
        $update_contrato_harpia = '';
        if (!empty($arrayDados['numero'])) {
          $retorno = verificar("numero", $arrayDados['numero']);
          if (!empty($retorno)) {
            $DadosAtt = new StdClass();
            $DadosAtt->data_credito = $arrayDados['data_credito'];
            $Condicao['id'] = $retorno->id;
            $dao = Crud::getInstance('contratos')->update($DadosAtt, $Condicao);
            inserir_tabela_relacao($retorno->id, $data_import_id);
          }
          else {
            $id_contrato = Crud::getInstance('contratos')->insert($arrayDados) ['id'];
            inserir_tabela_relacao($id_contrato, $data_import_id);
          }
        }
        else {
          $retorno = verificar("cpf", $arrayDados['cpf']);
          if (empty($retorno)) {
            $arrayDados['numero'] = null;
            $id_contrato = Crud::getInstance('contratos')->insert($arrayDados) ['id'];
            inserir_tabela_relacao($id_contrato, $data_import_id);
          }
        }
      }
    }

    catch(Exception $e) {
      echo $e->getMessage();
      throw new Exception('Erro na fonte de dados da planilha');
    }

    $x = $x + 1;
  }
}

function verificar($key, $condicao)
{
  $sql = "select id, data_operacao from contratos where {$key} = {$condicao} ";
  return Crud::getInstance()->getSQLGeneric($sql, array() , FALSE);
}

?>