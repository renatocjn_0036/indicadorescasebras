<?php
require_once __DIR__ . '/crud.php';

$username = $_POST['username'];
$password = $_POST['password'];
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($username) && !empty($password)) {

  $usuario = !empty(buscar_usuario($username)) ? buscar_usuario($username) : header("Location: /indicadorescasebras/login.php?error=1");

  if (!password_verify($password, $usuario->password)) {
    header("Location: /indicadorescasebras/login.php?error=1");
    exit;
  }

  if($usuario->situacao == 1){

    header("Location: /indicadorescasebras/login.php?error=2");
    exit;
  }

  gravar_acesso($usuario);
  unset($usuario->password);

  $dados_usuario = [
    'filiais' => buscar_filial($usuario),
    'permissoes' => buscar_permissao($usuario),
    'dados' => $usuario
  ];
  $_SESSION['usuario'] = $dados_usuario;
}
else {
  header("Location: /indicadorescasebras/login.php?error=1");
  exit;
}

function buscar_usuario(string $username)
{
  $sql = 'SELECT * from users where username = ?';
  $usuario = Crud::getInstance()->getSQLGeneric($sql, array(
    $username
  ) , FALSE);
  return $usuario;
}

function gravar_acesso($usuario)
{
  $ob = new stdClass();
  $usuario->id;
  $ob->last_access = date('Y-m-d H:i:s');
  Crud::getInstance('users')->update($ob, array('id'=>$usuario->id));
}

function buscar_filial($usuario)
{
  $array_filial = [];
  if($usuario->id_perfil !=1 ){

    $sql = 'SELECT * from filial_users where user_id = ?';
    $filial = Crud::getInstance()->getSQLGeneric($sql, array($usuario->id), TRUE);
    if(!empty($filial)){
      foreach ($filial as $key => $value) {
        $array_filial[] = "'$value->filial'";
      }
    }

  }else if($usuario->id_perfil == 1){
    $array_filial = '';
  }

  return $array_filial;
}

function buscar_permissao($usuario)
{
    $array = [];
    $sql = 'SELECT * from permissao where user_id = ?';
    $retorno = Crud::getInstance()->getSQLGeneric($sql, array($usuario->id), TRUE);

    if(!empty($retorno)){
      foreach ($retorno as $key => $value) {
        $array[] = $value->rota;
      }
    }
    return $array;
}

?>
<html>
    <head>
        <meta http-equiv="refresh" content="1; URL=/indicadorescasebras/#/Home">
    </head>
    <body>

		Aguarde ...
    </body>
</html>
