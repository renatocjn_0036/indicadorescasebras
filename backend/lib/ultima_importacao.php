<?php
require_once __DIR__ . '/crud.php';
$data = json_decode(file_get_contents("php://input"));

if(isset($data->id)){
  $sql = 'SELECT date_format(max(date_import), "%d/%m/%Y %H:%i:%s") as date FROM data_import di join datasources d on di.datasource_id = d.id where d.type = ? and failed = ?';

  $importacao = Crud::getInstance()->getSQLGeneric($sql, array($data->id, 0) , FALSE);
  echo json_encode($importacao);
}

?>
