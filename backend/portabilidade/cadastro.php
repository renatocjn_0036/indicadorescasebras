<?php
// error_reporting(0);
require_once '../lib/crud.php';
require_once 'enviar-email.php';
$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));

if(!empty($data->lista_email)){
  $lista_email = $data->lista_email;
}

if (!empty($data->dados)) {
    $dados = $data->dados;

    if(!empty($dados->id)){
      // print_r($lista_email);
      if(!empty($lista_email)){
        inserir_email($dados->id, $lista_email);
      }
      atualizar($dados,$lista_email);
      exit;
    }

    $sql_verificar = "SELECT * FROM portabilidade where numero_contrato = ?";
    $retorno_contratos = Crud::getInstance()->getSQLGeneric($sql_verificar, array($dados->numero_contrato), false);

  if(!empty($retorno_contratos)){
      $json = [
        'retorno'=>false,
        'msg'=>'Número de Contrato Já Cadastrado'
      ];
  }else{
    $dados->data_cadastro = date('Y-m-d H:i:s');
    $dados->situacao = 'Nao Averbado';
    $dados->data_saldo = date("Y-m-d",strtotime(str_replace('/','-',$dados->data_saldo)));

    $retorno = Crud::getInstance('portabilidade')->insert($dados);
    $json = [
      'retorno'=>true,
      'msg'=>'Portabilidade Cadastrada Com Sucesso'
    ];

    inserir_email($retorno['id'], $lista_email);
    if(isset($dados->enviar_email)){
      enviar_email($dados, $lista_email);
    }
  }
    echo json_encode($json);
    exit;
}

function inserir_email($id, $lista_email){

  Crud::getInstance('email_portabilidade')->delete(array('id_portabilidade' => $id));

  foreach ($lista_email as $key => $value) {
    $ob = new stdClass();
    $ob->email = $value->email;
    $ob->id_portabilidade = $id;
    Crud::getInstance('email_portabilidade')->insert($ob);

  }
}

function atualizar($dados,$lista_email){

  if(isset($dados->enviar_email)){
    enviar_email($dados, $lista_email);
  }

  $id = $dados->id;
  unset($dados->id);
  unset($dados->data_cadastro);
  unset($dados->dias);
  unset($dados->situacao);
  unset($dados->total_recuperado);
  unset($dados->enviar_email);
  if(!empty($dados->data_saldo)){
    $dados->data_saldo = date("Y-m-d",strtotime(str_replace('/','-',$dados->data_saldo)));
  }
  if(!empty($dados->data_operacao)){
    $dados->data_operacao = date("Y-m-d",strtotime(str_replace('/','-',$dados->data_operacao)));
  }
  Crud::getInstance('portabilidade')->update($dados, array('id' => $id));


  $json = [
    'retorno'=>true,
    'msg'=>'Portabilidade Atualizada Com Sucesso'
  ];
  echo json_encode($json);

}



?>
