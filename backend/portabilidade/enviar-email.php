<?php
ini_set('smtp_port', '587');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;;

require_once '../../vendor/autoload.php';

function enviar_email($dados, $emails)
{
  $mail = new PHPMailer(true);

  $assunto = 'PORTABILIDADE NÃO AVERBADA Nº ' . $dados->numero_contrato;
    $array_dados = array(
      $dados->numero_contrato,
      $dados->cliente,
      $dados->cpf,
      date("d/m/Y",strtotime(str_replace('-','/',$dados->data_operacao))),
      strtoupper($dados->banco),
      'R$'.number_format($dados->valor_prestacao, 2, ',', '.'),
      'R$'.number_format($dados->valor_saldo, 2, ',', '.'),
      $dados->unidade,
      $dados->nome_agente,
      'http://www.ltpromotora.com.br/imagem/portabilidade-cabecalho.jpg',
      'http://www.ltpromotora.com.br/imagem/portabilidade-rodape.jpg',
      nl2br($dados->observacao)
    );


    $template = file_get_contents("email.tpl");
    $mail->Body = str_replace(array(
      "%%numero_contrato%%",
      "%%cliente%%",
      "%%cpf%%",
      "%%data_operacao%%",
      "%%banco%%",
      "%%valor_prestacao%%",
      "%%valor_saldo%%",
      "%%unidade%%",
      "%%nome_agente%%",
      "%%img_cabecalho%%",
      "%%img_rodape%%",
      "%%observacao%%",
    ) , $array_dados, $template);

    $mail->SMTPDebug = 0;
    $mail->isSMTP();
    $mail->Host = '10.0.0.5';
    $mail->SMTPAuth = true;
    $mail->Username = 'no-reply@casebras.com.br';
    $mail->Password = 'case123';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->SMTPOptions = array(
      'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
      )
    );
    $mail->setFrom('retencaoport@grupocasebras.com.br', 'PORTABILIDADE MATRIZ');
    $mail->ClearAllRecipients();
    foreach($emails as $key => $value) {
      $mail->AddCC($value->email);
    }
    $mail->addReplyTo('retencaoport@grupocasebras.com.br', 'PORTABILIDADE MATRIZ');
    $mail->isHTML(true);
    $mail->CharSet = 'utf-8';
    $mail->Subject = $assunto;
    $enviado = $mail->Send();
    $mail->ClearAttachments();

  if (!$enviado) {
    return $mail->ErrorInfo;
  }
  else {
    return true;
  }
}


?>
