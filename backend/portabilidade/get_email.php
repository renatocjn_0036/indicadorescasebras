<?php
require_once '../lib/crud.php';
$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));

if(!empty($data->id->id_portabilidade)){
  $qry = 'select * from email_portabilidade where id_portabilidade = ?';
  $array_email = Crud::getInstance()->getSQLGeneric($qry, array($data->id->id_portabilidade), TRUE);
  echo json_encode($array_email);
}
?>
