<?php
require_once '../lib/crud.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));
$arrayParams = [];
$where = [];

$registrosPorPagina = $data->num_registros;
$paginaAtual = $data->pagina;

if (!empty($data->filtros)) {
  $filtro = $data->filtros;

  if (isset($filtro->data_inicial) && isset($filtro->data_final)) {
    $data_inicial = substr($filtro->data_inicial, 0, 10);
    $data_final = substr($filtro->data_final, 0, 10);
    $where[] = "data_saldo between '{$data_inicial}' and '{$data_final}'";
  }

  unset($filtro->data_inicial);
  unset($filtro->data_final);
  $arrayParams = array();

  foreach($filtro as $key => $value) {
    if($value == 'NULL'){
      $where[] = "{$key} is NULL";
    }else{
      $where[] = "{$key} = ?";
      $arrayParams[] = $value;
    }
  }
}

$sql = 'SELECT p.*,DATEDIFF(now(), p.data_saldo) as dias, sum(vr.valor_recuperado) as total_recuperado
FROM portabilidade p left join valor_recuperado vr on p.id = vr.portabilidade_id ';

if (!empty($where)) {
  $sql.= ' WHERE p.' . implode(' AND ', $where);
}

$offset = (intval($paginaAtual)-1) * $registrosPorPagina;

$sql .=  " GROUP BY numero_contrato ORDER BY dias DESC";

$count = count($dao->getSQLGeneric($sql, $arrayParams, TRUE));

// Adicionando a paginação
$sql .= " LIMIT {$registrosPorPagina} OFFSET {$offset}";

$registros = $dao->getSQLGeneric($sql, $arrayParams, TRUE);


echo json_encode([
  'registros'=> $registros,
  'paginacao' => [
    'registros_por_pagina' => $registrosPorPagina,
     'pagina_atual'         => $paginaAtual,
     'total_registros'      => $count,
  ]
])
?>
