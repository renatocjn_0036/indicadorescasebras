<?php
require_once '../lib/crud.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));

$sql = 'SELECT * FROM valor_recuperado where portabilidade_id = ?';

$valores = $dao->getSQLGeneric($sql, array($data->id), TRUE);
echo json_encode($valores);
?>
