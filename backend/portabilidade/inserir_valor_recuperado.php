<?php
require_once '../lib/crud.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));

$valores = $data->valores;
$id = $data->id;


Crud::getInstance('valor_recuperado')->delete(array('portabilidade_id' => $id));

if(!empty($valores)){
  foreach ($valores as $key => $value) {
    $ob = new stdClass();
    $ob->valor_recuperado = $value->valor_recuperado;
    $ob->data = date("Y-m-d",strtotime(str_replace('/','-',$value->data)));
    $ob->portabilidade_id = $id;
    Crud::getInstance('valor_recuperado')->insert($ob);
  }
}

$total = array_reduce($valores, function($price, $produto) {
  $price += $produto->valor_recuperado;
  return $price;
});

echo json_encode(array(
  'retorno' => true,
  'total' => $total
));
?>
