<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title></title>

<style type="text/css">@media yahoo{.kmHide,.kmMobileOnly,.kmMobileHeaderStackDesktopNone,.kmMobileWrapHeaderDesktopNone{display:none;font-size:0 !important;line-height:0 !important;mso-hide:all !important;max-height:0 !important;max-width:0 !important;width:0 !important}}@media only screen and (max-width:480px){body,table,td,p,a,li,blockquote{-webkit-text-size-adjust:none !important}body{width:100% !important;min-width:100% !important}#bodyCell{padding:10px !important}table.kmMobileHide{display:none !important}table.kmDesktopOnly,td.kmDesktopOnly,th.kmDesktopOnly,tr.kmDesktopOnly,td.kmDesktopWrapHeaderMobileNone{display:none !important}table.kmMobileOnly{display:table !important}tr.kmMobileOnly{display:table-row !important}td.kmMobileOnly,td.kmDesktopWrapHeader,th.kmMobileOnly{display:table-cell !important}tr.kmMobileNoAlign,table.kmMobileNoAlign{float:none !important;text-align:initial !important;vertical-align:middle !important;table-layout:fixed !important}tr.kmMobileCenterAlign{float:none !important;text-align:center !important;vertical-align:middle !important;table-layout:fixed !important}td.kmButtonCollection{padding-left:9px !important;padding-right:9px !important;padding-top:9px !important;padding-bottom:9px !important}td.kmMobileHeaderStackDesktopNone,img.kmMobileHeaderStackDesktopNone,td.kmMobileHeaderStack{display:block !important;margin-left:auto !important;margin-right:auto !important;padding-bottom:9px !important;padding-right:0 !important;padding-left:0 !important}td.kmMobileWrapHeader,td.kmMobileWrapHeaderDesktopNone{display:inline-block !important}td.kmMobileHeaderSpacing{padding-right:10px !important}td.kmMobileHeaderNoSpacing{padding-right:0 !important}table.kmDesktopAutoWidth{width:inherit !important}table.kmMobileAutoWidth{width:100% !important}table.kmTextContentContainer{width:100% !important}table.kmBoxedTextContentContainer{width:100% !important}td.kmImageContent{padding-left:0 !important;padding-right:0 !important}img.kmImage{width:100% !important}td.kmMobileStretch{padding-left:0 !important;padding-right:0 !important}table.kmSplitContentLeftContentContainer,table.kmSplitContentRightContentContainer,table.kmColumnContainer,td.kmVerticalButtonBarContentOuter table.kmButtonBarContent,td.kmVerticalButtonCollectionContentOuter table.kmButtonCollectionContent,table.kmVerticalButton,table.kmVerticalButtonContent{width:100% !important}td.kmButtonCollectionInner{padding-left:9px !important;padding-right:9px !important;padding-top:9px !important;padding-bottom:9px !important}td.kmVerticalButtonIconContent,td.kmVerticalButtonTextContent,td.kmVerticalButtonContentOuter{padding-left:0 !important;padding-right:0 !important;padding-bottom:9px !important}table.kmSplitContentLeftContentContainer td.kmTextContent,table.kmSplitContentRightContentContainer td.kmTextContent,table.kmColumnContainer td.kmTextContent,table.kmSplitContentLeftContentContainer td.kmImageContent,table.kmSplitContentRightContentContainer td.kmImageContent{padding-top:9px !important}td.rowContainer.kmFloatLeft,td.rowContainer.kmFloatLeft,td.rowContainer.kmFloatLeft.firstColumn,td.rowContainer.kmFloatLeft.firstColumn,td.rowContainer.kmFloatLeft.lastColumn,td.rowContainer.kmFloatLeft.lastColumn{float:left;clear:both;width:100% !important}table.templateContainer,table.templateContainer.brandingContainer,div.templateContainer,div.templateContainer.brandingContainer,table.templateRow{max-width:550px !important;width:100% !important}h1{font-size:40px !important;line-height:130% !important}h2{font-size:32px !important;line-height:130% !important}h3{font-size:24px !important;line-height:130% !important}h4{font-size:18px !important;line-height:130% !important}td.kmTextContent{font-size:14px !important;line-height:130% !important}td.kmTextBlockInner td.kmTextContent{padding-right:18px !important;padding-left:18px !important}table.kmTableBlock.kmTableMobile td.kmTableBlockInner{padding-left:9px !important;padding-right:9px !important}table.kmTableBlock.kmTableMobile td.kmTableBlockInner .kmTextContent{font-size:14px !important;line-height:130% !important;padding-left:4px !important;padding-right:4px !important}}</style>
<!--[if mso]>
    <style>

      .templateContainer {
        border: 0px none #AAAAAA;
        background-color: #CCCBFF;

          border-radius: 0px;

      }
      #brandingContainer {
        background-color: transparent !important;
        border: 0;
      }


      .templateContainerInner {
        padding: 0px;
      }

    </style>
  <![endif]-->
</head>
<body style="margin:0;padding:0;background-color:#CCCBFF">
<center>
<table align="center" border="0" cellpadding="0" cellspacing="0" id="bodyTable" width="100%" data-upload-file-url="/ajax/email-editor/file/upload" data-upload-files-url="/ajax/email-editor/files/upload" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;padding:0;background-image:url(https://d3k81ch9hvuctc.cloudfront.net/company/FnnkRD/images/45d8cf13-66c2-4b83-acf6-129ed92e861d.jpeg);background-repeat:repeat;background-position:left top;height:100%;margin:0;width:100%">
<tbody>
<tr>
<td align="center" id="bodyCell" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;padding-top:50px;padding-left:20px;padding-bottom:20px;padding-right:20px;border-top:0;height:100%;margin:0;width:100%">
<!--[if !mso]><!-->
<div class="templateContainer" style="border:0 none #AAA;background-color:#CCCBFF;border-radius:0;display: table; width:550px">
<div class="templateContainerInner" style="padding:0">
<!--<![endif]-->
<!--[if mso]>
              <table border="0" cellpadding="0" cellspacing="0" class="templateContainer"  width="550" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
              <tbody>
                <tr>
                  <td class="templateContainerInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
            <![endif]-->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<tr>
<td align="center" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<table border="0" cellpadding="0" cellspacing="0" class="templateRow" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<tbody>
<tr>
<td class="rowContainer kmFloatLeft" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<table border="0" cellpadding="0" cellspacing="0" class="kmTextBlock" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<tbody class="kmTextBlockOuter">
<tr>
<td class="kmTextBlockInner" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#FFFFFF;">
<table align="left" border="0" cellpadding="0" cellspacing="0" class="kmTextContentContainer" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<tbody>
<tr>
<td class="kmTextContent" valign="top" style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;color:#222;font-family:"Helvetica Neue", Arial;font-size:14px;line-height:130%;letter-spacing:normal;text-align:left;padding-top:9px;padding-bottom:9px;background-color:#FFFFFF;padding-left:18px;padding-right:18px;'>
<span style="font-family:times new roman,times,serif;"></span>
<h4 style='color:#222;display:block;font-family:"Helvetica Neue", Arial;font-size:18px;font-style:normal;font-weight:italic;line-height:110%;letter-spacing:normal;margin:0;margin-bottom:9px;text-align:left;text-align: center;'> </h4>
<p style="margin:0;padding-bottom:1em"><span style="font-size:18px;"><strong>Olá Luiz,</strong></span></p>
<p style="margin:0;padding-bottom:1em"> </p>
<p style="margin:0;padding-bottom:1em">Seja bem-vindo(a) a CRED SOLUÇÃO desde já queremos agradecer pela parceria</p>
<p style="margin:0;padding-bottom:1em">Estamos a sua disposição para lhe ajudar a crescer juntamente conosco.</p>
<p style="margin:0;padding-bottom:1em">Segue abaixo informações de acesso ao nosso sistema.</p>
<p style="margin:0;padding-bottom:1em"> </p>
<p style="margin:0;padding-bottom:1em">Cod parceiro: PA-12</p>
<p style="margin:0;padding-bottom:1em">Senha: 010677</p>
<p style="margin:0;padding-bottom:1em">Senha relatório/financeira: cred123</p>
<p style="margin:0;padding-bottom:1em"> </p>
<p style="margin:0;padding-bottom:0">Conte Conosco para o que preisar. BOAS VENDAS</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="kmTextBlock" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<tbody class="kmTextBlockOuter">
<tr>
<td class="kmTextBlockInner" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
<table align="left" border="0" cellpadding="0" cellspacing="0" class="kmTextContentContainer" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<tbody>
<tr>
<td class="kmTextContent" valign="top" style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;color:#222;font-family:"Helvetica Neue", Arial;font-size:14px;line-height:130%;letter-spacing:normal;text-align:left;color:#222222;padding-bottom:9px;padding-right:18px;padding-left:18px;padding-top:9px;background-color:#FFFFFF;'>
<p style="margin:0;padding-bottom:1em;text-align: center;"> </p>
<p style="margin:0;padding-bottom:1em;text-align: center;"> </p>
<p style="margin:0;padding-bottom:0;text-align: center;"><strong>Suporte@credsolucao.com.br | www.credsolucao.com.br</strong></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</table>
<!--[if !mso]><!-->
</div>
</div>
<!--<![endif]-->
<!--[if mso]>
                    </td>
                  </tr>
                </tbody>
              </table>
            <![endif]-->
<!--[if !mso]><!-->
<div class="templateContainer brandingContainer" style="border:0;background-color:transparent;border-radius:0;display: table; width:550px">
<div class="templateContainerInner" style="padding:0">
<!--<![endif]-->
<!--[if mso]>
              <table border="0" cellpadding="0" cellspacing="0" class="templateContainer" id="brandingContainer" width="550" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
              <tbody>
                <tr>
                  <td class="templateContainerInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;">
            <![endif]-->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<tr>
<td align="center" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<table border="0" cellpadding="0" cellspacing="0" class="templateRow" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<tbody>
<tr>
<td class="rowContainer kmFloatLeft" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0">
<table border="0" cellpadding="0" cellspacing="0" class="kmImageBlock" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;min-width:100%">
<tbody class="kmImageBlockOuter">
<tr>
<td class="kmImageBlockInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;padding:9px;padding-top:25px;padding-bottom:25px;" valign="top">
<table align="left" border="0" cellpadding="0" cellspacing="0" class="kmImageContentContainer" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;min-width:100%">
<tbody>
<tr>
<td class="kmImageContent" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;padding:0;font-size:0;padding-top:0px;padding-bottom:0;padding-left:9px;padding-right:9px;text-align: center;">
<a href="https://www.klaviyo.com/?utm_medium=freebie&amp;utm_source=brand&amp;utm_term=FnnkRD" target="_self" style="word-wrap:break-word;color:#15C;font-weight:normal;text-decoration:underline">
<img align="center" alt="Powered by Klaviyo" class="kmImage" src="https://d3k81ch9hvuctc.cloudfront.net/assets/email/branding/klaviyo-branding-option-0.png" width="122" style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;padding-bottom:0;display:inline;vertical-align:top;font-size:12px;max-width:122px;" />
</a>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</table>
<!--[if !mso]><!-->
</div>
</div>
<!--<![endif]-->
<!--[if mso]>
                    </td>
                  </tr>
                </tbody>
              </table>
            <![endif]-->
</td>
</tr>
</tbody>
</table>
</center>
</body>
</html>
