<?php 
require_once '../lib/crud.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));

$unidade = $data->unidade;

$sql = 'SELECT * FROM unidades where nome = ? and uf = ?';

$existe = $dao->getSQLGeneric($sql, array($unidade->nome, $unidade->uf), TRUE);

if($existe) {
    $json = array(
            'status' => 'error',
            'msg' => 'Unidade já existe'
        );
    echo json_encode($json);
    exit;
}

$inserir = Crud::getInstance('unidades')->insert($unidade);

if($inserir) {

    $json = array(
        'status' => 'success',
        'msg' => 'Operação realizada com sucesso'
    );
    echo json_encode($json);
    exit;
}
?>