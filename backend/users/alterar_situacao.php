<?php
require_once '../lib/crud.php';


$data = json_decode(file_get_contents("php://input"));
$usuario = $data->usuario;

$ob = new stdClass();
switch ($usuario->situacao) {
  case 1:
    $ob->situacao = 0;
    break;
  case 0:
    $ob->situacao = 1;
    break;
  default:
    $ob = $usuario->situacao;
    break;
}

$retorno = Crud::getInstance('users')->update($ob, array('id' => $usuario->id));

echo json_encode($retorno_array = [
  'retorno'=> $retorno['retorno'],
  'id'=>null
]);

?>
