<?php

require_once 'function.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));
$user = $data->user;
$permissao = $data->permissao;
if(!empty($data->filial)){  
  $filial = $data->filial;
}

if (!empty($user)) {

  if(!filter_var($user->email, FILTER_VALIDATE_EMAIL)){
    $retorno = [
      'retorno'=>false,
      'msg'=>'E-mail Inválido'
    ];
    echo json_encode($retorno);
    exit;
  }

  if(!empty($permissao)){
    inserir_permissao($user->id, $permissao);
  }

  if(!empty($filial)){
    inserir_filial($user->id, $filial);
  }

  $retorno = atualizar_dados($user);

  echo json_encode($retorno);
}

function atualizar_dados($user)
{

  if (!empty($user->password)) {
    $user->password = hash_senha($user);
  }

  $id = $user->id;
  unset($user->id);
  unset($user->ultimo_acesso);
  unset($user->perfil);
  unset($user->p2);

  $retorno = Crud::getInstance('users')->update($user, array('id' => $id))['retorno'];

  return $retorno_array = [
    'retorno'=> $retorno,
    'id'=>$id
  ];

}








?>
