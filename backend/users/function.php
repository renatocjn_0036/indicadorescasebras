<?php
require_once '../lib/crud.php';

function inserir_filial(string $id, array $filial)
{

  $sql_verificar = "SELECT * FROM filial_users where user_id = {$id}";
  $retorno_filial = Crud::getInstance()->getSQLGeneric($sql_verificar);

  if(!empty($retorno_filial)){
    foreach ($retorno_filial as $key => $value) {
      Crud::getInstance('filial_users')->delete(array('id' => $value->id));
    }
  }

  if(!empty($filial)){

   foreach ($filial as $key => $value) {
     Crud::getInstance('filial_users')->insert(array(
       'filial' => $value,
       'user_id' => $id
     ));
   }
  }

}

function inserir_permissao(string $id, array $permissao)
{
  $sql_verificar = "SELECT * FROM permissao where user_id = {$id}";
  $retorno = Crud::getInstance()->getSQLGeneric($sql_verificar);

  if(!empty($retorno)){
    foreach ($retorno as $key => $value) {
      Crud::getInstance('permissao')->delete(array('id' => $value->id));
    }
  }

  if(!empty($permissao)){
   $permissao[] = '/Home';
     foreach ($permissao as $key => $value) {
       Crud::getInstance('permissao')->insert(array(
         'rota' => $value,
         'user_id' => $id
       ));
     }
   }

}

function hash_senha($user)
{
  if($user->password !== $user->p2){
    $retorno = [
      'retorno'=>false,
      'msg'=>'Senhas não Conferem'
    ];
    echo json_encode($retorno);
    exit;
  }
  return password_hash($user->password, PASSWORD_BCRYPT);
}
?>
