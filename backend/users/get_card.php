<?php
require_once '../lib/crud.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));

if(!empty($data->id)){

  $sql = 'SELECT c.id, c.title from cards c join card_user u on u.id_card = c.id where u.id_user = ?';
  $params = array($data->id);
  $datasources = $dao->getSQLGeneric($sql,$params);
  foreach ($datasources as $key => $value) {
    $array[] = $value->id;
  }
// print_r($array);
  echo json_encode($array);

}else{

  $params = array();
  $sql = "SELECT id, title FROM cards";
  $datasources = $dao->getSQLGeneric($sql,$params);
  echo json_encode($datasources);
}



?>
