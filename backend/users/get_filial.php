<?php
require_once '../lib/crud.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));

if(!empty($data->id)){

  $sql = 'SELECT f.id, f.filial from filiais f join filial_users u on u.id_filial = f.id where u.id_user = ?';
  $datasources = $dao->getSQLGeneric($sql,array($data->id));
  foreach ($datasources as $key => $value) {
    $array[] = $value->id;
  }
// print_r($array);
  echo json_encode($array);

}else{

  $params = array();
  $sql = "SELECT id, filial FROM filiais";
  $datasources = $dao->getSQLGeneric($sql,$params);
  // foreach ($datasources as $key => $value) {
  //   $array[] = $value->filial;
  // }
  echo json_encode($datasources);
}



?>
