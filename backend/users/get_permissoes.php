<?php
require_once '../lib/crud.php';

$sql = 'SELECT * FROM rotas';
$retorno = Crud::getInstance()->getSQLGeneric($sql, array() , TRUE);

foreach($retorno as $key => $value) {
  if (empty($value->pai_id)) {
    $obj_pai = new stdClass();
    $obj_pai->id = $value->id;
    $obj_pai->name = $value->nome;
    $obj_pai->rota = $value->rota;
    $obj_pai->checked = false;
    $obj_pai->children = [];
    $array[] = $obj_pai;
  }
  else {
    $obj_filho = new stdClass();
    $obj_filho->pai_id = $value->pai_id;
    $obj_filho->name = $value->nome;
    $obj_filho->rota = $value->rota;
    $obj_filho->checked = false;
    array_filho($obj_filho, $array);
  }
}

function array_filho($filho, $array)
{
  foreach($array as $key => $value) {
    if ($value->id == $filho->pai_id) {
      $value->children[] = $filho;
    }
  }
}

echo json_encode($array);
?>
