<?php
// error_reporting(0);
require_once '../lib/crud.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));

$sql = "SELECT u.id, u.name, u.username, u.email, u.id_perfil, p.perfil, date_format(u.last_access, '%d/%m/%Y %H:%i:%s') as ultimo_acesso,u.situacao FROM users u join perfil p on u.id_perfil = p.id";

if(!empty($data->id)){
  $sql .= " WHERE u.id = ?";
  $params = array($data->id);
  $fetch = FALSE;
  $permissoes = getPermissao($data->id);
  $filiais = getFilial($data->id);

}else{
  $params = array();
  $fetch = TRUE;
}

$array = [
  'dados' => Crud::getInstance()->getSQLGeneric($sql,$params,$fetch),
  'filiais' => !empty($filiais) ? $filiais : '',
  'permissoes' => !empty($permissoes) ? $permissoes : ''
];

echo json_encode($array);

function getPermissao($id){
  $permissao = [];
  $sql = 'SELECT * FROM permissao where user_id = ?';
  $retorno_permissao = Crud::getInstance()->getSQLGeneric($sql,array($id),TRUE);
  foreach ($retorno_permissao as $key => $value) {
    $permissao[] = $value->rota;
  }
  return $permissao;
}

function getFilial($id){
  $filial = [];
  $sql = 'SELECT * FROM filial_users where user_id = ?';
  $retorno_filial = Crud::getInstance()->getSQLGeneric($sql,array($id),TRUE);
  foreach ($retorno_filial as $key => $value) {
    $filial[] = $value->filial;
  }
  return $filial;
}

?>
