<?php
error_reporting(0);
require_once 'function.php';

$dao = Crud::getInstance();
$data = json_decode(file_get_contents("php://input"));
$user = $data->user;
$permissao = $data->permissao;
$filial = $data->filial;

if (!empty($user)) {

  if(!filter_var($user->email, FILTER_VALIDATE_EMAIL)){
    $retorno = [
      'retorno'=>false,
      'msg'=>'E-mail Inválido'
    ];
    echo json_encode($retorno);
    exit;
  }

  $retorno = adicionar_dados($user);

  if(!empty($permissao)){

    inserir_permissao($retorno['id'], $permissao);
  }
  if(!empty($filial)){
     inserir_filial($retorno['id'], $filial);
  }

  echo json_encode($retorno);
}

function adicionar_dados($user)
{

  $user->password = hash_senha($user);

  $sql_verificar_email = 'SELECT * FROM users where email = ?';
  $retorno_email = Crud::getInstance()->getSQLGeneric($sql_verificar_email, array($user->email), TRUE);
  if(!empty($retorno_email)){
    $retorno = [
      'retorno'=>false,
      'msg'=>'E-mail já cadastrado'
    ];
    echo json_encode($retorno);
    exit;
  }
  unset($user->p2);
  $user->situacao = 0;
  $retorno = Crud::getInstance('users')->insert($user);

  return $retorno;
}




?>
