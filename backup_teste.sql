-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: bi_casebras
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auditoria`
--

DROP TABLE IF EXISTS `auditoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditoria` (
  `page` varchar(250) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditoria`
--

LOCK TABLES `auditoria` WRITE;
/*!40000 ALTER TABLE `auditoria` DISABLE KEYS */;
INSERT INTO `auditoria` VALUES ('/Home',1,'2018-11-12 11:43:18'),('/usuarios',1,'2018-11-12 11:44:31'),('/Home',1,'2018-11-12 11:44:35'),('/Home',1,'2018-11-12 01:24:12'),('/Home',1,'2018-11-12 01:24:16'),('/Home',1,'2018-11-12 01:24:29'),('/Home',1,'2018-11-12 01:24:30'),('/Home',1,'2018-11-12 01:25:13'),('/Home',1,'2018-11-12 01:26:41'),('/Home',1,'2018-11-12 01:26:43'),('/Home',1,'2018-11-12 01:27:38'),('/Home',1,'2018-11-12 01:28:04'),('/Home',1,'2018-11-12 01:28:06'),('/Home',1,'2018-11-12 01:31:47'),('/Home',1,'2018-11-12 01:40:31'),('/Home',1,'2018-11-12 01:40:34'),('/Home',1,'2018-11-12 01:41:20'),('/Home',1,'2018-11-12 01:41:22'),('/Home',1,'2018-11-12 01:41:41'),('/Home',1,'2018-11-12 01:41:43'),('/Home',1,'2018-11-12 01:42:26'),('/Home',1,'2018-11-12 01:43:07'),('/Home',1,'2018-11-12 01:43:10'),('/Home',1,'2018-11-12 01:43:13'),('/Home',1,'2018-11-12 01:44:49'),('/Home',1,'2018-11-12 01:45:43'),('/Home',1,'2018-11-12 01:45:44'),('/Home',1,'2018-11-12 01:45:52'),('/Home',1,'2018-11-12 01:46:42'),('/Home',1,'2018-11-12 01:46:45'),('/Home',1,'2018-11-12 01:46:58'),('/Home',1,'2018-11-12 01:47:06'),('/Home',1,'2018-11-12 01:47:15'),('/Home',1,'2018-11-12 01:47:26'),('/Home',1,'2018-11-12 01:47:36'),('/Home',1,'2018-11-12 01:52:58'),('/Home',1,'2018-11-12 01:53:11'),('/Home',1,'2018-11-12 01:53:20'),('/Home',1,'2018-11-12 01:53:45'),('/Home',1,'2018-11-12 01:55:00'),('/Home',1,'2018-11-12 01:55:01'),('/Home',1,'2018-11-12 01:55:12'),('/Home',1,'2018-11-12 01:55:13'),('/Home',1,'2018-11-12 01:55:33'),('/Home',1,'2018-11-12 01:55:38'),('/Home',1,'2018-11-12 01:56:02'),('/Home',1,'2018-11-12 01:56:04'),('/Home',1,'2018-11-12 01:57:16'),('/Home',1,'2018-11-12 01:57:18'),('/Home',1,'2018-11-12 02:00:11'),('/Home',1,'2018-11-12 02:00:21'),('/Home',1,'2018-11-12 02:01:07'),('/Home',1,'2018-11-12 02:01:23'),('/Home',1,'2018-11-12 02:02:30'),('/Home',1,'2018-11-12 02:02:33'),('/Home',1,'2018-11-12 02:03:02'),('/Home',1,'2018-11-12 02:03:14'),('/Home',1,'2018-11-12 02:06:40'),('/Home',1,'2018-11-12 02:07:10'),('/Home',1,'2018-11-12 02:07:32'),('/Home',1,'2018-11-12 02:07:33'),('/Home',1,'2018-11-12 02:07:36'),('/Home',1,'2018-11-12 02:08:01'),('/Home',1,'2018-11-12 02:12:37'),('/Home',1,'2018-11-12 02:12:38'),('/Home',1,'2018-11-12 02:13:13'),('/Home',1,'2018-11-13 08:14:01'),('/Home',1,'2018-11-13 08:16:17'),('/Home',1,'2018-11-13 08:21:49'),('/Home',1,'2018-11-13 08:22:46'),('/Home',1,'2018-11-13 08:38:05'),('/Home',1,'2018-11-13 08:38:07'),('/Home',1,'2018-11-13 08:38:27'),('/Home',1,'2018-11-13 08:47:00'),('/Home',1,'2018-11-13 08:50:58'),('/Home',1,'2018-11-13 08:51:01'),('/Home',1,'2018-11-13 08:51:10'),('/Home',1,'2018-11-13 08:51:58'),('/Home',1,'2018-11-13 08:57:45'),('/Home',1,'2018-11-13 08:57:48'),('/Home',1,'2018-11-13 08:58:41'),('/Home',1,'2018-11-13 08:58:55'),('/Home',1,'2018-11-13 09:02:24'),('/Home',1,'2018-11-13 09:05:14'),('/Home',1,'2018-11-13 09:18:26'),('/Home',1,'2018-11-13 09:20:16'),('/Home',1,'2018-11-13 09:20:18'),('/Home',1,'2018-11-13 09:21:46'),('/Home',1,'2018-11-13 09:24:39'),('/Home',1,'2018-11-13 09:24:41'),('/Home',1,'2018-11-13 09:24:43'),('/Home',1,'2018-11-13 09:25:45'),('/Home',1,'2018-11-13 09:25:46'),('/Home',1,'2018-11-13 09:45:34'),('/Home',1,'2018-11-13 09:46:14'),('/Home',1,'2018-11-13 09:46:16'),('/Home',1,'2018-11-13 09:47:12'),('/Home',1,'2018-11-13 09:50:30'),('/Home',1,'2018-11-13 09:50:33'),('/Home',1,'2018-11-13 09:50:39'),('/Home',1,'2018-11-13 09:50:42'),('/Home',1,'2018-11-13 09:50:50'),('/Home',1,'2018-11-13 10:16:25'),('/Home',1,'2018-11-13 10:16:27'),('/Home',1,'2018-11-13 10:17:16'),('/Home',1,'2018-11-13 10:17:45'),('/Home',1,'2018-11-13 10:17:46'),('/Home',1,'2018-11-13 10:49:51'),('/Home',1,'2018-11-13 10:57:49'),('/Home',1,'2018-11-13 10:58:31'),('/Home',1,'2018-11-13 11:00:10'),('/Home',1,'2018-11-13 11:00:17'),('/Home',1,'2018-11-13 11:13:47'),('/Home',1,'2018-11-13 11:21:17'),('/Home',1,'2018-11-13 11:34:44'),('/Home',1,'2018-11-13 11:36:00'),('/Home',1,'2018-11-13 11:36:01'),('/Home',1,'2018-11-13 11:36:10'),('/analise-documental',1,'2018-11-13 11:36:21'),('/indicadores-harpia',1,'2018-11-13 11:36:34'),('/Home',1,'2018-11-13 11:37:06'),('/Home',1,'2018-11-13 11:39:34'),('/analise-documental',1,'2018-11-13 11:40:26'),('/Home',1,'2018-11-13 11:40:30'),('/home-formalizacao',1,'2018-11-13 11:40:31'),('/Home',1,'2018-11-13 11:40:32'),('/home-sde',1,'2018-11-13 11:40:33'),('/Home',1,'2018-11-13 11:40:34'),('/usuarios',1,'2018-11-13 11:40:36'),('/Home',1,'2018-11-13 11:40:40'),('/home-formalizacao',1,'2018-11-13 12:54:37'),('/Home',1,'2018-11-13 12:54:38'),('/analise-documental',1,'2018-11-13 01:34:08'),('/Home',1,'2018-11-13 01:34:09'),('/Home',1,'2018-11-14 10:24:03'),('/Home',1,'2018-11-14 10:29:16'),('/Home',1,'2018-11-14 10:32:27'),('',1,'2018-11-14 10:34:18'),('/Home',1,'2018-11-14 10:34:31'),('/Home',1,'2018-11-14 10:42:43'),('/!',1,'2018-11-14 10:43:27'),('/!',1,'2018-11-14 10:43:29'),('/Home',1,'2018-11-14 10:43:34'),('/Home',1,'2018-11-14 10:43:36'),('/contestacao',1,'2018-11-14 10:43:38'),('/contestacao',1,'2018-11-14 10:43:41'),('/contestacao',1,'2018-11-14 01:40:52'),('/novo-contestacao',1,'2018-11-14 01:40:53'),('/novo-contestacao',1,'2018-11-14 01:42:27'),('/novo-contestacao',1,'2018-11-14 01:42:36'),('/novo-contestacao',1,'2018-11-14 01:42:52'),('/novo-contestacao',1,'2018-11-14 01:42:58'),('/novo-contestacao',1,'2018-11-14 01:43:18'),('/novo-contestacao',1,'2018-11-14 01:44:03'),('/novo-contestacao',1,'2018-11-14 01:44:10'),('/novo-contestacao',1,'2018-11-14 01:45:23'),('/novo-contestacao',1,'2018-11-14 01:46:05'),('/novo-contestacao',1,'2018-11-14 01:46:13'),('/novo-contestacao',1,'2018-11-14 01:47:36'),('/novo-contestacao',1,'2018-11-14 01:47:50'),('/novo-contestacao',1,'2018-11-14 01:50:11'),('/novo-contestacao',1,'2018-11-14 01:50:20'),('/novo-contestacao',1,'2018-11-14 01:50:28'),('/novo-contestacao',1,'2018-11-14 01:50:33'),('/novo-contestacao',1,'2018-11-14 01:50:39'),('/novo-contestacao',1,'2018-11-14 01:50:50'),('/novo-contestacao',1,'2018-11-14 01:58:36'),('/novo-contestacao',1,'2018-11-14 01:58:50'),('/novo-contestacao',1,'2018-11-14 02:02:19'),('/novo-contestacao',1,'2018-11-14 02:05:01'),('/novo-contestacao',1,'2018-11-14 02:05:11'),('/novo-contestacao',1,'2018-11-14 02:06:39'),('/novo-contestacao',1,'2018-11-14 02:09:49'),('/novo-contestacao',1,'2018-11-14 02:14:39'),('/novo-contestacao',1,'2018-11-14 02:28:56'),('/novo-contestacao',1,'2018-11-14 02:28:59'),('/novo-contestacao',1,'2018-11-14 02:29:13'),('/novo-contestacao',1,'2018-11-14 02:29:48'),('/novo-contestacao',1,'2018-11-14 02:31:26'),('/novo-contestacao',1,'2018-11-14 02:31:29'),('/novo-contestacao',1,'2018-11-14 02:35:17'),('/novo-contestacao',1,'2018-11-14 02:40:59'),('/novo-contestacao',1,'2018-11-14 02:41:41'),('/novo-contestacao',1,'2018-11-14 02:41:44'),('/novo-contestacao',1,'2018-11-14 02:41:59'),('/novo-contestacao',1,'2018-11-14 02:42:24'),('/novo-contestacao',1,'2018-11-14 03:06:11'),('/novo-contestacao',1,'2018-11-14 03:12:08'),('/novo-contestacao',1,'2018-11-14 03:12:25'),('/novo-contestacao',1,'2018-11-14 03:12:42'),('/novo-contestacao',1,'2018-11-14 03:13:05'),('/novo-contestacao',1,'2018-11-14 03:13:18'),('/novo-contestacao',1,'2018-11-14 03:14:08'),('/novo-contestacao',1,'2018-11-14 03:15:47'),('/novo-contestacao',1,'2018-11-14 03:48:40'),('/novo-contestacao',1,'2018-11-14 03:51:27'),('/novo-contestacao',1,'2018-11-14 03:53:22'),('/novo-contestacao',1,'2018-11-14 04:05:31'),('/novo-contestacao',1,'2018-11-14 04:08:32'),('/novo-contestacao',1,'2018-11-14 04:28:25'),('/novo-contestacao',1,'2018-11-14 04:28:33'),('/novo-contestacao',1,'2018-11-14 04:29:02'),('/novo-contestacao',1,'2018-11-14 04:36:27'),('/novo-contestacao',1,'2018-11-16 09:12:21'),('/novo-contestacao',1,'2018-11-16 09:49:13'),('/novo-contestacao',1,'2018-11-16 09:55:03'),('/Home',1,'2018-11-16 01:13:53'),('/contestacao',1,'2018-11-16 01:14:11'),('/novo-contestacao',1,'2018-11-16 01:14:15'),('/novo-contestacao',1,'2018-11-16 01:48:52'),('/novo-contestacao',1,'2018-11-16 02:02:52'),('/novo-contestacao',1,'2018-11-16 02:12:15'),('/novo-contestacao',1,'2018-11-16 02:14:40'),('/novo-contestacao',1,'2018-11-16 02:15:02'),('/novo-contestacao',1,'2018-11-16 02:18:31'),('/novo-contestacao',1,'2018-11-16 02:19:35'),('/Home',1,'2018-11-16 02:20:40'),('/contestacao',1,'2018-11-16 02:20:50'),('/Home',1,'2018-11-16 02:20:53'),('/home-sde',1,'2018-11-16 02:20:54'),('/Home',1,'2018-11-16 02:20:57'),('/contestacao',1,'2018-11-16 02:20:58'),('/indicadores-contestacao',1,'2018-11-16 02:21:01'),('/contestacao',1,'2018-11-16 02:21:03'),('/novo-contestacao',1,'2018-11-16 02:21:05'),('/Home',1,'2018-11-16 02:22:20'),('/usuarios',1,'2018-11-16 02:22:23'),('/usuarios/novo',1,'2018-11-16 02:22:24'),('/usuarios',1,'2018-11-16 02:22:43'),('/Home',1,'2018-11-16 02:22:46'),('/Home',1,'2018-11-16 02:34:03'),('/contestacao',1,'2018-11-16 02:37:00'),('/novo-contestacao',1,'2018-11-16 02:37:02'),('/novo-contestacao',1,'2018-11-16 02:40:56'),('/Home',16,'2018-11-16 03:15:14'),('/Home',1,'2018-11-16 03:19:24'),('/contestacao',1,'2018-11-16 03:19:30'),('/novo-contestacao',1,'2018-11-16 03:19:32'),('/contestacao',16,'2018-11-16 03:24:38'),('/novo-contestacao',16,'2018-11-16 03:24:41'),('/Home',16,'2018-11-16 03:28:18'),('/contestacao',16,'2018-11-16 03:28:31'),('/novo-contestacao',16,'2018-11-16 03:28:44'),('/Home',16,'2018-11-16 03:30:55'),('/contestacao',16,'2018-11-16 03:30:56'),('/indicadores-contestacao',16,'2018-11-16 03:30:58'),('/novo-contestacao',1,'2018-11-16 03:31:13'),('/Home',1,'2018-11-16 03:31:14'),('/contestacao',1,'2018-11-16 03:31:16'),('/contestacao',1,'2018-11-16 03:31:17'),('/contestacao',16,'2018-11-16 03:31:17'),('/contestacao',1,'2018-11-16 03:31:19'),('/Home',16,'2018-11-16 03:31:22'),('/contestacao',1,'2018-11-16 03:46:53'),('/contestacao',1,'2018-11-16 03:47:22'),('/novo-contestacao',1,'2018-11-16 03:50:17'),('/',1,'2018-11-16 03:50:19'),('/novo-contestacao',1,'2018-11-16 03:50:24'),('/novo-contestacao',1,'2018-11-16 03:50:44'),('/',1,'2018-11-16 03:50:49'),('/Home',16,'2018-11-19 09:46:49'),('/analise-documental',16,'2018-11-19 09:46:59'),('/Home',16,'2018-11-19 09:47:23'),('/home-sde',16,'2018-11-19 09:47:26'),('/Home',16,'2018-11-19 09:47:29'),('/home-monitoramento',16,'2018-11-19 09:47:30'),('/Home',16,'2018-11-19 09:50:17'),('/analise-documental',16,'2018-11-19 09:50:18'),('/Home',16,'2018-11-19 10:06:21'),('/contestacao',16,'2018-11-19 10:06:24'),('/novo-contestacao',16,'2018-11-19 10:06:32'),('/',16,'2018-11-19 10:07:41'),('/novo-contestacao',16,'2018-11-19 10:07:45'),('/Home',16,'2018-11-19 10:07:46'),('/home-formalizacao',16,'2018-11-19 10:07:50'),('/indicadores-formalizacao',16,'2018-11-19 10:07:52'),('/indicadores-formalizacao',16,'2018-11-19 10:08:36'),('/home-formalizacao',16,'2018-11-19 10:08:38'),('/Home',16,'2018-11-19 10:08:40'),('/analise-documental',16,'2018-11-19 10:08:43'),('/indicadores-harpia',16,'2018-11-19 10:08:45'),('/Home',16,'2018-11-19 12:27:42'),('/contestacao',16,'2018-11-19 12:27:45'),('/novo-contestacao',16,'2018-11-19 12:27:56'),('/Home',16,'2018-11-19 03:15:37'),('/contestacao',16,'2018-11-19 03:15:39'),('/indicadores-contestacao',16,'2018-11-19 03:15:44'),('/indicadores-contestacao',16,'2018-11-19 03:50:26'),('/indicadores-contestacao',16,'2018-11-19 03:50:47'),('/indicadores-contestacao',16,'2018-11-19 03:52:29'),('/Home',16,'2018-11-19 03:52:39'),('/fonte',16,'2018-11-19 03:52:46'),('/fonte/edit/21',16,'2018-11-19 03:52:50'),('/Home',16,'2018-11-19 03:53:09'),('/fonte',16,'2018-11-19 03:53:10'),('/fonte/edit/4',16,'2018-11-19 03:53:18'),('/Home',16,'2018-11-19 03:53:45'),('/importacao',16,'2018-11-19 03:53:46'),('/Home',16,'2018-11-19 03:53:53'),('/usuarios',16,'2018-11-19 03:53:54'),('/Home',16,'2018-11-19 03:54:06'),('/contestacao',16,'2018-11-19 03:54:18'),('/novo-contestacao',16,'2018-11-19 03:54:21'),('/Home',16,'2018-11-19 03:54:23'),('/contestacao',16,'2018-11-19 03:54:25'),('/indicadores-contestacao',16,'2018-11-19 03:54:26'),('/contestacao',16,'2018-11-19 03:54:29'),('/Home',16,'2018-11-19 03:54:30'),('/home-monitoramento',16,'2018-11-19 03:54:33'),('/indicadores-monitoramento',16,'2018-11-19 03:54:35'),('/indicadores-monitoramento',16,'2018-11-19 05:41:16'),('/Home',16,'2018-11-19 05:41:20'),('/contestacao',16,'2018-11-19 05:41:29'),('/novo-contestacao',16,'2018-11-19 05:41:30'),('/Home',1,'2018-11-20 05:30:00'),('/contestacao',1,'2018-11-20 05:30:01'),('/lista-contestacao',1,'2018-11-20 05:30:03'),('/lista-contestacao',1,'2018-11-20 05:32:45'),('/Home',1,'2018-11-20 05:32:46'),('/Home',1,'2018-11-20 05:32:47'),('/contestacao',1,'2018-11-20 05:33:17'),('/novo-contestacao',1,'2018-11-20 05:33:20'),('/novo-contestacao',1,'2018-11-20 05:38:00'),('/lista-contestacao',1,'2018-11-20 05:38:45'),('/usuarios/novo/',1,'2018-11-20 05:39:58'),('/usuarios/novo',1,'2018-11-20 05:39:58'),('/Home',1,'2018-11-20 05:39:59'),('/contestacao',1,'2018-11-20 05:40:01'),('/indicadores-contestacao',1,'2018-11-20 05:40:03'),('/contestacao',1,'2018-11-20 05:40:05'),('/lista-contestacao',1,'2018-11-20 05:40:07'),('/novo-contestacao',1,'2018-11-20 05:42:21'),('/Home',16,'2018-11-21 08:38:58'),('/usuarios',16,'2018-11-21 08:39:00'),('/usuarios/novo/14',16,'2018-11-21 08:39:05'),('/Home',16,'2018-11-21 08:39:16'),('/contestacao',16,'2018-11-21 08:39:18'),('/Home',16,'2018-11-21 08:40:51'),('/contestacao',16,'2018-11-21 08:40:53'),('/novo-contestacao',16,'2018-11-21 08:41:02'),('/Home',16,'2018-11-21 08:41:08'),('/contestacao',16,'2018-11-21 08:41:09'),('/novo-contestacao',16,'2018-11-21 08:41:13'),('/Home',16,'2018-11-21 08:51:42'),('/contestacao',16,'2018-11-21 08:51:58'),('/Home',16,'2018-11-21 08:52:38'),('/contestacao',16,'2018-11-21 08:52:43'),('/novo-contestacao',16,'2018-11-21 08:52:49'),('/Home',16,'2018-11-21 09:05:08'),('/contestacao',16,'2018-11-21 09:05:09'),('/lista-contestacao',16,'2018-11-21 09:05:11'),('/contestacao',16,'2018-11-21 09:06:34'),('/Home',16,'2018-11-21 09:06:38'),('/contestacao',16,'2018-11-21 09:06:40'),('/novo-contestacao',16,'2018-11-21 09:06:44'),('/Home',16,'2018-11-21 09:09:08'),('/contestacao',16,'2018-11-21 09:09:10'),('/novo-contestacao',16,'2018-11-21 09:09:11'),('/lista-contestacao',16,'2018-11-21 09:09:13'),('/Home',16,'2018-11-21 09:09:15'),('/Home',16,'2018-11-21 09:22:14'),('/contestacao',16,'2018-11-21 09:22:15'),('/novo-contestacao',16,'2018-11-21 09:22:18'),('/Home',16,'2018-11-21 12:00:04'),('/contestacao',16,'2018-11-21 12:00:07'),('/lista-contestacao',16,'2018-11-21 12:00:14'),('/lista-contestacao',16,'2018-11-21 12:00:21'),('/novo-contestacao',16,'2018-11-21 12:01:25'),('/novo-contestacao',16,'2018-11-21 12:01:41'),('/lista-contestacao',16,'2018-11-21 12:02:50'),('/novo-contestacao',16,'2018-11-21 12:02:53'),('/lista-contestacao',16,'2018-11-21 12:02:55'),('/lista-contestacao',16,'2018-11-21 12:03:03'),('/Home',16,'2018-11-22 12:07:04'),('/contestacao',16,'2018-11-22 12:08:08'),('/Home',16,'2018-11-22 12:08:11'),('/importacao',16,'2018-11-22 12:08:13'),('/nova-importacao',16,'2018-11-22 12:08:19'),('/Home',16,'2018-11-22 12:08:31'),('/fonte',16,'2018-11-22 12:08:35'),('/fonte/edit/3',16,'2018-11-22 12:08:41'),('/fontes',16,'2018-11-22 12:08:54'),('/fonte/edit/3',16,'2018-11-22 12:09:00'),('/fonte',16,'2018-11-22 12:09:00'),('/Home',16,'2018-11-22 12:09:02'),('/analise-documental',16,'2018-11-22 12:09:08'),('/indicadores-harpiaxwork',16,'2018-11-22 12:09:12'),('',16,'2018-11-22 12:09:46'),('',16,'2018-11-22 12:09:53'),('/Home',16,'2018-11-22 12:10:15'),('/contestacao',16,'2018-11-22 12:10:17'),('/Home',16,'2018-11-22 12:10:19'),('/home-formalizacao',16,'2018-11-22 12:10:21'),('/indicadores-formalizacao',16,'2018-11-22 12:10:25'),('/home-formalizacao',16,'2018-11-22 12:10:30'),('/Home',16,'2018-11-22 12:10:31'),('/contestacao',16,'2018-11-22 12:10:32'),('/lista-contestacao',16,'2018-11-22 12:10:35'),('/novo-contestacao',16,'2018-11-22 12:11:11'),('/lista-contestacao',16,'2018-11-22 12:11:46'),('/novo-contestacao',16,'2018-11-22 12:15:03'),('/lista-contestacao',16,'2018-11-22 12:34:47'),('/novo-contestacao',16,'2018-11-22 12:38:01'),('/lista-contestacao',16,'2018-11-22 12:38:13'),('/novo-contestacao',16,'2018-11-22 12:38:15'),('/lista-contestacao',16,'2018-11-22 12:52:06'),('/novo-contestacao',16,'2018-11-22 12:52:09'),('/Home',16,'2018-11-22 02:36:32'),('/contestacao',16,'2018-11-22 02:36:48'),('/lista-contestacao',16,'2018-11-22 02:36:53'),('/Home',16,'2018-11-23 08:48:22'),('/contestacao',16,'2018-11-23 08:48:39'),('/lista-contestacao',16,'2018-11-23 08:54:33'),('/novo-contestacao',16,'2018-11-23 09:18:32'),('/lista-contestacao',16,'2018-11-23 09:21:08'),('/novo-contestacao',16,'2018-11-23 09:30:17'),('/lista-contestacao',16,'2018-11-23 09:32:43'),('/novo-contestacao',16,'2018-11-23 09:42:11'),('/lista-contestacao',16,'2018-11-23 09:43:29'),('/novo-contestacao',16,'2018-11-23 09:44:28'),('/lista-contestacao',16,'2018-11-23 09:51:01'),('/novo-contestacao',16,'2018-11-23 09:57:44'),('/lista-contestacao',16,'2018-11-23 10:04:48'),('/Home',16,'2018-11-23 10:04:51'),('/contestacao',16,'2018-11-23 10:04:53'),('/novo-contestacao',16,'2018-11-23 10:04:54'),('/lista-contestacao',16,'2018-11-23 10:16:22'),('/novo-contestacao',16,'2018-11-23 10:16:28'),('/lista-contestacao',16,'2018-11-23 10:16:40'),('/Home',1,'2018-11-24 11:18:04'),('/contestacao',1,'2018-11-24 11:18:05'),('/novo-contestacao',1,'2018-11-24 11:18:06'),('/novo-contestacao',1,'2018-11-24 11:18:08'),('/novo-contestacao',1,'2018-11-24 11:18:17'),('/novo-contestacao',1,'2018-11-24 11:18:30'),('/Home',1,'2018-11-24 11:25:24'),('/contestacao',1,'2018-11-24 11:25:25'),('/contestacao',1,'2018-11-24 11:25:27'),('/lista-contestacao',1,'2018-11-24 11:25:29'),('/Home',1,'2018-11-26 08:16:39'),('/contestacao',1,'2018-11-26 08:16:43'),('/lista-contestacao',1,'2018-11-26 08:16:45'),('/lista-contestacao',1,'2018-11-26 08:17:33'),('/novo-contestacao/1',1,'2018-11-26 08:17:36'),('/Home',1,'2018-11-26 08:17:42'),('/contestacao',1,'2018-11-26 08:20:03'),('/Home',16,'2018-11-26 11:54:17'),('/contestacao',16,'2018-11-26 11:54:23'),('/indicadores-contestacao',16,'2018-11-26 11:54:25'),('/contestacao',16,'2018-11-26 11:54:27'),('/lista-contestacao',16,'2018-11-26 11:54:30'),('/lista-contestacao',16,'2018-11-26 12:09:23'),('/novo-contestacao',16,'2018-11-26 12:09:31'),('/lista-contestacao',16,'2018-11-26 12:09:34'),('/novo-contestacao',16,'2018-11-26 12:26:05'),('/lista-contestacao',16,'2018-11-26 12:26:45'),('/Home',16,'2018-11-26 12:26:52'),('/contestacao',16,'2018-11-26 12:26:53'),('/indicadores-contestacao',16,'2018-11-26 12:26:54'),('/contestacao',16,'2018-11-26 12:26:58'),('/Home',16,'2018-11-26 12:29:07'),('/home-formalizacao',16,'2018-11-26 12:29:08'),('/indicadores-formalizacao',16,'2018-11-26 12:29:10'),('/home-formalizacao',16,'2018-11-26 12:33:03'),('/indicadores-formalizacao',16,'2018-11-26 12:34:22'),('/Home',16,'2018-11-26 01:39:39'),('/contestacao',16,'2018-11-26 01:39:40'),('/lista-contestacao',16,'2018-11-26 01:39:42'),('/novo-contestacao',16,'2018-11-26 01:43:26'),('/lista-contestacao',16,'2018-11-26 01:43:27'),('/Home',16,'2018-11-26 02:45:33'),('/contestacao',16,'2018-11-26 02:45:37'),('/lista-contestacao',16,'2018-11-26 02:45:38'),('/Home',16,'2018-11-26 06:00:12'),('/contestacao',16,'2018-11-26 06:00:14'),('/indicadores-contestacao',16,'2018-11-26 06:00:25'),('/contestacao',16,'2018-11-26 06:00:28'),('/lista-contestacao',16,'2018-11-26 06:00:29'),('/novo-contestacao',16,'2018-11-26 06:00:39'),('/Home',16,'2018-11-27 10:45:34'),('/usuarios',16,'2018-11-27 10:45:36'),('/usuarios/novo/14',16,'2018-11-27 10:45:43'),('/usuarios',16,'2018-11-27 10:46:02'),('/Home',16,'2018-11-27 10:46:21'),('/contestacao',16,'2018-11-27 10:46:23'),('/lista-contestacao',16,'2018-11-27 10:46:29'),('/novo-contestacao',16,'2018-11-27 10:49:34'),('/Home',1,'2018-11-27 11:27:49'),('/contestacao',1,'2018-11-27 11:27:51'),('/Home',16,'2018-11-27 01:16:17'),('/contestacao',16,'2018-11-27 01:16:22'),('/novo-contestacao',16,'2018-11-27 01:16:32'),('/novo-contestacao',16,'2018-11-27 01:19:39'),('/Home',16,'2018-11-27 01:21:52'),('/contestacao',16,'2018-11-27 01:21:55'),('/novo-contestacao',16,'2018-11-27 01:21:59'),('/lista-contestacao',16,'2018-11-27 01:24:10'),('/novo-contestacao',16,'2018-11-27 01:24:21'),('/novo-contestacao',16,'2018-11-27 01:33:51'),('/novo-contestacao',16,'2018-11-27 01:33:58'),('/Home',16,'2018-11-27 01:34:19'),('/contestacao',16,'2018-11-27 01:34:23'),('/novo-contestacao',16,'2018-11-27 01:34:24'),('/Home',1,'2018-11-27 02:01:48'),('/contestacao',1,'2018-11-27 02:01:50'),('/Home',1,'2018-11-27 02:30:54'),('/contestacao',1,'2018-11-27 02:41:16'),('/contestacao',1,'2018-11-27 02:56:01'),('/Home',1,'2018-11-27 03:00:22'),('/usuarios',1,'2018-11-27 03:01:58'),('/usuarios',1,'2018-11-27 03:19:55'),('/Home',16,'2018-11-28 09:11:44'),('/contestacao',16,'2018-11-28 09:11:46'),('/indicadores-contestacao',16,'2018-11-28 09:11:53'),('/contestacao',16,'2018-11-28 09:11:56'),('/lista-contestacao',16,'2018-11-28 09:11:56'),('/novo-contestacao',16,'2018-11-28 09:12:04'),('/lista-contestacao',16,'2018-11-28 09:16:23'),('/novo-contestacao',16,'2018-11-28 09:16:24'),('/novo-contestacao',16,'2018-11-28 09:16:27'),('/Home',1,'2018-11-28 09:16:58'),('/contestacao',1,'2018-11-28 09:17:01'),('/lista-contestacao',1,'2018-11-28 09:17:02'),('/novo-contestacao',1,'2018-11-28 09:17:03'),('/novo-contestacao',16,'2018-11-28 09:18:47'),('/Home',1,'2018-11-28 09:57:29'),('/analise-documental',1,'2018-11-28 09:57:31'),('/indicadores-harpiaxwork',1,'2018-11-28 09:57:32'),('/indicadores-harpiaxwork',1,'2018-11-28 10:17:31'),('/indicadores-harpiaxwork',1,'2018-11-28 10:17:32'),('/usuarios',1,'2018-11-28 10:45:38'),('/Home',1,'2018-11-28 02:04:57'),('/contestacao',1,'2018-11-28 02:05:11'),('/lista-contestacao',1,'2018-11-28 02:05:18'),('/novo-contestacao/2',1,'2018-11-28 02:14:45'),('/lista-contestacao',1,'2018-11-28 02:14:51'),('/lista-contestacao',1,'2018-11-28 02:14:53'),('/novo-contestacao/1',1,'2018-11-28 02:15:01'),('/lista-contestacao',1,'2018-11-28 02:15:03'),('/lista-contestacao',1,'2018-11-28 02:22:47'),('/novo-contestacao/1',1,'2018-11-28 02:22:55'),('/lista-contestacao',1,'2018-11-28 02:23:01'),('/lista-contestacao',1,'2018-11-28 02:23:02'),('/lista-contestacao',1,'2018-11-28 02:30:22'),('/Home',16,'2018-11-28 02:39:04'),('/contestacao',16,'2018-11-28 02:39:15'),('/lista-contestacao',16,'2018-11-28 02:39:27'),('/novo-contestacao',16,'2018-11-28 02:41:17'),('/lista-contestacao',16,'2018-11-28 02:55:09'),('/novo-contestacao',16,'2018-11-28 02:59:26'),('/lista-contestacao',16,'2018-11-28 03:04:08'),('/novo-contestacao',16,'2018-11-28 03:04:11'),('/lista-contestacao',16,'2018-11-28 03:10:14'),('/novo-contestacao',16,'2018-11-28 03:10:55'),('/lista-contestacao',16,'2018-11-28 03:13:48'),('/novo-contestacao',16,'2018-11-28 03:13:49'),('/Home',16,'2018-11-29 08:03:20'),('/contestacao',16,'2018-11-29 08:03:27'),('/Home',16,'2018-11-29 08:03:35'),('/contestacao',16,'2018-11-29 08:03:36'),('/lista-contestacao',16,'2018-11-29 08:03:48'),('/Home',16,'2018-11-29 08:54:33'),('/contestacao',16,'2018-11-29 08:54:41'),('/lista-contestacao',16,'2018-11-29 09:04:49'),('/Home',16,'2018-11-29 09:06:39'),('/contestacao',16,'2018-11-29 09:06:40'),('/Home',16,'2018-11-29 09:06:45'),('/contestacao',16,'2018-11-29 09:07:31'),('/lista-contestacao',16,'2018-11-29 09:07:33'),('/Home',16,'2018-11-29 11:45:29'),('/contestacao',16,'2018-11-29 11:45:31'),('/novo-contestacao',16,'2018-11-29 11:45:33'),('/lista-contestacao',16,'2018-11-29 11:53:28'),('/novo-contestacao/49',16,'2018-11-29 11:54:30'),('/lista-contestacao',16,'2018-11-29 12:16:31'),('/novo-contestacao',16,'2018-11-29 12:22:54'),('/lista-contestacao',16,'2018-11-29 12:25:00'),('/novo-contestacao',16,'2018-11-29 12:25:14'),('/Home',16,'2018-11-30 08:22:08'),('/contestacao',16,'2018-11-30 08:22:32'),('/novo-contestacao',16,'2018-11-30 08:22:39'),('/lista-contestacao',16,'2018-11-30 08:22:42'),('/novo-contestacao',16,'2018-11-30 08:24:22'),('/lista-contestacao',16,'2018-11-30 08:27:37'),('/novo-contestacao',16,'2018-11-30 08:28:45'),('/lista-contestacao',16,'2018-11-30 09:08:16'),('/novo-contestacao',16,'2018-11-30 09:08:50'),('/Home',16,'2018-11-30 10:08:10'),('/contestacao',16,'2018-11-30 10:08:13'),('/novo-contestacao',16,'2018-11-30 10:08:16'),('/lista-contestacao',16,'2018-11-30 10:12:55'),('/lista-contestacao',16,'2018-11-30 10:13:46'),('/novo-contestacao',16,'2018-11-30 10:16:12'),('/Home',16,'2018-12-03 08:43:02'),('/contestacao',16,'2018-12-03 08:43:12'),('/lista-contestacao',16,'2018-12-03 08:43:43'),('/Home',16,'2018-12-03 08:43:46'),('/contestacao',16,'2018-12-03 08:43:47'),('/indicadores-contestacao',16,'2018-12-03 08:43:49'),('/contestacao',16,'2018-12-03 08:43:55'),('/Home',1,'2018-12-03 10:26:26'),('/Home',1,'2018-12-03 10:27:43'),('/Home',1,'2018-12-03 10:27:45'),('/contestacao',1,'2018-12-03 10:27:47'),('/lista-contestacao',1,'2018-12-03 10:27:57'),('/novo-contestacao',1,'2018-12-03 10:28:01'),('/indicadores-contestacao',1,'2018-12-03 10:28:03'),('/novo-contestacao',1,'2018-12-03 10:28:05'),('/Home',1,'2018-12-03 10:28:06'),('/Home',16,'2018-12-03 11:05:03'),('/contestacao',16,'2018-12-03 11:05:06'),('/lista-contestacao',16,'2018-12-03 11:05:08'),('/Home',16,'2018-12-03 02:04:55'),('/usuarios',16,'2018-12-03 02:04:57'),('/usuarios/novo',16,'2018-12-03 02:05:03'),('/usuarios',16,'2018-12-03 02:06:03'),('/Home',16,'2018-12-03 02:08:14'),('/contestacao',16,'2018-12-03 02:08:18'),('/lista-contestacao',16,'2018-12-03 02:08:22'),('/Home',16,'2018-12-03 04:00:22'),('/contestacao',16,'2018-12-03 04:00:24'),('/lista-contestacao',16,'2018-12-03 04:00:25'),('/Home',1,'2018-12-03 04:31:03'),('/contestacao',1,'2018-12-03 04:31:05'),('/indicadores-contestacao',1,'2018-12-03 04:31:07'),('/indicadores-contestacao',1,'2018-12-03 04:31:09'),('/Home',16,'2018-12-03 04:44:48'),('/contestacao',16,'2018-12-03 04:44:51'),('/indicadores-contestacao',16,'2018-12-03 04:44:52'),('/Home',16,'2018-12-03 04:45:54'),('/contestacao',16,'2018-12-03 04:45:56'),('/indicadores-contestacao',16,'2018-12-03 04:46:02'),('/indicadores-contestacao',1,'2018-12-03 04:51:53'),('/lista-contestacao',1,'2018-12-03 04:51:56'),('/Home',16,'2018-12-03 05:21:49'),('/contestacao',16,'2018-12-03 05:21:51'),('/indicadores-contestacao',16,'2018-12-03 05:21:52'),('/indicadores-contestacao',16,'2018-12-03 05:37:12'),('/indicadores-contestacao',16,'2018-12-03 05:39:07'),('/Home',16,'2018-12-03 05:43:27'),('/home-formalizacao',16,'2018-12-03 05:44:06'),('/indicadores-formalizacao',16,'2018-12-03 05:44:08'),('/home-formalizacao',16,'2018-12-03 05:44:11'),('/Home',16,'2018-12-03 05:54:27'),('/usuarios',16,'2018-12-03 05:54:30'),('/usuarios/novo/17',16,'2018-12-03 05:54:33'),('/Home',16,'2018-12-04 08:26:22'),('/contestacao',16,'2018-12-04 08:26:30'),('/indicadores-contestacao',16,'2018-12-04 08:27:06'),('/lista-contestacao',16,'2018-12-04 08:29:37'),('/Home',16,'2018-12-04 11:50:25'),('/contestacao',16,'2018-12-04 11:53:51'),('/indicadores-contestacao',16,'2018-12-04 11:54:37'),('/lista-contestacao',16,'2018-12-04 11:56:55'),('/Home',16,'2018-12-04 11:59:22'),('/Home',1,'2018-12-04 01:31:30'),('/usuarios',1,'2018-12-04 01:31:32'),('/Home',16,'2018-12-04 01:57:50'),('/Home',1,'2018-12-04 02:02:19'),('/fonte',1,'2018-12-04 02:02:21'),('/fonte/edit/4',1,'2018-12-04 02:02:25'),('/fonte/edit/4',1,'2018-12-04 02:03:34'),('/contestacao',16,'2018-12-04 02:04:15'),('/Home',16,'2018-12-04 02:04:27'),('/contestacao',16,'2018-12-04 02:05:11'),('/indicadores-contestacao',16,'2018-12-04 02:05:14'),('/Home',16,'2018-12-04 03:42:32'),('/contestacao',16,'2018-12-04 03:42:36'),('/indicadores-contestacao',16,'2018-12-04 03:42:42'),('/lista-contestacao',16,'2018-12-04 03:46:47'),('/lista-contestacao',16,'2018-12-04 03:47:33'),('/novo-contestacao',16,'2018-12-04 03:53:48'),('/lista-contestacao',16,'2018-12-04 03:56:49'),('/Home',16,'2018-12-04 03:58:40'),('/usuarios',16,'2018-12-04 03:58:42'),('/usuarios/novo',16,'2018-12-04 03:58:45'),('/usuarios',16,'2018-12-04 04:00:01'),('/Home',16,'2018-12-04 04:06:46'),('/Home',18,'2018-12-04 04:10:51'),('/contestacao',18,'2018-12-04 04:10:56'),('/lista-contestacao',18,'2018-12-04 04:11:04'),('/lista-contestacao',18,'2018-12-04 04:27:37'),('/contestacao',16,'2018-12-04 04:27:37'),('/lista-contestacao',16,'2018-12-04 04:27:40'),('/Home',16,'2018-12-05 10:06:38'),('/contestacao',16,'2018-12-05 10:06:39'),('/lista-contestacao',16,'2018-12-05 10:06:43'),('/novo-contestacao',16,'2018-12-05 10:06:51'),('/lista-contestacao',16,'2018-12-05 10:18:09'),('/novo-contestacao',16,'2018-12-05 10:18:10'),('/Home',18,'2018-12-05 10:49:24'),('/contestacao',18,'2018-12-05 10:49:31'),('/lista-contestacao',18,'2018-12-05 10:50:08'),('/Home',18,'2018-12-05 10:50:15'),('/contestacao',18,'2018-12-05 10:50:16'),('/lista-contestacao',18,'2018-12-05 10:50:18'),('/novo-contestacao',18,'2018-12-05 10:51:02'),('/lista-contestacao',18,'2018-12-05 11:09:49'),('/Home',1,'2018-12-05 11:47:44'),('/usuarios',1,'2018-12-05 11:47:46'),('/Home',1,'2018-12-05 11:48:04'),('/Home',16,'2018-12-05 12:05:50'),('/contestacao',16,'2018-12-05 12:05:52'),('/indicadores-contestacao',16,'2018-12-05 12:05:55'),('/Home',1,'2018-12-05 01:53:33'),('/analise-documental',1,'2018-12-05 01:53:34'),('/analise-documental',1,'2018-12-05 01:53:37'),('/configuracao/1',1,'2018-12-05 01:53:54'),('/Home',1,'2018-12-05 01:53:56'),('/home-formalizacao',1,'2018-12-05 01:53:57'),('/configuracao/2',1,'2018-12-05 01:53:59'),('/Home',1,'2018-12-05 01:54:00'),('/Home',16,'2018-12-05 02:30:23'),('/contestacao',16,'2018-12-05 02:30:25'),('/indicadores-contestacao',16,'2018-12-05 02:30:29'),('/lista-contestacao',16,'2018-12-05 02:31:52'),('/Home',16,'2018-12-05 02:32:22'),('/contestacao',16,'2018-12-05 02:32:24'),('/lista-contestacao',16,'2018-12-05 02:32:26'),('/Home',18,'2018-12-05 03:26:10'),('/contestacao',18,'2018-12-05 03:26:12'),('/lista-contestacao',18,'2018-12-05 03:26:14'),('/Home',18,'2018-12-05 04:46:58'),('/contestacao',18,'2018-12-05 04:47:00'),('/lista-contestacao',18,'2018-12-05 04:47:02'),('/Home',18,'2018-12-06 08:51:35'),('/contestacao',18,'2018-12-06 08:57:53'),('/lista-contestacao',18,'2018-12-06 08:58:08'),('/novo-contestacao',18,'2018-12-06 08:58:54'),('/indicadores-contestacao',18,'2018-12-06 08:59:15'),('/novo-contestacao',18,'2018-12-06 09:00:15'),('/indicadores-contestacao',18,'2018-12-06 09:00:17'),('/novo-contestacao',18,'2018-12-06 09:00:20'),('/Home',18,'2018-12-06 09:25:41'),('/Home',1,'2018-12-06 01:01:52'),('/usuarios',1,'2018-12-06 01:01:54'),('/Home',18,'2018-12-06 04:30:37'),('/contestacao',18,'2018-12-06 04:30:47'),('/novo-contestacao',18,'2018-12-06 04:30:55'),('/novo-contestacao',18,'2018-12-06 04:40:47'),('/Home',1,'2018-12-06 05:12:53'),('/usuarios',1,'2018-12-06 05:12:56'),('/Home',16,'2018-12-06 06:24:22'),('/contestacao',16,'2018-12-06 06:24:25'),('/configuracao/8',16,'2018-12-06 06:24:29'),('/contestacao',16,'2018-12-06 06:24:36'),('/indicadores-contestacao',16,'2018-12-06 06:24:36'),('/novo-contestacao',16,'2018-12-06 06:24:39'),('/lista-contestacao',16,'2018-12-06 06:28:09'),('/novo-contestacao',16,'2018-12-06 06:28:12'),('/lista-contestacao',16,'2018-12-06 06:33:37'),('/indicadores-contestacao',16,'2018-12-06 06:34:22'),('/Home',16,'2018-12-06 06:35:27'),('/contestacao',16,'2018-12-06 06:35:28'),('/indicadores-contestacao',16,'2018-12-06 06:35:30'),('/lista-contestacao',16,'2018-12-06 06:37:02'),('/Home',18,'2018-12-07 11:17:45'),('/contestacao',18,'2018-12-07 11:18:49'),('/indicadores-contestacao',18,'2018-12-07 11:25:14'),('/lista-contestacao',18,'2018-12-07 11:26:08'),('/indicadores-contestacao',18,'2018-12-07 11:27:42'),('/lista-contestacao',18,'2018-12-07 11:27:44'),('/Home',16,'2018-12-07 12:00:48'),('/contestacao',16,'2018-12-07 12:00:55'),('/lista-contestacao',16,'2018-12-07 12:02:09'),('/novo-contestacao',16,'2018-12-07 12:02:21'),('/lista-contestacao',16,'2018-12-07 12:13:43'),('/novo-contestacao',16,'2018-12-07 12:13:48'),('/lista-contestacao',16,'2018-12-07 12:17:22'),('/novo-contestacao',16,'2018-12-07 12:17:28'),('/lista-contestacao',16,'2018-12-07 12:18:59'),('/novo-contestacao',16,'2018-12-07 12:19:04'),('/lista-contestacao',16,'2018-12-07 12:25:20'),('/novo-contestacao',16,'2018-12-07 12:25:30'),('/lista-contestacao',16,'2018-12-07 12:28:58'),('/novo-contestacao',16,'2018-12-07 12:29:02'),('/lista-contestacao',16,'2018-12-07 12:30:43'),('/novo-contestacao',16,'2018-12-07 12:30:58'),('/Home',16,'2018-12-07 04:40:19'),('/contestacao',16,'2018-12-07 04:40:20'),('/indicadores-contestacao',16,'2018-12-07 04:40:22'),('/Home',16,'2018-12-07 04:40:40'),('/contestacao',16,'2018-12-07 04:40:42'),('/indicadores-contestacao',16,'2018-12-07 04:40:45'),('/Home',16,'2018-12-07 04:42:37'),('/usuarios',16,'2018-12-07 04:42:39'),('/usuarios/novo/16',16,'2018-12-07 04:42:44'),('/Home',16,'2018-12-07 04:44:58'),('/analise-documental',16,'2018-12-07 04:45:01'),('/indicadores-harpia',16,'2018-12-07 04:45:03'),('/indicadores-harpiaxwork',16,'2018-12-07 04:45:52'),('/analise-documental',16,'2018-12-07 04:46:38'),('/Home',16,'2018-12-07 04:46:42'),('/home-sde',16,'2018-12-07 04:46:45'),('/indicadores-sde',16,'2018-12-07 04:46:46'),('/home-sde',16,'2018-12-07 04:47:11'),('/Home',16,'2018-12-07 04:48:04'),('/contestacao',16,'2018-12-07 04:48:05'),('/novo-contestacao',16,'2018-12-07 04:48:08'),('/contestacao',16,'2018-12-07 04:48:10'),('/configuracao/8',16,'2018-12-07 04:48:10'),('/Home',16,'2018-12-07 04:48:26'),('/contestacao',16,'2018-12-07 04:48:27'),('/novo-contestacao',16,'2018-12-07 04:48:34'),('/lista-contestacao',16,'2018-12-07 04:50:53'),('/Home',16,'2018-12-10 08:25:14'),('/contestacao',16,'2018-12-10 08:26:11'),('/lista-contestacao',16,'2018-12-10 08:26:17'),('/lista-contestacao',16,'2018-12-10 08:27:39'),('/indicadores-contestacao',16,'2018-12-10 08:28:56'),('/Home',1,'2018-12-10 01:24:40'),('/usuarios',1,'2018-12-10 01:24:53'),('/Home',1,'2018-12-10 01:25:27'),('/Home',1,'2018-12-10 01:25:30'),('/Home',16,'2018-12-10 04:14:46'),('/contestacao',16,'2018-12-10 04:14:48'),('/novo-contestacao',16,'2018-12-10 04:14:52'),('/lista-contestacao',16,'2018-12-10 04:14:54'),('/novo-contestacao',16,'2018-12-10 04:15:54'),('/lista-contestacao',16,'2018-12-10 04:35:48'),('/novo-contestacao',16,'2018-12-10 04:36:10'),('/Home',1,'2018-12-11 08:45:58'),('/usuarios',1,'2018-12-11 08:46:24'),('/Home',16,'2018-12-11 09:14:02'),('/contestacao',16,'2018-12-11 09:14:04'),('/lista-contestacao',16,'2018-12-11 09:14:06'),('/Home',1,'2018-12-11 09:27:15'),('/importacao',1,'2018-12-11 09:27:17'),('/importacao',1,'2018-12-11 09:27:20'),('/Home',16,'2018-12-11 03:35:23'),('/contestacao',16,'2018-12-11 03:37:14'),('/indicadores-contestacao',16,'2018-12-11 03:37:17'),('/lista-contestacao',16,'2018-12-11 03:37:18'),('/indicadores-contestacao',16,'2018-12-11 03:37:54'),('/Home',18,'2018-12-12 08:09:55'),('/contestacao',18,'2018-12-12 08:10:18'),('/novo-contestacao',18,'2018-12-12 08:14:19'),('/lista-contestacao',18,'2018-12-12 08:14:23'),('/novo-contestacao',18,'2018-12-12 08:15:25'),('/lista-contestacao',18,'2018-12-12 08:20:25'),('/Home',1,'2018-12-12 08:24:32'),('/Home',16,'2018-12-12 08:32:15'),('/contestacao',16,'2018-12-12 08:32:17'),('/lista-contestacao',16,'2018-12-12 08:32:20'),('/novo-contestacao',18,'2018-12-12 08:32:38'),('/lista-contestacao',18,'2018-12-12 08:36:29'),('/novo-contestacao',18,'2018-12-12 08:36:55'),('/lista-contestacao',18,'2018-12-12 08:36:56');
/*!40000 ALTER TABLE `auditoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `devname` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank`
--

LOCK TABLES `bank` WRITE;
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` VALUES (1,'BRADESCO','Bradesco'),(2,'PAN','Pan'),(3,'ITAU','Itau'),(4,'DAYCOVAL','Daycoval'),(5,'BANRISUL','BANCO DO ESTADO DO RIO GRANDE DO SUL'),(6,'BONSUCESSO','Bonsucesso'),(8,'SAFRA','Safra'),(9,'BMG','BMG'),(10,'BANRISUL','Banrisul'),(11,'CCB','BANCO DA CHINA BRASIL S.A'),(12,'FINASA BMC','BANCO FINASA BMC S.A'),(13,'BANRISUL','Unicred Central do Rio Grande do Sul'),(14,'SABEMI','SABEMI'),(15,'ITAU','BANCO ITABANCO S.A'),(16,'CCB','CCB'),(17,'BRADESCO','FINASA BMC');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_user`
--

DROP TABLE IF EXISTS `card_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_card` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_user`
--

LOCK TABLES `card_user` WRITE;
/*!40000 ALTER TABLE `card_user` DISABLE KEYS */;
INSERT INTO `card_user` VALUES (1,10,1),(2,10,2),(3,10,3),(4,10,4),(25,11,1),(26,11,2),(27,11,3),(28,11,4),(29,17,8),(30,18,8);
/*!40000 ALTER TABLE `card_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `text` varchar(250) NOT NULL,
  `img` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (1,'Análise Documental','Passo inicial, para uma FORMALIZAÇÃO  menos vulnerável a Fraudes, com vistas a EXECELÊNCIA na  PRODUÇÃO.','analise-documental.png','/analise-documental'),(2,'Formalização','Conjunto de processos, que tem por objetivo a minimização dos RISCOS e maximização dos RESULTADOS.','formalizacao.jpg','/home-formalizacao'),(3,'SDE','Atender as demandas internas e externas, no que tange a criação, exclusão, e manutenção dos usuários.','sde.png','/home-sde'),(4,'Monitoramento','Acompanhamento das demandas geradas pelos BANCOS, na busca de  maximizar o indicador de conversibilidade da produção.','monitoramento.png','/home-monitoramento'),(5,'Fontes','Some quick example text to build on the card title and make up the bulk of the cards content.','fontes.jpg','/fonte'),(6,'Importação','Some quick example text to build on the card title and make up the bulk of the cards content.','importacao.png','/importacao'),(7,'Usuários','Some quick example text to build on the card title and make up the bulk of the cards content.','usuarios.jpg','/usuarios'),(8,'Contestações','Some quick example text to build on the card title and make up the bulk of the cards content..','contestacao.jpg','/contestacao');
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_secundario`
--

DROP TABLE IF EXISTS `cards_secundario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_secundario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_card` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `texto` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_secundario`
--

LOCK TABLES `cards_secundario` WRITE;
/*!40000 ALTER TABLE `cards_secundario` DISABLE KEYS */;
INSERT INTO `cards_secundario` VALUES (1,8,'Missão','Encaminhamento das contestações notificadas pelos bancos para todas as unidades envolvidas.'),(2,8,'Processos Internos','Orientação a fim de se melhorar a qualidade das operações.'),(3,8,'Atividades','Acompanhar, cobrar e orientar para a resolução das contestações.'),(4,8,'Ferramenta','Emails de notificações enviadas pelos bancos | BI dos Bancos.'),(7,1,'Missão','Contribuir na prevenção de fraudes na busca da produção com excelência'),(8,1,'Processos Internos','Roteiros operacionais análise documental'),(9,1,'Atividades','Análise (Harpia) | Reanálise (Harpia) | Liberação de Contratos | Cancelamentos de Contratos'),(10,1,'Ferramenta','Harpia'),(11,2,'Missão','Minimizar os ricos das operações, buscando excelência na formalização da produção'),(12,2,'Resultados','Contribuir para a EXCELÊNCIA e RENTABILIDADE da produção, minimizando o prejuízo financeiro, devirado de fraudes e pendências'),(13,2,'Processos Internos','Fomentar o hábito da leitura e aplicações dos ROs, estabelecer excelência na gestão do processo, aprimorar a comunicação interna e com as filiais'),(14,2,'Ferramenta','E-mail | Work Bank'),(15,3,'Missão','Criar, Habilitar e manter os usuários nos sistemas internos e externos (bancos) para colaboradores e parceiros'),(16,3,'Resultados','Contribuir para a melhoria e aperfeiçoamento da segurança da informação, excelência da produção, minimizando a vulnerabilidade de fraudadores do processo'),(17,3,'Processos Internos','Solicitação de criação manutenção dos usuários, liberação de senhas e permissões, bloqueio e desbloqueio de usuários'),(18,3,'Ferramenta','GLPI'),(19,4,'Missão','Encaminhamentos de consistências e/ou pendências sinalizadas pelo banco para unidades'),(20,4,'Processos Internos','Fomentar o hábito da leitura e aplicações dos ROs, estabelecer excelência na gestão do processo, aprimorar a comunicação interna e com as filiais'),(21,4,'Resultados','Acompanhar as consistências e/ou pendências geradas na digitação/produção contribuindo para melhoria do índice de conversão'),(22,4,'Ferramenta','E-mail - GLPI');
/*!40000 ALTER TABLE `cards_secundario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contestacao`
--

DROP TABLE IF EXISTS `contestacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contestacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banco` varchar(200) NOT NULL,
  `cliente` varchar(250) NOT NULL,
  `cpf` varchar(200) NOT NULL,
  `numero_contrato` int(11) NOT NULL,
  `valor_contrato` float NOT NULL,
  `unidade` varchar(200) NOT NULL,
  `uf` varchar(10) NOT NULL,
  `agente` varchar(200) NOT NULL,
  `nome_agente` varchar(250) NOT NULL,
  `date` datetime NOT NULL,
  `situacao` int(11) NOT NULL,
  `tipo` varchar(250) NOT NULL,
  `observacao` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contestacao`
--

LOCK TABLES `contestacao` WRITE;
/*!40000 ALTER TABLE `contestacao` DISABLE KEYS */;
INSERT INTO `contestacao` VALUES (1,'BMG','VERA LUCIA CORREIA CARDOSO','383.186.607-49',50311191,1198,'RJ RIO DE JANEIRO','RJ','corretor','19871 AZEVEDO LTDA* - 19871','2018-11-21 00:00:00',1,'Contestacao',NULL),(2,'DAYCOVAL','CARLA DOS SANTOS PALUMBO','074.479.107-32',807631580,24377,'RJ RIO DE JANEIRO','RJ','corretor','','2018-11-21 00:00:00',2,'Contestacao',NULL),(3,'BANRISUL','ELIZABETH DOMINGUES DE SOUZA','420.191.766-53',5831924,22416,'MG BELO HORIZONTE','MG','corretor','20053 ELAINE REIS**','2018-11-21 00:00:00',2,'Contestacao',NULL),(4,'BANRISUL','IVANILDO BELO DO NASCIMENTO','142.233.174-15',5779256,3800,'PE RECIFE','PE','corretor','20267 DANIELE FIGUEIREDO','2018-11-21 00:00:00',2,'Contestacao',NULL),(5,'BANRISUL','VERA LUCIA G DOS SANTOS','078.261.414-00',6220439,6721.52,'PE RECIFE','PE','corretor','17534 FACILITY CADASTRAIS','2018-11-21 00:00:00',2,'Documental',NULL),(6,'ITAU','VICENTE DE PAULO RODRIGUES','691.873.713-91',29690614,538.12,'CE ITAPIPOCA - HELP','CE','corretor','153 FRANCISCO AVELINO','2018-11-21 00:00:00',2,'Documental',NULL),(7,'ITAU','VICENTE DE PAULO RODRIGUES','691.873.713-91',26299061,250.59,'CE ITAPIPOCA - HELP','CE','balcao','','2018-11-21 00:00:00',2,'Documental',NULL),(8,'BONSUCESSO','EDNER MOREIRA','414.587.617-20',858237975,20532.2,'RJ RIO DE JANEIRO','RJ','corretor','20147 CLAYTON ME**','2018-11-21 00:00:00',2,'Contestacao',NULL),(9,'BONSUCESSO','EDILMA ARIMATEA DA SILVA','343.969.691-04',852293955,7426.53,'DF BRASILIA ANHANGUERA','DF','balcao','','2018-11-21 00:00:00',1,'Documental',NULL),(10,'BMG','BERENICE DE ALMEIDA LIMA','174.108.352-49',1041131,1320.62,'PA BELÉM','PA','corretor','434 MARTA MENEZES**','2018-11-21 00:00:00',2,'Contestacao',NULL),(11,'ITAU','BERENICE DE ALMEIDA LIMA','174.108.352-49',4361732,3715.64,'PA BELÉM','PA','corretor','434 MARTA MENEZES**','2018-11-21 00:00:00',2,'Contestacao',NULL),(12,'ITAU','BERENICE DE ALMEIDA LIMA','174.108.352-49',14459149,1222.82,'PA BELÉM','PA','corretor','434 MARTA MENEZES**','2018-11-21 00:00:00',2,'Contestacao',NULL),(13,'ITAU','TANIA MARIA SANTANA MARQUES','559.751.195-53',13550888,340.04,'BA ED STATUS - SALVADOR ( COMERCIO CORRETOR )','BA','corretor','1452 ADEMILDES FERREIRA','2018-11-21 00:00:00',2,'Contestacao',NULL),(14,'BMG','JOSE GILMAR B DO NASCIMENTO','956.918.711-53',41338169,752.63,'GO GOIANIA','GO','corretor','5334 UOALLACE CHARLES','2018-11-21 00:00:00',2,'Contestacao',NULL),(15,'BMG','SEBASTIAO DE ALMEIDA SOUZA','389.744.957-91',50907542,4038.45,'RJ RIO DE JANEIRO','RJ','corretor','19394 UNIX NEGOCIOS','2018-11-21 00:00:00',2,'Contestacao',NULL),(16,'BMG','ARLEI CALDAS DE FARIAS','571.201.967-15',49448305,6108,'RJ RIO DE JANEIRO','RJ','corretor','19391 ALEXANDRE SIMOES ID[19391]','2018-11-22 00:00:00',2,'Contestacao',NULL),(17,'ITAU','RAIMUNDO MOURA DA SILVA FILHO','972.389.533-15',1144759,954,'PI LOJA BARROSO','PI','corretor','12900 ANTONIA MIRIAM DO NASCIMENTO ID[12900] CAMPO MAIOR','2018-11-22 00:00:00',2,'Contestacao',NULL),(18,'ITAU','MARIA LUCIA DA SILVA','600.048.043-17',1719065,5002,'PI LOJA BARROSO','PI','corretor','14561 CARLOS CESAR RIBEIRO DA LUZ* ID[14561] CAMPO MAIOR','2018-11-22 00:00:00',2,'Contestacao',NULL),(19,'ITAU','JOSEFA MARIA DA SILVA','255.532.594-87',8529963,1232,'AL MACEIO','AL','balcao','','2018-11-22 00:00:00',2,'Contestacao',NULL),(20,'ITAU','ALMADA FERNANDES CRUZ','067.802.103-15',9863025,669,'CE ITAPIPOCA - HELP','CE','balcao','','2018-11-22 00:00:00',2,'Contestacao',NULL),(21,'ITAU','ALZIRA RIBEIRO PINTO','166.726.822-87',19633219,2077,'PA BELÉM','PA','corretor','16587 L P A BOTELHO - ME ID[16587]','2018-11-22 00:00:00',1,'Contestacao',NULL),(22,'ITAU','ALZIRA RIBEIRO PINTO','166.726.822-87',19633373,2384,'PA BELÉM','PA','corretor','16587 L P A BOTELHO - ME ID[16587]','2018-11-22 00:00:00',1,'Contestacao',NULL),(23,'ITAU','MANOEL DE JESUS MONTEIRO DA SI','305.258.963-68',20948212,3911,'PI LOJA TERESINA','PI','corretor','12814 CELIO ERNANE RIBEIRO DA SILVA','2018-11-22 00:00:00',1,'Documental',NULL),(24,'DAYCOVAL','MARCOS ANTONIO BEZERRA MARQUES','785.029.187-53',807624804,63903.8,'RJ RIO DE JANEIRO','RJ','corretor','20601 FLP PROMOTORA DE VENDAS LTDA ID[20601]','2018-11-22 00:00:00',2,'Contestacao',NULL),(25,'BANRISUL','JOAO VITORINO SANTOS','378.996.786-68',5594731,8505,'MG BELO HORIZONTE','MG','corretor','19326 KETULI FERNANDES DE OLIVEIRA ID[19326]','2018-11-22 00:00:00',2,'Contestacao','AUDIENCIA'),(26,'PAN','JOSE FRANCISCO DE LIMA CLEMENTINO','256.606.673-68',322112710,5986,'AL MACEIO','CE','balcao','','2018-11-23 00:00:00',2,'Contestacao',NULL),(27,'BMG','ARNALDO EDUARDO DE LIMA FILHO','108.276.504-00',47024606,2000,'RN PARNAMIRIM','RN','corretor','17888 JAYNE FIDELIS','2018-11-23 00:00:00',2,'Contestacao',NULL),(28,'BMG','DENNER PEREIRA','582.001.697-15',52315827,3091,'RJ RIO DE JANEIRO','RJ','corretor','19871 AZEVEDO LTDA* - 19871','2018-11-23 00:00:00',1,'Contestacao',NULL),(29,'DAYCOVAL','EDNEUZA BRITO MARQUES','740.227.252-49',807615688,6760,'AM MANAUS GETULIO VARGAS - HELP','AM','corretor','16817 C ME - 16817','2018-11-23 00:00:00',1,'Documental',NULL),(30,'BMG','PAULO FERREIRA DE LIMA','209.416.844-15',31937044,355,'AL MACEIO','AL','corretor','3667 MEYRE SILVA*','2018-11-23 00:00:00',1,'Documental','O Contrato descrito abaixo foi entregue, porém identificamos que falta a documentação pessoal do cliente completa e legível.'),(31,'ITAU','LUCIA CIRQUEIRA N DOS SANTOS','398.561.861-53',21645273,21811,'DF BRASILIA ANHANGUERA','DF','corretor','6909 JOSE SANTOS - 6909','2018-11-23 00:00:00',1,'Contestacao',NULL),(32,'BMG','LAUDECI LOPES DE OLIVEIRA','594.104.776-20',39892040,4060,'MG BELO HORIZONTE','MG','corretor','15566 MG LTDA','2018-11-27 13:27:39',2,'Contestacao',NULL),(33,'ITAU','GERSON ULISSES DA SILVA','285.302.414-87',23012129,7555.29,'DF BRASILIA','DF','balcao','','2018-11-27 13:36:58',2,'Documental','ENVIAR -  kit contratação (CCB completa devidamente assinada + documentos pessoais)'),(36,'BANRISUL','MARIA LUCIA DA CUNHA PEREIRA','310.229.363-87',5940551,3764.14,'CE CALL CENTER VENDAS','CE','balcao','','2018-11-28 12:32:00',2,'Documental','Pergunta: Documento de Identificação está conforme as regras ? Resposta: Não Motivo: Incompleto'),(37,'ITAU','FRANCISCO DE ASSIS OLIVEIRA','213.079.464-53',21940269,17.1,'PE RECIFE','PE','corretor','17534 FACILITY CADASTRAIS - 17534','2018-11-28 14:11:05',2,'Contestacao',NULL),(38,'ITAU','FRANCISCO DE ASSIS OLIVEIRA','213.079.464-53',24086730,9028.18,'PE RECIFE','PE','corretor','17534 FACILITY CADASTRAIS - 17534','2018-11-28 14:16:35',1,'Contestacao',NULL),(39,'ITAU','MARIA DAS GRACAS BRITO FEITOSA','180.146.403-06',28226297,783.39,'CE ALVORADA','CE','corretor','19891 APROVA LTDA** - 19891','2018-11-28 14:37:29',2,'Contestacao',NULL),(40,'ITAU','MARIA DAS GRACAS BRITO FEITOSA','180.146.403-06',27341664,783.39,'CE ALVORADA','CE','corretor','19891 APROVA LTDA** - 19891','2018-11-28 14:43:12',2,'Contestacao',NULL),(41,'ITAU','ANTONIO DOS SANTOS NUNES','058.146.002-25',17905510,61.78,'PA BELÉM','PA','corretor','3521 ELIZANGELA LEONES* - 3521','2018-11-28 14:49:37',2,'Contestacao',NULL),(42,'BONSUCESSO','ROMULO TAVARES DA SILVA','416.390.901-00',76874020,1488.38,'DF BRASILIA ANHANGUERA','DF','corretor','9620 ELAINE SILVA - 9620','2018-11-28 15:00:26',2,'Contestacao',NULL),(43,'ITAU','ANTONIO PEREIRA DE OLIVEIRA','450.483.806-06',27057213,286.1,'MG BETIM REGIAO CENTRAL','MG','corretor','20131 PEDRO AZEVEDO** - 20131','2018-11-28 15:02:49',1,'Documental','REGULARIZAR:  kit contratação (CCB completa devidamente assinada + documentos pessoais)'),(44,'ITAU','MARIA JOSE BATISTA','625.091.797-72',20382775,12430,'SE ARACAJU CENTRO','SE','balcao','','2018-11-28 15:08:13',2,'Documental','REGULARIZAR : falta a documentação pessoal do cliente completa e legível e a CCB completa e legível'),(45,'BRADESCO','ANTONIO FERNANDES DE SOUSA','182.140.963-91',797195661,798.43,'PI LOJA BARROSO','PI','corretor','2015 RAIMUNDO * - 2015','2018-11-28 15:13:23',2,'Contestacao',NULL),(46,'ITAU','CLAUDIO MACEDO FURTADO','976.239.702-91',18180222,281,'PA BELÉM','PA','corretor','10398 JOSICLEIA FARIAS** - 10398','2018-11-28 15:15:22',2,'Contestacao',NULL),(47,'ITAU','ANTONIO DOS SANTOS NUNES','058.146.002-25',14742982,3838.61,'PA BELÉM','PA','corretor','3521 ELIZANGELA LEONES* - 3521','2018-11-28 15:16:39',2,'Contestacao',NULL),(48,'BMG','LUCINEIDE FERREIRA DA SILVA','013.698.553-03',35374663,5934.59,'PI LOJA BARROSO','PI','corretor','7539 IRANI ** - 7539','2018-11-28 15:19:22',2,'Documental','REGULARIZAR: falta a documentação pessoal do cliente completa e legível.'),(49,'ITAU','ADELAIDE PEREIRA DOS SANTOS','744.538.823-00',27530140,413.48,'PI LOJA PARNAIBA','PI','corretor','2285 MARIA SANTOS - 2285','2018-11-29 11:51:00',2,'Documental',NULL),(50,'ITAU','DAMIANA DE ARAUJO SILVA','603.560.571-00',25871571,299.9,'RJ RIO DE JANEIRO','RJ','corretor','19447 SLC NEGOCIOS - 19447','2018-11-29 12:24:35',2,'Contestacao',NULL),(51,'ITAU','JOSEFA MARIA DA SILVA','255.532.594-87',8529104,1196.96,'AL MACEIO','AL','balcao','','2018-11-29 12:27:49',2,'Contestacao',NULL),(52,'ITAU','JOSEFA MARIA DA SILVA','255.532.594-87',8529567,901.76,'AL MACEIO','AL','balcao','','2018-11-29 12:28:51',2,'Contestacao',NULL),(53,'BANRISUL','JOAO BATISTA ALVES DE MELO','189.243.874-72',6304218,7642.41,'PE RECIFE','PE','balcao','','2018-11-30 08:26:36',1,'Documental','REGULARIZAR: Kit completo. \nCumpre-nos esclarecer que caso não seja enviado a Cédula de Crédito Bancária em até 02 (dois) dias corridos, a operação será cancelada.'),(54,'BANRISUL','CECILIA MARIA RODRIGUES','120.659.513-20',5447589,8996.24,'CE CALL CENTER VENDAS','CE','balcao','','2018-11-30 08:30:02',2,'Contestacao',NULL),(55,'PAN','JOSE DE SOUSA','003.993.173-09',310138394,884.67,'PI LOJA BARROSO','PI','balcao','','2018-11-30 08:57:49',2,'Documental','REGULARIZAR: Kit completo + documentação imediatamente.'),(56,'BONSUCESSO','SONIA DE LOURDES REIS PEREIRA','167.744.932-20',858352920,22305.4,'CE ITAPIPOCA - HELP','CE','corretor','19891 APROVA LTDA** - 19891','2018-11-30 09:10:09',1,'Contestacao',NULL),(57,'BONSUCESSO','GENALDO LOPES NOBREGA','399.080.031-00',859457503,8142.53,'DF BRASILIA ANHANGUERA','DF','corretor','16374 - CLÁUDIA CRISTINA DOS SANTOS CARDOSO ID[16374]','2018-12-03 11:00:52',1,'Documental','solicitamos com a máxima urgência o envio da imagem de toda a documentação do (s) contrato'),(58,'DAYCOVAL','CLAUDIOMAR PINHEIRO REIS','159.987.382-68',807360236,4782.27,'AM MANAUS GETULIO VARGAS','AM','balcao','','2018-12-03 11:42:32',1,'Documental','REGULARIZAR: Kit contrato completo.'),(59,'BRADESCO','MARIA LUCIA CAMARA FONSECA','411.285.672-53',810117222,7269.22,'PA BELÉM','PA','corretor','7676 INAIARA CUNHA** - 7676','2018-12-03 12:16:07',1,'Documental','REGULARIZAR: Kit completo'),(60,'DAYCOVAL','JULIO FERNANDES DE OLIVEIRA','946.529.968-53',806297813,7333.53,'BA ITAPOÃ','BA','balcao','','2018-12-03 12:28:45',1,'Documental','Kit completo'),(61,'ITAU','MANOEL ALVES DO NASCIMENTO','368.069.433-49',23708635,3048.29,'CE SOBRAL','CE','balcao','','2018-12-03 12:35:35',2,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante.'),(62,'ITAU','MARIA CICERA GOMES DA SILVA','023.093.594-08',11023340,236.3,'PE CALLCENTER RECIFE','PE','balcao','','2018-12-03 12:42:14',2,'Documental','REGULARIZAR:\n\n-  falta a documentação pessoal do cliente completa e legível'),(63,'BONSUCESSO','CLOVIS CARVALHO BRITTO','892.070.841-04',859105289,25000.1,'DF BRASILIA ANHANGUERA','DF','corretor','5519 JOAO ALVES* - 5519','2018-12-04 10:10:43',1,'Contestacao','- CLIENTE ENTRA EM CONTATO COM O BANCO INFORMANDO QUE NÃO CONTRATOU EMPRÉSTIMO , O MESMO SÓ FICOU SABENDO PORQUE VIU NO CONTRACHEQUE. O MESMO DESEJA O CANCELAMENTO DO CONTRATO E SE OCORRER ALGUM DESCONTO O REEMBOLSO.\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante.'),(64,'ITAU','SEBASTIANA BELO E SILVA','154.710.983-15',13370271,263.9,'CE ALVORADA','CE','corretor','1296 JOAO SILVA* - 1296','2018-12-04 14:21:30',2,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante.'),(65,'BANRISUL','FIRMINA PEREIRA DO CARMO','607.909.706-00',5529466,4593.35,'MG BELO HORIZONTE','MG','corretor','18767 ARACELI BARBARO - 18767','2018-12-04 14:26:01',2,'Contestacao','Banco Banrisul está recebendo reclamação do(a) cliente abaixo identificado, alegando que não realizou a operação.\n\nCaso tenham alguma informação que possa nos auxiliar na defesa junto ao demandante, solicitamos que entrem em contato com o Gerente Comercial de seu relacionamento junto à esta empresa no prazo de 24 horas.'),(66,'BANRISUL','INES DE SOUSA LAURIANO','426.744.503-63',5653468,7583.54,'CE CALL CENTER VENDAS','CE','balcao','','2018-12-04 14:31:55',2,'Contestacao','Banco Banrisul está recebendo reclamação do(a) cliente abaixo identificado, alegando que não realizou a operação.\n\n        Caso tenham alguma informação que possa nos auxiliar na defesa junto ao demandante, solicitamos que entrem em contato com o Gerente Comercial de seu relacionamento junto à esta empresa no prazo de 24 horas.'),(67,'BANRISUL','INES DE SOUSA LAURIANO','426.744.503-63',5801253,8341.69,'CE CALL CENTER VENDAS','CE','balcao','','2018-12-04 14:33:56',2,'Contestacao','Banco Banrisul está recebendo reclamação do(a) cliente abaixo identificado, alegando que não realizou a operação.\n\nCaso tenham alguma informação que possa nos auxiliar na defesa junto ao demandante, solicitamos que entrem em contato com o Gerente Comercial de seu relacionamento junto à esta empresa no prazo de 24 horas.'),(68,'ITAU','ANETE FERREIRA EVAGELISTA','226.007.874-53',3933538,217.1,'PB CAMPINA GRANDE','PB','balcao','','2018-12-04 15:20:43',1,'Documental','REGULARIZAR:\n\nidentificamos que falta a documentação pessoal do cliente completa e legível.'),(69,'ITAU','TELVINA DE MENESES GUIMARAES','928.851.441-49',24331723,6553.72,'TO PALMAS','TO','corretor','20073 LORRANNY CARVALHO** - 20073','2018-12-04 15:27:13',1,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante.'),(70,'BMG','MANUEL FERREIRA DE SOUZA','297.607.717-72',50371235,4792.75,'RJ RIO DE JANEIRO (BUENOS AIRES)','RJ','corretor','19871 AZEVEDO LTDA* - 19871','2018-12-04 15:56:19',2,'Contestacao','Nova ação judicial foi cadastrada favor nos enviar subsídios para que seja possível realizar a defesa da ação em até 2 dias úteis.'),(71,'ITAU','DILCILENE PUREZA DUTRA','746.697.882-72',21162412,830.63,'AP MACAPA','AP','corretor','18118 J ME - 18118','2018-12-05 10:14:19',2,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante.'),(72,'BONSUCESSO','MARIA DAS MERCES CARLOS PONTES','612.735.894-91',853872860,1000,'PB CAMPINA GRANDE','PB','balcao','','2018-12-05 10:22:19',2,'Documental','- Uma vez que a Olé Consignado foi acionada pelo cliente a seguir e, ao realizar a busca do contrato para elaboração da defesa, foi identificado que o mesmo ainda não foi entregue, solicitamos com a máxima urgência o envio da imagem de toda a documentação do (s) contrato.'),(73,'ITAU','NEUSA MACHADO DA SILVA','554.054.013-15',22442296,2101.72,'PI LOJA BARROSO','PI','corretor','7979 DIEGO TERESINA) - 7979','2018-12-05 11:09:37',2,'Documental','REGULARIZAR:\n\nkit contratação (CCB completa devidamente assinada + documentos pessoais)'),(74,'BMG','AIRTON PEREIRA SIQUEIRA','770.822.497-72',50603647,2913.65,'RJ RIO DE JANEIRO (BUENOS AIRES)','RJ','corretor','19871 AZEVEDO LTDA* - 19871','2018-12-05 11:09:39',2,'Contestacao','Nova ação judicial foi cadastrada favor nos enviar subsídios para que seja possível realizar a defesa da ação em até 2 dias úteis.'),(75,'ITAU','FRANCISCA IVANDILA FERNANDES','680.032.503-15',4753365,1252.44,'CE IGUATU - HELP','CE','corretor','9174 PATRICIA NASCIMENTO* - 9174','2018-12-05 11:21:15',2,'Documental','REGULARIZAR:\n\nfalta a documentação pessoal do cliente e rogo completa e legível.'),(76,'ITAU','TEODORO LINO BARBOSA','181.474.703-63',823460,5000,'PI LOJA PIRIPIRI','PI','corretor','10518 FRANCISCO PEREIRA* - 10518','2018-12-05 11:53:34',2,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o Setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante, favor enviar.'),(77,'BMG','EDITE CARVALHO DOS SANTOS','195.527.775-34',190700875,583.01,'BA SALVADOR - HELP','BA','corretor','MARCIO RODRIGUES - 3568','2018-12-05 14:36:32',2,'Contestacao','Nova ação judicial foi cadastrada favor nos enviar subsídios para que seja possível realizar a defesa da ação em até 2 dias úteis.'),(78,'BMG','ANTONIO GOMES','113.009.225-91',52455367,56.14,'BA SALVADOR - HELP','BA','balcao','','2018-12-05 16:45:46',1,'Documental','Caso o kit contratação ainda não tenha sido enviado, solicitamos que em até 48hrs, nos forneça a devida formalização desta operação(formulários devidamente assinados mais documentos pessoais ou gravação da venda). Caso a contratação tenha sido feita pelos meios de formalização digitalizada ou eletrônica, o envio da documentação não será necessária.'),(79,'BMG','LAURINDO BATISTA COSTA','221.694.156-53',41412383,3680.87,'SP SAO PAULO','SP','corretor','17351 R. ME','2018-12-06 09:10:34',2,'Contestacao','Nova ação judicial foi cadastrada favor nos enviar subsídios para que seja possível realizar a defesa da ação em até 2 dias úteis.'),(80,'BMG','BALBINA GOMES RIBEIRO ALMEIDA','935.236.706-59',53744721,1220,'RJ RIO DE JANEIRO (BUENOS AIRES)','RJ','corretor','20076 WANDERLEY EIRELI','2018-12-06 16:40:37',1,'Contestacao','Caso o kit contratação ainda não tenha sido enviado, solicitamos que em até 48 hrs,nos forneça a devida formalização desta operação (formulários devidamente assinados mais documentos pessoais ou gravação da venda).'),(81,'BRADESCO','MARIA DA PAZ DE CARVALHO','026.479.934-83',810746414,9801,'PE RECIFE','PE','balcao','','2018-12-06 18:27:37',2,'Contestacao','Recebemos, reclamação do cliente em referência, cujo relato exige análise da operação de empréstimo consignado.\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o Setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante, favor enviar.'),(82,'BRADESCO','GILVAN DE JESUS SANTOS','201.998.645-00',2147483647,3585.93,'SE ARACAJU','SE','balcao','','2018-12-06 18:32:58',2,'Documental','REGULARIZAR: Kit contrato completo. Caso recepcionado ao banco, enviar protocolo urgente. \n\nEm razão da ausência de formalização do contrato em referência, estamos procedendo a sua liquidação/baixa e os prejuízos financeiros serão lançados em valores a recuperar de responsabilidade dessa empresa.'),(83,'ITAU','PAULO HENRIQUE FERRAZ LIMA','038.013.544-23',573830968,334.34,'SP SAO PAULO','SP','corretor','18762 PONTO NEGOCIOS - 18762','2018-12-07 12:06:27',0,'Contestacao','Cliente informa que recebeu uma proposta de portabilidade através de supostos agentes da loja (nomes não informados), mas foi cadastrado um contrato novo.\n\nA partir dos fatos expostos acima e cientes de que não há evidências suficientes do envolvimento de agentes cadastrados pelo banco nesta negociação, ressaltamos que esta instituição não aplica regras diferentes das estabelecidas por órgãos reguladores e convênios.\n\nAGUARDAMOS  RETORNO.'),(84,'BANRISUL','LUZENIR CASTRO SOUSA','662.365.333-34',212147,17652.7,'CE CONJUNTO CEARA - HELP','CE','balcao','','2018-12-07 12:11:43',0,'Documental','Favor providenciar o envio da documentação (Kit Contrato + documentação completa e legível).\n\nCumpre-nos esclarecer que caso não seja enviado a Cédula de Crédito Bancária em até 02 (dois) dias corridos, a operação será cancelada.'),(85,'ITAU','MARIA DA CONCEICAO ALVES M -','138.612.044-87',12428687,2861.58,'PE CALLCENTER RECIFE','PE','balcao','','2018-12-07 12:16:14',0,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante.'),(86,'ITAU','TEODORO LINO BARBOSA','181.474.703-63',1739206,49.8,'PI LOJA PIRIPIRI','PI','balcao','','2018-12-07 12:18:39',0,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante.'),(87,'ITAU','DAMASCENO DA SILVA','446.687.322-49',23654461,2178.13,'RR BOA VISTA','RR','corretor','15547 DEIDE PEREIRA - 15547','2018-12-07 12:20:52',0,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante.'),(88,'ITAU','FRANCISCA IVANDILA FERNANDES','680.032.503-15',4753160,1278.83,'CE IGUATU - HELP','CE','corretor','9174 PATRICIA NASCIMENTO* - 9174','2018-12-07 12:24:28',0,'Documental','REGULARIZAR:\n\nFalta a documentação pessoal do rogo completa e legível.'),(89,'ITAU','ROSALINO MIRANDA CONCEICAO','174.468.215-15',16450603,1273.26,'BA COMERCIO','BA','balcao','','2018-12-07 12:27:43',0,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante.'),(90,'ITAU','RAIMUNDO COSTA','830.517.832-34',19945801,281,'PA BELÉM','PA','corretor','17355 S ME - 17355','2018-12-07 12:30:29',0,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante.'),(91,'ITAU','ANTONIO PEDRO DOS SANTOS','309.512.836-34',27724042,286.1,'MG BELO HORIZONTE','MG','corretor','19693 AMANDA CARDOSO - 19693','2018-12-07 12:32:04',0,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação extra que possa nos auxiliar junto ao demandante. - Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o setor de Qual'),(92,'ITAU','MANOEL DAMIAO DE ARAUJO','117.550.402-53',8774537,1147.14,'PA BELÉM','PA','corretor','10398 JOSICLEIA FARIAS** - 10398','2018-12-07 16:50:36',0,'Documental','REGULARIZAR:\n\nfalta a documentação pessoal do cliente completa e legível.\n\nÉ primordial que a documentação física seja encaminhada o mais breve possível para uma de nossas filiais.'),(93,'BANRISUL','LUZENIR CASTRO SOUSA','662.365.333-34',2121747,17652.7,'CE CONJUNTO CEARA','CE','balcao','','2018-12-10 11:17:07',0,'Documental','Viemos notificar-lhes que se faz necessário o encaminhamento do(s) documento(s) físico(s) relativos à(s) operação(ões) de empréstimo consignado.\n\nNo aguardo.'),(94,'ITAU','PEDRO DE CARVALHO','990.742.651-20',9719550,669.46,'TO PALMAS','TO','corretor','16033 JOSIVAL BARBOSA - 16033','2018-12-10 16:17:48',0,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o Setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação e/ou documentação extra que possa nos auxiliar junto ao demandante, favor enviar.'),(95,'ITAU','HONORIVAL FERNANDES DA SILVA','157.308.166-34',24574462,5972.16,'TO PALMAS','TO','corretor','19947 LUCIANA CARVALHO** - 19947','2018-12-10 17:13:08',0,'Contestacao','- Caso o kit contratação ainda não tenha sido enviado ao Banco, solicitamos o envio imediato das imagens legíveis (formulários devidamente assinados e documentos pessoais ou gravação da venda);\n\n- Entre em contato com o cliente a fim de entender o motivo que o mesmo abriu a reclamação contra o Banco e retorne contato com o Setor de Qualidade para informar qual ação será tomada. Caso tenha alguma informação e/ou documentação extra que possa nos auxiliar junto ao demandante, favor enviar.'),(96,'BMG','AIRTON DE ASSIS','451.241.917-91',35759974,157.3,'RJ CALL CENTER - BUENOS AIRES','RJ','balcao','','2018-12-12 08:19:02',0,'Contestacao','Nova ação judicial foi cadastrada favor nos enviar subsídios para que seja possível realizar a defesa da ação em até 2 dias úteis.'),(97,'BMG','RENATO MORAES GONÇALVES','056.797.157-08',46946260,1100,'RJ RIO DE JANEIRO (BUENOS AIRES)','RJ','corretor','9921 RT','2018-12-12 08:36:23',0,'Contestacao','Nova ação judicial foi cadastrada favor nos enviar subsídios para que seja possível realizar a defesa da ação em até 2 dias úteis.');
/*!40000 ALTER TABLE `contestacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract_attribute_column_numbers`
--

DROP TABLE IF EXISTS `contract_attribute_column_numbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract_attribute_column_numbers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(255) DEFAULT NULL,
  `column_number` int(11) DEFAULT NULL,
  `datasource_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_contract_attribute_column_numbers_on_datasource_id` (`datasource_id`),
  CONSTRAINT `fk_rails_5ebd1666f9` FOREIGN KEY (`datasource_id`) REFERENCES `datasources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract_attribute_column_numbers`
--

LOCK TABLES `contract_attribute_column_numbers` WRITE;
/*!40000 ALTER TABLE `contract_attribute_column_numbers` DISABLE KEYS */;
INSERT INTO `contract_attribute_column_numbers` VALUES (12,'number',10,3),(13,'client_cpf',3,3),(14,'bank',9,3),(15,'uf',11,3),(16,'company_unit',0,3),(17,'state',8,3),(18,'date',0,3),(19,'operation',0,3),(20,'value',5,3),(21,'partnership',0,3),(22,'updated_at',7,3),(23,'number',5,4),(24,'client_cpf',8,4),(25,'bank',7,4),(26,'uf',6,4),(27,'company_unit',3,4),(28,'state',11,4),(29,'date',4,4),(30,'operation',13,4),(31,'value',10,4),(32,'partnership',12,4),(33,'updated_at',11,4),(34,'id_chamado',1,5),(35,'data_abertura',3,5),(36,'uf',4,5),(37,'unidade',5,5),(38,'subcategoria',10,5),(39,'categoria',8,5),(40,'data_fechamento',11,5),(41,'number',15,6),(42,'pendency',8,6),(43,'date',25,6),(44,'number',1,7),(45,'pendency',24,NULL),(46,'date',20,7),(47,'number',13,8),(48,'date',0,8),(49,'pendency',0,8),(50,'number',2,9),(51,'pendency',23,9),(52,'date',1,9),(53,'number',9,10),(54,'pendency',11,10),(55,'date',7,10),(56,'number',6,11),(57,'pendency',18,11),(58,'date',11,11),(59,'number',3,12),(60,'pendency',7,12),(61,'date',2,12),(62,'pendency',20,7),(63,'titulo',2,5),(64,'tipo_unidade',6,5),(65,'number',14,14),(66,'number',4,13),(67,'pendency',11,13),(68,'date',6,13),(69,'uf',5,11),(70,'uf',11,22),(71,'number',13,22),(72,'date',8,22),(73,'pendency',14,22),(74,'uf',3,10),(75,'uf',13,6),(76,'uf',4,7),(77,'uf',8,8),(78,'uf',8,14),(79,'uf',0,13),(80,'uf',0,12);
/*!40000 ALTER TABLE `contract_attribute_column_numbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contracts`
--

DROP TABLE IF EXISTS `contracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contracts` (
  `id` bigint(20) NOT NULL,
  `number` varchar(255) DEFAULT NULL,
  `client_cpf` varchar(255) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `devname_bank` varchar(255) DEFAULT NULL,
  `uf` varchar(255) DEFAULT NULL,
  `company_unit` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT '0',
  `date` date DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `partnership` varchar(255) DEFAULT NULL,
  `devname_partnership` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `contract_import_id` bigint(20) DEFAULT NULL,
  `pendency` varchar(100) DEFAULT '0',
  `date_pendency` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contracts`
--

LOCK TABLES `contracts` WRITE;
/*!40000 ALTER TABLE `contracts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contracts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_import`
--

DROP TABLE IF EXISTS `data_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_import` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `datasource_id` bigint(20) DEFAULT NULL,
  `import_file_path` text,
  `failed` tinyint(1) DEFAULT NULL,
  `err_message` varchar(255) DEFAULT NULL,
  `date_import` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_data_import_on_datasource_id` (`datasource_id`),
  CONSTRAINT `fk_rails_b268ec74ff` FOREIGN KEY (`datasource_id`) REFERENCES `datasources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_import`
--

LOCK TABLES `data_import` WRITE;
/*!40000 ALTER TABLE `data_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datasources`
--

DROP TABLE IF EXISTS `datasources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasources` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `import_file_data_delimiter` varchar(255) DEFAULT NULL,
  `header_lines` int(11) DEFAULT NULL,
  `devname` varchar(255) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datasources`
--

LOCK TABLES `datasources` WRITE;
/*!40000 ALTER TABLE `datasources` DISABLE KEYS */;
INSERT INTO `datasources` VALUES (3,'Harpia',';',1,'Harpia','harpia'),(4,'WorkBank',';',1,'WorkBank','workbank'),(5,'GLPI - SDE',';',1,'GLPI','glpi'),(6,'Pendência Olé',';',8,'Ole','pendencia'),(7,'Pendência Pan',';',1,'Pan','pendencia'),(8,'Pendência Banrisul',';',6,'Banrisul','pendencia'),(9,'Pendência Safra',';',1,'Safra','pendencia'),(10,'Pendência Itaú',';',2,'Itau','pendencia'),(11,'Pendência BMG',';',2,'BMG','pendencia'),(12,'Pendência Sabemi',';',1,'Sabemi','pendencia'),(13,'Pendência CCB',';',1,'CCB','pendencia'),(14,'Pendência Bradesco',';',1,'Bradesco','pendencia'),(15,'Monitoramento PAN',';',1,'PAN','monitoramento'),(16,'Monitoramento Ole',';',4,'Ole','monitoramento'),(17,'Monitoramento Daycoval',';',1,'Daycoval','monitoramento'),(18,'Monitoramento Bradesco',';',1,'Bradesco','monitoramento'),(19,'Monitoramento Banrisul',';',2,'Banrisul','monitoramento'),(20,'CADIOCONSIG DIGITADOR',';',2,'Ibconsig','monitoramento'),(21,'CADIOCONSIG CONTAS',';',2,'Ibconsig','monitoramento'),(22,'Pendencia Daycoval',';',1,'Daycoval','pendencia');
/*!40000 ALTER TABLE `datasources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_contestacao`
--

DROP TABLE IF EXISTS `email_contestacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_contestacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) NOT NULL,
  `id_contestacao` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_contestacao`
--

LOCK TABLES `email_contestacao` WRITE;
/*!40000 ALTER TABLE `email_contestacao` DISABLE KEYS */;
INSERT INTO `email_contestacao` VALUES (1,'annacarlla@grupocasebras.com.br',32),(2,'regis@grupocasebras.com.br',32),(3,'cidapimentel@grupocasebras.com.br',32),(7,'ANNACARLLA@GRUPOCASEBRAS.COM.BR',34),(8,'annacarlla@grupocasebras.com.br',35),(13,'Karine@promotorafacilite.com.br',37),(14,'danilomartins@grupocasebras.com.br;',37),(15,'brunaleticia <brunaleticia@padraodevida.com.br>;',37),(16,'Flávio Gomes. | Loja Recife\" <flaviosantana@padraodevida.com.br>;',37),(17,'qualidade@casebras.com.br;',37),(18,'Karine@promotorafacilite.com.br',38),(19,'danilomartins@grupocasebras.com.br',38),(20,'Flávio Gomes. | Loja Recife\" <flaviosantana@padraodevida.com.br>',38),(21,'brunaleticia <brunaleticia@padraodevida.com.br>',38),(22,'Marcella Lins | PE <marcella@padraodevida.com.br>',38),(23,'qualidade@casebras.com.br',38),(24,'luizmedeiros00@gmail.com',30),(25,'norzerlandio@padraodevida.com.br',39),(26,'loja.alvorada@padraodevida.com.br',39),(27,'qualidade@casebras.com.br',39),(28,'norzerlandio@padraodevida.com.br',40),(29,'loja.alvorada@padraodevida.com.br',40),(30,'qualidade@casebras.com.br',40),(31,'adrianaleal@padraodevida.com.br',41),(32,'emilycarla@padraodevida.com.br',41),(33,'edinaldomoraes@padraodevida.com.br',41),(34,'qualidade@casebras.com.br',41),(35,'deboraramos@grupocasebras.com.br',33),(36,'atendimentodf@padraodevida.com.br',33),(37,'ketelynpereira@padraodevida.com.br',33),(38,'qualidade@casebras.com.br',33),(39,'deboraramos@grupocasebras.com.br',42),(40,'atendimentodf@padraodevida.com.br',42),(41,'ketelynpereira@padraodevida.com.br',42),(42,'qualidade@casebras.com.br',42),(43,'lojatupi@padraodevida.com.br',43),(44,'lojabhte@padraodevida.com.br',43),(45,'rosalianeta@padraodevida.com.br',43),(46,'joanafreitas@grupocasebras.com.br',43),(47,'deboraramos@grupocasebras.com.br',43),(48,'elisangelamarinho@padraodevida.com.br',44),(49,'Erikafabiana@padraodevida.com.br',44),(50,'leandronunes@grupocasebras.com.br',44),(51,'talitaulisses@grupocasebras.com.br',45),(52,'kamilabrito@padraodevida.com.br',45),(53,'vivianesousa@padraodevida.com.br',45),(54,'emilycarla@padraodevida.com.br',46),(55,'adrianaleal@padraodevida.com.br',46),(56,'edinaldomoraes@padraodevida.com.br',46),(57,'reginavasconcelos@grupocasebras.com.br',46),(58,'adrianaleal@padraodevida.com.br',47),(59,'edinaldomoraes@padraodevida.com.br',47),(60,'emilycarla@padraodevida.com.br',47),(61,'reginavasconcelos@grupocasebras.com.br',47),(62,'kamilabrito@padraodevida.com.br',48),(63,'vivianesousa@padraodevida.com.br',48),(64,'talitaulisses@grupocasebras.com.br',48),(65,'qualidade@casebras.com.br',48),(66,'talitaulisses@grupocasebras.com.br',49),(67,'QUALIDADE@CASEBRAS.COM.BR',49),(68,'kamilabrito@padraodevida.com.br',49),(69,'vivianesousa@padraodevida.com.br',49),(70,'grazielesabino@padraodevida.com.br',50),(71,'marceloveloso@grupocasebras.com.br',50),(72,'suporterj3@padraodevida.com.br',50),(73,'gustavomiranda@padraodevida.com.br',50),(74,'paulafirmino@padraodevida.com.br',50),(75,'edgarjunior@padraodevida.com.br',51),(76,'wanessacalazans@padraodevida.com.br',51),(77,'eudesmartins@grupocasebras.com.br',51),(78,'QUALIDADE@CASEBRAS.COM.BR',51),(79,'eudesmartins@grupocasebras.com.br',52),(80,'edgarjunior@padraodevida.com.br',52),(81,'wanessacalazans@padraodevida.com.br',52),(82,'lojas.pernambuco@padraodevida.com.br',53),(83,'danilomartins@grupocasebras.com.br',53),(84,'marcella@padraodevida.com.br',53),(85,'brunaleticia@padraodevida.com.br',53),(86,'mara@grupocasebras.com.br',54),(87,'backofficecallcenterfortaleza@padraodevida.com.br',54),(88,'marciagabriela@padraodevida.com.br',54),(89,'lojas.piaui@padraodevida.com.br',55),(90,'kamilabrito@padraodevida.com.br',55),(91,'loja.teresina@padraodevida.com.br',55),(92,'loja.barroso@padraodevida.com.br',55),(93,'vivianesousa@padraodevida.com.br',55),(94,'talitaulisses@grupocasebras.com.br',55),(95,'loja.alvorada@padraodevida.com.br',56),(96,'alessandra@grupocasebras.com.br',56),(97,'norzerlandio@padraodevida.com.br',56),(98,'backofficecallcenterfortaleza@padraodevida.com.br;',36),(99,'Mara Lourenço <mara@grupocasebras.com.br>;',36),(100,'marciagabriela@padraodevida.com.br',36),(101,'qualidade@grupocasebras.com.br',36),(102,'QUALIDADE@CASEBRAS.COM.BR',58),(128,'lojas.paraiba@padraodevida.com.br',68),(129,'fabriciofernandes@grupocasebras.com.br',68),(130,'lucijanesilva@padraodevida.com.br',68),(131,'erikaranielle@padraodevida.com.br',68),(132,'givanildoviana@padraodevida.com.br',68),(133,'qualidade@casebras.com.br',68),(150,'reginavasconcelos@grupocasebras.com.br',71),(151,'mayararichelli@padraodevida.com.br',71),(152,'loja.macapa@padraodevida.com.',71),(153,'genilsonlima@padraodevida.com.br',71),(154,'qualidade@casebras.com.br',71),(155,'givanildoviana@padraodevida.com.br',72),(156,'erikaranielle@padraodevida.com.br',72),(157,'lucijanesilva@padraodevida.com.br',72),(158,'fabriciofernandes@grupocasebras.com.br',72),(159,'lojas.paraiba@padraodevida.com.br',72),(160,'qualidade@casebras.com.br',72),(161,'lojas.piaui@padraodevida.com.br',73),(162,'kamilabrito@padraodevida.com.br',73),(163,'loja.teresina@padraodevida.com.br',73),(164,'loja.barroso@padraodevida.com.br',73),(165,'vivianesousa@padraodevida.com.br',73),(166,'talitaulisses@grupocasebras.com.br',73),(167,'qualidade@casebras.com.br',73),(168,'gustavomiranda@padraodevida.com.br',74),(169,'marceloveloso@grupocasebras.com.br',74),(170,'grazielesabino@padraodevida.com.br',74),(171,'paulafirmino@padraodevida.com.br',74),(172,'qualidade@casebras.com.br',74),(173,'suporterj5@padraodevida.com.br',74),(174,'suporterj4@padraodevida.com.br',74),(175,'suporterj2@padraodevida.com.br',74),(176,'lojas.help@padraodevida.com.br',75),(177,'qualidade@casebras.com.br',75),(178,'elisiane@grupocasebras.com.br',75),(179,'itamar@grupocasebras.com.br',75),(180,'lojas.piaui@padraodevida.com.br',76),(181,'kamilabrito@padraodevida.com.br',76),(182,'loja.teresina@padraodevida.com.br',76),(183,'loja.barroso@padraodevida.com.br',76),(184,'vivianesousa@padraodevida.com.br',76),(185,'talitaulisses@grupocasebras.com.br',76),(186,'qualidade@casebras.com.br',76),(187,'QUALIDADE@CASEBRAS.COM.BR',61),(188,'lilianybarreto@padraodevida.com.br',61),(189,'glecio@grupocasebras.com.br',61),(190,'lojas.pernambuco@padraodevida.com.br',62),(191,'danilomartins@grupocasebras.com.br',62),(192,'marcella@padraodevida.com.br',62),(193,'brunaleticia@padraodevida.com.br',62),(194,'lucianofelipe@padraodevida.com.br',62),(195,'lojas.df@padraodevida.com.br',63),(196,'ketelynpereira@padraodevida.com.br',63),(197,'deboraramos@grupocasebras.com.br',63),(198,'qualidade@casebras.com.br',63),(199,'loja.alvorada@padraodevida.com.br',64),(200,'alessandra@grupocasebras.com.br',64),(201,'norzerlandio@padraodevida.com.br',64),(202,'qualidade@casebras.com.br',64),(203,'lojatupi@padraodevida.com.br',65),(204,'lojabhte@padraodevida.com.br',65),(205,'rosalianeta@padraodevida.com.br',65),(206,'joanafreitas@grupocasebras.com.br',65),(207,'qualidade@casebras.com.br',65),(208,'mara@grupocasebras.com.br',67),(209,'backofficecallcenterfortaleza@padraodevida.com.br',67),(210,'marciagabriela@padraodevida.com.br',67),(211,'qualidade@casebras.com.br',67),(212,'ranyellylucena@padraodevida.com.br',69),(213,'deboraramos@grupocasebras.com.br',69),(214,'qualidade@casebras.com.br',69),(215,'suporterj2@padraodevida.com.br',70),(216,'suporterj4@padraodevida.com.br',70),(217,'suporterj5@padraodevida.com.br',70),(218,'gustavomiranda@padraodevida.com.br',70),(219,'marceloveloso@grupocasebras.com.br',70),(220,'grazielesabino@padraodevida.com.br',70),(221,'paulafirmino@padraodevida.com.br',70),(222,'qualidade@casebras.com.br',70),(223,'lojas.bahia@padraodevida.com.br',77),(224,'elisangelamarinho@padraodevida.com.br',77),(225,'leandronunes@grupocasebras.com.br',77),(226,'deboraramos@grupocasebras.com.br',77),(227,'qualidade@casebras.com.br',77),(228,'elisangelamarinho@padraodevida.com.br',78),(229,'leandronunes@grupocasebras.com.br',78),(230,'deboraramos@grupocasebras.com.br',78),(231,'qualidade@casebras.com.br',78),(232,'suportelojasp@padraodevida.com.br',79),(233,'portabilidadesp@padraodevida.com.br',79),(234,'fagnervinicius@padraodevida.com.br',79),(235,'janainaalves@padraodevida.com.br',79),(236,'mariotuffi@grupocasebras.com.br',79),(237,'lojas.saopaulo@padraodevida.com.br',79),(238,'qualidade@casebras.com.br',79),(239,'gustavomiranda@padraodevida.com.br',80),(240,'marceloveloso@grupocasebras.com.br',80),(241,'grazielesabino@padraodevida.com.br',80),(242,'paulafirmino@padraodevida.com.br',80),(243,'qualidade@casebras.com.br',80),(244,'suporterj5@padraodevida.com.br',80),(245,'suporterj4@padraodevida.com.br',80),(246,'suporterj2@padraodevida.com.br',80),(247,'danilomartins@grupocasebras.com.br',81),(248,'marcella@padraodevida.com.br',81),(249,'lucianofelipe@padraodevida.com.br',81),(250,'brunaleticia@padraodevida.com.br',81),(251,'qualidade@casebras.com.br',81),(252,'edgarjunior@padraodevida.com.br',82),(253,'anaclaudia@padraodevida.com.br',82),(254,'qualidade@casebras.com.br',82),(255,'fagnervinicius@padraodevida.com.br',83),(256,'janainaalves@padraodevida.com.br',83),(257,'mariotuffi@grupocasebras.com.br',83),(258,'suportelojasp@padraodevida.com.br',83),(259,'qualidade@casebras.com.br',83),(260,'lilianybarreto@padraodevida.com.br',84),(261,'glecio@grupocasebras.com.br',84),(262,'qualidade@casebras.com.br',84),(263,'elisianealencar@grupocasebras.com.br',84),(264,'talitaulisses@grupocasebras.com.br',86),(265,'vivianesousa@padraodevida.com.br',86),(266,'loja.teresina@padraodevida.com.br',86),(267,'qualidade@casebras.com.br',86),(268,'kamilabrito@padraodevida.com.br',86),(269,'adrianasouza@padraodevida.com.br',87),(270,'loreley@casebras.com.br',87),(271,'loja.boavista@padraodevida.com.br',87),(272,'qualidade@casebras.com.br',87),(273,'lilianybarreto@padraodevida.com.br',88),(274,'qualidade@casebras.com.br',88),(275,'elisianealencar@grupocasebras.com.br',88),(276,'lojas.help@padraodevida.com.br',88),(277,'joselinecardoso@padraodevida.com.br',88),(278,'elisangelamarinho@padraodevida.com.br',89),(279,'leandronunes@grupocasebras.com.br',89),(280,'deboraramos@grupocasebras.com.br',89),(281,'qualidade@casebras.com.br',89),(282,'lojas.bahia@padraodevida.com.br',89),(283,'loja.belem@padraodevida.com.br',90),(284,'emilycarla@padraodevida.com.br',90),(285,'carolinacosta@padraodevida.com.br',90),(286,'reginavasconcelos@grupocasebras.com.br',90),(287,'qualidade@casebras.com.br',90),(288,'lojabhte@padraodevida.com.br',91),(289,'lojatupi@padraodevida.com.br',91),(290,'rosalianeta@padraodevida.com.br',91),(291,'joanafreitas@grupocasebras.com.br',91),(292,'qualidade@casebras.com.br',91),(293,'emilycarla@padraodevida.com.br',92),(294,'carolinacosta@padraodevida.com.br',92),(295,'reginavasconcelos@grupocasebras.com.br',92),(296,'qualidade@casebras.com.br',92),(300,'ranyellylucena@padraodevida.com.br',94),(301,'deboraramos@grupocasebras.com.br',94),(302,'qualidade@casebras.com.br',94),(303,'ranyellylucena@padraodevida.com.br',95),(304,'deboraramos@grupocasebras.com.br',95),(305,'qualidade@casebras.com.br',95),(306,'gustavomiranda@padraodevida.com.br',96),(307,'marceloveloso@grupocasebras.com.br',96),(308,'grazielesabino@padraodevida.com.br',96),(309,'paulafirmino@padraodevida.com.br',96),(310,'qualidade@casebras.com.br',96),(311,'suporterj5@padraodevida.com.br',96),(312,'suporterj4@padraodevida.com.br',96),(313,'suporterj2@padraodevida.com.br',96),(314,'lilianybarreto@padraodevida.com.br',93),(315,'glecio@grupocasebras.com.br',93),(316,'qualidade@casebras.com.br',93),(317,'suporterj2@padraodevida.com.br',97),(318,'suporterj4@padraodevida.com.br',97),(319,'suporterj5@padraodevida.com.br',97),(320,'gustavomiranda@padraodevida.com.br',97),(321,'marceloveloso@grupocasebras.com.br',97),(322,'grazielesabino@padraodevida.com.br',97),(323,'paulafirmino@padraodevida.com.br',97),(324,'qualidade@casebras.com.br',97);
/*!40000 ALTER TABLE `email_contestacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filiais`
--

DROP TABLE IF EXISTS `filiais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filiais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filial` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filiais`
--

LOCK TABLES `filiais` WRITE;
/*!40000 ALTER TABLE `filiais` DISABLE KEYS */;
INSERT INTO `filiais` VALUES (1,'AC'),(2,'AL'),(3,'AP'),(4,'AM'),(5,'BA'),(6,'CE'),(7,'DF'),(8,'ES'),(9,'GO'),(10,'MA'),(11,'MT'),(12,'MS'),(13,'MG'),(14,'PA'),(15,'PB'),(16,'PR'),(17,'PE'),(18,'PI'),(19,'RJ'),(20,'RN'),(21,'RS'),(22,'RO'),(23,'RR'),(24,'SC'),(25,'SP'),(26,'SE'),(27,'TO');
/*!40000 ALTER TABLE `filiais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filial_users`
--

DROP TABLE IF EXISTS `filial_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filial_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_filial` int(250) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filial_users`
--

LOCK TABLES `filial_users` WRITE;
/*!40000 ALTER TABLE `filial_users` DISABLE KEYS */;
INSERT INTO `filial_users` VALUES (15,4,11),(16,6,11),(17,14,11);
/*!40000 ALTER TABLE `filial_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glpi`
--

DROP TABLE IF EXISTS `glpi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glpi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(300) DEFAULT NULL,
  `id_chamado` varchar(11) DEFAULT NULL,
  `data_abertura` datetime DEFAULT NULL,
  `uf` varchar(100) DEFAULT NULL,
  `unidade` varchar(100) DEFAULT NULL,
  `tipo_unidade` varchar(100) DEFAULT NULL,
  `categoria` varchar(100) DEFAULT NULL,
  `subcategoria` varchar(100) DEFAULT NULL,
  `data_fechamento` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glpi`
--

LOCK TABLES `glpi` WRITE;
/*!40000 ALTER TABLE `glpi` DISABLE KEYS */;
/*!40000 ALTER TABLE `glpi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harpia`
--

DROP TABLE IF EXISTS `harpia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harpia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpf` varchar(200) DEFAULT NULL,
  `valor` decimal(5,2) DEFAULT NULL,
  `ultima_data` datetime DEFAULT NULL,
  `situacao` varchar(200) DEFAULT NULL,
  `banco` varchar(200) DEFAULT NULL,
  `devname_banco` varchar(200) DEFAULT NULL,
  `numero` int(200) DEFAULT NULL,
  `uf` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harpia`
--

LOCK TABLES `harpia` WRITE;
/*!40000 ALTER TABLE `harpia` DISABLE KEYS */;
/*!40000 ALTER TABLE `harpia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_contracts`
--

DROP TABLE IF EXISTS `import_contracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `data_import_id` int(11) NOT NULL,
  `pendency` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_contracts`
--

LOCK TABLES `import_contracts` WRITE;
/*!40000 ALTER TABLE `import_contracts` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_contracts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monitoramento`
--

DROP TABLE IF EXISTS `monitoramento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monitoramento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL,
  `value` double(5,2) DEFAULT NULL,
  `client_cpf` varchar(120) DEFAULT NULL,
  `operation` varchar(100) DEFAULT NULL,
  `uf` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `state_itau` varchar(120) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `bank` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monitoramento`
--

LOCK TABLES `monitoramento` WRITE;
/*!40000 ALTER TABLE `monitoramento` DISABLE KEYS */;
/*!40000 ALTER TABLE `monitoramento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partnership`
--

DROP TABLE IF EXISTS `partnership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partnership` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `devname` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partnership`
--

LOCK TABLES `partnership` WRITE;
/*!40000 ALTER TABLE `partnership` DISABLE KEYS */;
INSERT INTO `partnership` VALUES (1,'INSS','INSS'),(2,'AERONAUTICA','AERONAUTICA'),(3,'AMAZON','AMAZON'),(4,'BANRISUL','BANRISUL'),(5,'BBS','BBS'),(6,'BMG','BMG'),(7,'DAYCOVAL','DAYCOVAL'),(8,'EXERCITO','EXERCITO'),(9,'FEDERAL','FEDERAL'),(10,'GOV','GOVERNO'),(11,'IPREM','IPREM'),(12,'MARINHA','MARINHA'),(13,'PAN','PAN'),(14,'PM','PM'),(15,'PREF','PREFEITURA'),(16,'TJ','TJ');
/*!40000 ALTER TABLE `partnership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil`
--

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` VALUES (1,'Administrador'),(2,'Gerente'),(3,'Operador');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `id_perfil` int(11) DEFAULT NULL,
  `last_access` datetime DEFAULT NULL,
  `situacao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'luiz fabio lima de medeiros','luizmedeiros','luizmedeiros@casebras.com.br','$2y$10$.YukUHQHMV1xFFyahOyDtOHpPqlX7B/ix/HmpIVfbnSxb3csl5pdG',1,'2018-12-11 08:45:56',0),(14,'Mikaelly Moura','mikaelly','mikaellymoura@grupocasebras.com.br','$2y$10$u.9hrpUBV1TUYc06BXvo8uX080pBjLONsfIG4E6jHgLHgmbelzjjO',3,'2018-11-09 14:43:02',1),(15,'Fernando Ricardo Holanda Perdigão','Perdigão','fernandoperdigao@grupocasebras.com.br','$2y$10$ht.ooeigSRZeP8N9.MgcZOybYGHYmr/BtN3YpJ2jHfNcaCgleR5oq',1,'2018-10-30 15:48:32',1),(16,'Anna Carlla','annacarlla','annacarlla@grupocasebras.com.br','$2y$10$FVcAJqPxcQ/OGpElYFSthOk5GOA2F/U9v20tjv0HJRP7PpukwHeL6',1,'2018-12-12 08:32:13',0),(17,'ELISIANE FERREIRA DE ALENCAR FAUSTINO','ELISIANE.MATRIZ','elisiane@grupocasebras.com.br','$2y$10$GpeNniO0PbU2cXH.ubN.eeyu.2KPUxJaKaX9gj7qxAwhaaH9.sQXK',2,NULL,0),(18,'FRANCISCO ALGENOR NOGUEIRA OLIVEIRA FILHO','ALGENOR.MATRIZ','algenoroliveira@casebras.com.br','$2y$10$CQaaJ7PZzvPP0/cAujxzm.cxDJSsexPc4hDnlppAJjsaj3gO5jC4q',2,'2018-12-12 08:09:52',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-12  8:46:22
