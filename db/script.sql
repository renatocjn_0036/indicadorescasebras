-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05-Out-2018 às 22:32
-- Versão do servidor: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bi_casebras`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `devname` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `bank`
--

INSERT INTO `bank` (`id`, `devname`, `name`) VALUES
(1, 'BRADESCO', 'Bradesco'),
(2, 'PAN', 'Pan'),
(3, 'ITAU', 'Itau'),
(4, 'DAYCOVAL', 'Daycoval'),
(5, 'BANRISUL', 'BANCO DO ESTADO DO RIO GRANDE DO SUL'),
(6, 'BONSUCESSO', 'Bonsucesso'),
(8, 'SAFRA', 'Safra'),
(9, 'BMG', 'BMG'),
(10, 'BANRISUL', 'Banrisul'),
(11, 'CCB', 'BANCO DA CHINA BRASIL S.A'),
(12, 'FINASA BMC', 'BANCO FINASA BMC S.A'),
(13, 'BANRISUL', 'Unicred Central do Rio Grande do Sul'),
(14, 'SABEMI', 'SABEMI'),
(15, 'ITAU', 'BANCO ITABANCO S.A'),
(16, 'CCB', 'CCB'),
(17, 'BRADESCO', 'FINASA BMC');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contracts`
--

CREATE TABLE `contracts` (
  `id` bigint(20) NOT NULL,
  `number` varchar(255) DEFAULT NULL,
  `client_cpf` varchar(255) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `devname_bank` varchar(255) DEFAULT NULL,
  `uf` varchar(255) DEFAULT NULL,
  `company_unit` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT '0',
  `date` date DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `partnership` varchar(255) DEFAULT NULL,
  `devname_partnership` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `contract_import_id` bigint(20) DEFAULT NULL,
  `pendency` varchar(100) DEFAULT '0',
  `date_pendency` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contract_attribute_column_numbers`
--

CREATE TABLE `contract_attribute_column_numbers` (
  `id` bigint(20) NOT NULL,
  `attribute_name` varchar(255) DEFAULT NULL,
  `column_number` int(11) DEFAULT NULL,
  `datasource_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `contract_attribute_column_numbers`
--

INSERT INTO `contract_attribute_column_numbers` (`id`, `attribute_name`, `column_number`, `datasource_id`) VALUES
(12, 'number', 10, 3),
(13, 'client_cpf', 3, 3),
(14, 'bank', 9, 3),
(15, 'uf', 11, 3),
(16, 'company_unit', 0, 3),
(17, 'state', 8, 3),
(18, 'date', 0, 3),
(19, 'operation', 0, 3),
(20, 'value', 5, 3),
(21, 'partnership', 0, 3),
(22, 'updated_at', 7, 3),
(23, 'number', 5, 4),
(24, 'client_cpf', 8, 4),
(25, 'bank', 7, 4),
(26, 'uf', 6, 4),
(27, 'company_unit', 3, 4),
(28, 'state', 11, 4),
(29, 'date', 4, 4),
(30, 'operation', 13, 4),
(31, 'value', 10, 4),
(32, 'partnership', 12, 4),
(33, 'updated_at', 11, 4),
(34, 'id_chamado', 1, 5),
(35, 'data_abertura', 3, 5),
(36, 'uf', 4, 5),
(37, 'unidade', 5, 5),
(38, 'subcategoria', 10, 5),
(39, 'categoria', 8, 5),
(40, 'data_fechamento', 11, 5),
(41, 'number', 15, 6),
(42, 'pendency', 8, 6),
(43, 'date', 25, 6),
(44, 'number', 1, 7),
(45, 'pendency', 24, NULL),
(46, 'date', 24, 7),
(47, 'number', 13, 8),
(48, 'date', 0, 8),
(49, 'pendency', 0, 8),
(50, 'number', 2, 9),
(51, 'pendency', 23, 9),
(52, 'date', 1, 9),
(53, 'number', 9, 10),
(54, 'pendency', 11, 10),
(55, 'date', 7, 10),
(56, 'number', 6, 11),
(57, 'pendency', 18, 11),
(58, 'date', 11, 11),
(59, 'number', 3, 12),
(60, 'pendency', 7, 12),
(61, 'date', 2, 12),
(62, 'pendency', 24, 7),
(63, 'titulo', 2, 5),
(64, 'tipo_unidade', 6, 5),
(65, 'number', 2, 14),
(66, 'number', 4, 13),
(67, 'pendency', 11, 13),
(68, 'date_pendency', 0, 13);

-- --------------------------------------------------------

--
-- Estrutura da tabela `datasources`
--

CREATE TABLE `datasources` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `import_file_data_delimiter` varchar(255) DEFAULT NULL,
  `header_lines` int(11) DEFAULT NULL,
  `devname` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `datasources`
--

INSERT INTO `datasources` (`id`, `name`, `import_file_data_delimiter`, `header_lines`, `devname`) VALUES
(3, 'Harpia', ';', 1, 'Harpia'),
(4, 'WorkBank', ';', 1, 'WorkBank'),
(5, 'GLPI - SDE', ';', 1, 'GLPI'),
(6, 'Pendência Olé', ';', 8, 'Ole'),
(7, 'Pendência Pan', ';', 1, 'Pan'),
(8, 'Pendência Banrisul', ';', 6, 'Banrisul'),
(9, 'Pendência Safra', ';', 1, 'Safra'),
(10, 'Pendência Itaú', ';', 2, 'Itau'),
(11, 'Pendência BMG', ';', 2, 'BMG'),
(12, 'Pendência Sabemi', ';', 1, 'Sabemi'),
(13, 'Pendência CCB', ';', 1, 'CCB'),
(14, 'Pendência Bradesco', ';', 1, 'Bradesco'),
(15, 'GLPI - Monitoramento', ';', 1, 'GLPI');

-- --------------------------------------------------------

--
-- Estrutura da tabela `data_import`
--

CREATE TABLE `data_import` (
  `id` bigint(20) NOT NULL,
  `datasource_id` bigint(20) DEFAULT NULL,
  `import_file_path` text,
  `failed` tinyint(1) DEFAULT NULL,
  `err_message` varchar(255) DEFAULT NULL,
  `date_import` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `glpi`
--

CREATE TABLE `glpi` (
  `id` int(11) NOT NULL,
  `titulo` varchar(300) DEFAULT NULL,
  `id_chamado` varchar(11) DEFAULT NULL,
  `data_abertura` datetime DEFAULT NULL,
  `uf` varchar(100) DEFAULT NULL,
  `unidade` varchar(100) DEFAULT NULL,
  `tipo_unidade` varchar(100) DEFAULT NULL,
  `categoria` varchar(100) DEFAULT NULL,
  `subcategoria` varchar(100) DEFAULT NULL,
  `data_fechamento` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `import_contracts`
--

CREATE TABLE `import_contracts` (
  `id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `data_import_id` int(11) NOT NULL,
  `pendency` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `partnership`
--

CREATE TABLE `partnership` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `devname` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `partnership`
--

INSERT INTO `partnership` (`id`, `name`, `devname`) VALUES
(1, 'INSS', 'INSS'),
(2, 'AERONAUTICA', 'AERONAUTICA'),
(3, 'AMAZON', 'AMAZON'),
(4, 'BANRISUL', 'BANRISUL'),
(5, 'BBS', 'BBS'),
(6, 'BMG', 'BMG'),
(7, 'DAYCOVAL', 'DAYCOVAL'),
(8, 'EXERCITO', 'EXERCITO'),
(9, 'FEDERAL', 'FEDERAL'),
(10, 'GOV', 'GOVERNO'),
(11, 'IPREM', 'IPREM'),
(12, 'MARINHA', 'MARINHA'),
(13, 'PAN', 'PAN'),
(14, 'PM', 'PM'),
(15, 'PREF', 'PREFEITURA'),
(16, 'TJ', 'TJ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password_digest` varchar(255) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_contracts_on_contract_import_id` (`contract_import_id`);

--
-- Indexes for table `contract_attribute_column_numbers`
--
ALTER TABLE `contract_attribute_column_numbers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_contract_attribute_column_numbers_on_datasource_id` (`datasource_id`);

--
-- Indexes for table `datasources`
--
ALTER TABLE `datasources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_import`
--
ALTER TABLE `data_import`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_data_import_on_datasource_id` (`datasource_id`);

--
-- Indexes for table `glpi`
--
ALTER TABLE `glpi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `import_contracts`
--
ALTER TABLE `import_contracts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partnership`
--
ALTER TABLE `partnership`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contract_attribute_column_numbers`
--
ALTER TABLE `contract_attribute_column_numbers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `datasources`
--
ALTER TABLE `datasources`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `data_import`
--
ALTER TABLE `data_import`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `glpi`
--
ALTER TABLE `glpi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `import_contracts`
--
ALTER TABLE `import_contracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partnership`
--
ALTER TABLE `partnership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `contracts`
--
ALTER TABLE `contracts`
  ADD CONSTRAINT `fk_rails_f92f5ebafb` FOREIGN KEY (`contract_import_id`) REFERENCES `data_import` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `contract_attribute_column_numbers`
--
ALTER TABLE `contract_attribute_column_numbers`
  ADD CONSTRAINT `fk_rails_5ebd1666f9` FOREIGN KEY (`datasource_id`) REFERENCES `datasources` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `data_import`
--
ALTER TABLE `data_import`
  ADD CONSTRAINT `fk_rails_b268ec74ff` FOREIGN KEY (`datasource_id`) REFERENCES `datasources` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
