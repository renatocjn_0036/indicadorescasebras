<?php
session_start();
if(!isset($_SESSION["usuario"]) && (empty($_SESSION["usuario"]))){

    header("Location: /indicadorescasebras/login.php");
    exit;
}
?>
<html ng-app="app">
  <head>
    <meta charset="utf-8" />
    <title>BI Casebrás</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link href="lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="lib/select2/select2.css">
    <link rel="stylesheet" href="lib/select2/select2-bootstrap.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- JQUERY -->
    <script src="assets/js/modernizr.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>

    <!-- Boostrap -->
    <script src="lib/bootstrap/dist/js/bootstrap.min.js" ></script>

    <script src="node_modules/xlsexport/xls-export.es5.js" ></script>

    <script src="lib/select2/select2.js"></script>

    <!-- angularjs -->
    <script src="lib/angular/angular.min.js" ></script>
    <!-- <link rel="stylesheet" href="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.css">
    <script src="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.js"></script> -->
    <script src="app/angular-route.min.js" ></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-animate.js"></script>
    <script src="lib/angular-google-chart/ng-google-chart.js" ></script>
    <script src="app/ui-bootstrap-tpls-3.0.5.min.js" ></script>
    <script src="app/angular-locale_pt-br.js"></script>
    <script src="app/angular-input-masks.br.min.js"></script>
    <script src="lib/angular-ui-select2/src/select2.js"></script>
    <script src="app/directive/paging.js"></script>
    <script src="app/app.js" ></script>
    <script src="app/config/routeConfig.js" ></script>

                    <!-- Controller -->
    <script src="app/controller/ImportacaoController.js" ></script>
    <script src="app/controller/FonteController.js" ></script>
    <script src="app/controller/UsuariosController.js" ></script>
    <script src="app/controller/NovoUsuarioController.js" ></script>
    <script src="app/controller/EditarUsuarioController.js" ></script>

    <script src="app/controller/AdminController.js" ></script>
    <script src="app/controller/HomeController.js" ></script>
    <script src="app/controller/ConfiguracaoController.js" ></script>

        <!-- Análise Documental -->
    <script src="app/controller/analise-documental/HomeAnaliseDocumentalController.js" ></script>
    <script src="app/controller/analise-documental/IndicadoresHarpiaController.js" ></script>
    <script src="app/controller/analise-documental/IndicadoresHarpiaWorkController.js" ></script>
        <!-- Formalização -->
    <script src="app/controller/formalizacao/HomeFormalizacaoController.js" ></script>
    <script src="app/controller/formalizacao/IndicadoresFormalizacaoController.js" ></script>
        <!-- SDE -->
    <script src="app/controller/SDE/HomeSdeController.js" ></script>
    <script src="app/controller/SDE/IndicadoresGLPIController.js" ></script>
        <!-- Contestacao -->
    <script src="app/controller/contestacao/HomeContestacaoController.js" ></script>
    <script src="app/controller/contestacao/ListaContestacaoController.js" ></script>
    <script src="app/controller/contestacao/IndicadoresContestacaoController.js" ></script>
        <!-- Monitoramento -->
    <script src="app/controller/monitoramento/IndicadoresMonitoramentoController.js" ></script>
    <script src="app/controller/monitoramento/HomeMonitoramentoController.js" ></script>
        <!-- Gestor -->
    <script src="app/controller/gestor/GestorPendenciaController.js" ></script>
    <script src="app/controller/gestor/AcompanharPendenciaController.js" ></script>
        <!-- Portabilidade -->
    <script src="app/controller/portabilidade/HomePortabilidadeController.js" ></script>
    <script src="app/controller/portabilidade/ListaPortabilidadeController.js" ></script>

    <!-- Service -->
    <script src="app/service/ChartService.js" ></script>
    <script src="app/service/ChartContestacaoService.js" ></script>
    <script src="app/service/ultimaImportacaoService.js" ></script>
    <script src="app/service/DataContestacaoService.js" ></script>
    <script src="app/service/DataUserService.js" ></script>
    <script src="app/service/CardService.js" ></script>
    <script src="app/service/DataGestorPendenciaService.js" ></script>
    <script src="app/service/PortabilidadeService.js" ></script>
    <script src="app/service/PermissaoService.js" ></script>
    <!-- Directive -->
    <script src="app/directive/uiFormulario.js" ></script>
    <script src="app/directive/uiCpfDirective.js" ></script>
    <script src="app/directive/uiAtualizacao.js" ></script>
    <script src="app/directive/uiDate.js" ></script>
    <script src="app/directive/uiTree.js" ></script>
    <style>
    .list{
      margin-left: 64px
    }
    .permissao-pai{
      margin-left: 14px
    }
    ul.tree {
    list-style: none;
    }

    ul.tree > li {
        margin: 4px;
    }

    ul.tree > li > i {
        cursor: pointer;
    }
    .modal-lg{
      max-width: 90%;
    }
    .help-block{
          color: red;
    }
    .selecionado{
      font-weight: bold
    }
    .table > thead > tr > th {
    text-align: left;
  }
    .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
        z-index: 2;
        color: #fff;
        cursor: default;
        background-color: #337ab7;
        border-color: #337ab7;
    }
    .pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
    }
    .badge-secondary {
      color: #fff !important;
      background-color: #6c757d !important;
    }

    .nav-tabs {
        border-bottom: 1px solid #dee2e6 !important;
    }

    /* .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        color: #495057;
        background-color: #fff;
        border-color: #dee2e6 #dee2e6 #fff;
    }

    .nav-tabs .nav-link {
        border: 1px solid transparent;
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem;
    } */

    </style>
  </head>
  <body>
        <div ng-view></div>

      <!-- Popper for Bootstrap -->

        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

          <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>


      <!-- //  <script src="lib/bootstrap-validator/dist/validator.min.js" ></script> -->
  </body>
</html>
