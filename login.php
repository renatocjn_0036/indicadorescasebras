<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <title>Login Indicadores Casebrás</title>

        <link href="lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>
        <script src="assets/js/jquery.min.js"></script>
        <script>
        window.history.pushState({}, document.title, "/" + "indicadorescasebras/login.php");
        </script>
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="card-box">
                <div class="panel-heading">
                  <div class="logo" style="text-align: center;">
                      <img src="assets/images/logo_login.png" alt="" class="logo-lg">
                  </div>
                    <h4 class="text-center"> <strong class="text-custom">B.I Indicadores</strong></h4>
                </div>
                <div class="p-20">
                          <?php
                              if(isset($_GET['error'])){

                                switch ($_GET['error']) {
                                  case "1":
                                    $msg = 'Usuário ou senha Inválida';
                                    break;
                                  case "2":
                                    $msg = 'Usuário Bloqueado';
                                    break;
                                  default:
                                    $msg = 'Usuário ou senha Inválida';
                                    break;
                                }
                          ?>
                              <div class="alert alert-danger alert-dismissable" style="text-align: center;">
                                <strong><?php echo $msg ?></strong></a>.
                              </div>
                          <?php
                              }
                          ?>
                    <form class="form-horizontal m-t-20" action="backend/lib/login.php" method="post" novalidate>

                        <div class="form-group ">
                            <div class="col-12">
                                <input class="form-control" type="text" name="username" required="" placeholder="Usuário">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12">
                                <input class="form-control" type="password" name="password" required="" placeholder="Senha">
                            </div>
                        </div>

                        <div class="form-group text-center m-t-40">
                            <div class="col-12">
                                <button class="btn btn-primary btn-block text-uppercase waves-effect waves-light"
                                        type="submit">Log In
                                </button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>


        </div>


        <!-- jQuery  -->

        <script src="lib/bootstrap/dist/js/bootstrap.min.js" ></script>

	</body>
</html>
