<div class="topbar-main">
  <div class="container-fluid">
    <div class="logo">
      <a href="index.html" class="logo">
        <img src="assets/images/logo_casebras.png" alt="" class="logo-lg">
        <img src="assets/images/logo_sm.png" alt="" height="24" class="logo-sm">
      </a>
    </div>
    <div class="menu-extras topbar-custom">
      <ul class="list-inline float-right mb-0">
        <li class="menu-item list-inline-item">
          <a class="navbar-toggle nav-link">
            <div class="lines">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </a>
        </li>
        <li class="list-inline-item dropdown notification-list">
          <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="" role="button" aria-haspopup="false" aria-expanded="false">

          <?php session_start(); echo ucwords(strtolower($_SESSION['usuario']['dados']->name));
          ?>

          </a>
          <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
            <a href="backend/lib/logout.php" class="dropdown-item notify-item">
              <i class="md md-settings-power"></i> <span>SAIR</span>
            </a>
            <?php 
            if($_SESSION['usuario']['dados']->id_perfil == 1){

            ?>
                <a href="#/admin" class="dropdown-item notify-item">
                  <i class="md md-user"></i> <span>Administração</span>
                </a>
            <?php } ?>
          </div>
        </li>
      </ul>
    </div>
    <!-- end menu-extras -->

    <div class="clearfix"></div>

  </div>
  <!-- end container -->
</div>
